<?php

/**
 *  该接口用于释放RDS实例，要求如下：
    实例状态为运行中。
    实例没有被人为锁定。
    实例类型为主实例（按量付费类型）、只读实例、灾备实例、临时实例。
 *
 * @DateTime 2018-12-25 16:53:01
 */

include_once '../path/aliyun-php-sdk-core/Config.php';
use Rds\Request\V20140815\DeleteDBInstanceRequest;

date_default_timezone_set('Asia/Shanghai'); 
$info = $_GET;

$iClientProfile = DefaultProfile::getProfile($info['RegionId'], $info['AccessKeyId'], $info['AccessSecret']);
$client = new DefaultAcsClient($iClientProfile);
$request = new DeleteDBInstanceRequest();

// -------------------------必填参数---------------------------

//*实例名。
$request->setDBInstanceId($info['DBInstanceId']);


//发起请求并处理返回
try {
    $response = $client->getAcsResponse($request);
    echo json_encode($response);
} catch(ServerException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
} catch(ClientException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
}