<?php

/**
 * 该接口用于查看指定实例的详细属性。
 *
 * @DateTime 2018-12-25 17:10:23
 */

include_once '../path/aliyun-php-sdk-core/Config.php';
use Rds\Request\V20140815\DescribeDBInstanceAttributeRequest;

date_default_timezone_set('Asia/Shanghai'); 
$info = $_GET;

$iClientProfile = DefaultProfile::getProfile($info['RegionId'], $info['AccessKeyId'], $info['AccessSecret']);
$client = new DefaultAcsClient($iClientProfile);
$request = new DescribeDBInstanceAttributeRequest();

// -------------------------必填参数---------------------------

//*实例ID，可以一次输入多个，以英文“,”分隔；最多传入30个。
$request->setDBInstanceId($info['DBInstanceId']);

// --------------------------------非必填参数-----------------------------------

//资源组ID。
if (isset($info['ResourceGroupId']))
{
    $request->setResourceGroupId($info['ResourceGroupId']);
}


//发起请求并处理返回
try {
    $response = $client->getAcsResponse($request);
    echo json_encode($response);
} catch(ServerException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
} catch(ClientException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
}
