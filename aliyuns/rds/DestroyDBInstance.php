<?php

/**
 * 该接口用于销毁RDS实例，要求实例状态为锁定中。
 *
 * @DateTime 2018-12-25 16:57:52
 */

include_once '../path/aliyun-php-sdk-core/Config.php';
use Rds\Request\V20140815\DestroyDBInstanceRequest;

date_default_timezone_set('Asia/Shanghai'); 
$info = $_GET;

$iClientProfile = DefaultProfile::getProfile($info['RegionId'], $info['AccessKeyId'], $info['AccessSecret']);
$client = new DefaultAcsClient($iClientProfile);
$request = new DestroyDBInstanceRequest();

// -------------------------必填参数---------------------------

//*实例的ID。
$request->setDBInstanceId($info['DBInstanceId']);


//发起请求并处理返回
try {
    $response = $client->getAcsResponse($request);
    echo json_encode($response);
} catch(ServerException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
} catch(ClientException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
}