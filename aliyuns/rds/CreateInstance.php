<?php

/**
 * 创建RDS实例
 */

include_once '../path/aliyun-php-sdk-core/Config.php';
use Rds\Request\V20140815\CreateDBInstanceRequest;

date_default_timezone_set('Asia/Shanghai'); 
$info = $_GET;

$iClientProfile = DefaultProfile::getProfile($info['RegionId'], $info['AccessKeyId'], $info['AccessSecret']);
$client = new DefaultAcsClient($iClientProfile);
$request = new CreateDBInstanceRequest();

// -------------------------必填参数---------------------------

//*地域，通过接口 aliyuns/rds/DescribeRegions 获取
$request->setRegionId($info['RegionId']);

//*数据库类型，取值范围为MySQL/SQLServer/PostgreSQL/PPAS
$request->setEngine($info['Engine']);

//*数据库版本号，取值如下：
//MySQL：5.5/5.6/5.7
//SQLServer：2008r2/2012/2012/web/2016_web/2012_std_ha/2012_ent_ha/2016_std_ha/2016_ent_ha
//PostgreSQL：9.4
//PPAS：9.3
$request->setEngineVersion($info['EngineVersion']);

//*实例规格
$request->setDBInstanceClass($info['DBInstanceClass']);

//*自定义存储空间，取值范围：
//MySQL/PostgreSQL/PPAS 双机高可用版为 [5,2000]；
//MySQL 5.7 单机基础版为 [20,1000]；
//SQL Server 2008R2 为 [10,2000]；
//SQL Server 2012/2016 单机基础版为 [20,2000];
//SQL Server 2012/2016 双机高可用版为 [20,5000];
//每5G进行递增。单位：GB。详见实例规格表 。
$request->setDBInstanceStorage($info['DBInstanceStorage']);

//*实例的网络连接类型：Internet：表示公网   Intranet：表示私网
$request->setDBInstanceNetType($info['DBInstanceNetType']);

//*该实例下所有数据库的IP名单，以逗号隔开，不可重复，最多1000个。
//支持格式：%，0.0.0.0/0，10.23.12.24（IP），或者10.23.12.24/24（CIDR模式，无类域间路由，/24表示了地址中前缀的长度，范围[1,32]）其中，0.0.0.0/0，表示不限制。
$request->setSecurityIPList($info['SecurityIPList']);

//*付费类型：
//Postpaid：后付费实例
//Prepaid：预付费实例
$request->setPayType($info['PayType']);

//*用于保证幂等性。
$request->setClientToken($info['ClientToken']);

// --------------------------------非必填参数-----------------------------------

//可用区ID，通过接口 aliyuns/rds/DescribeRegions 获取
if (isset($info['ZoneId']))
{
    $request->setZoneId($info['ZoneId']);
}

//实例的描述或备注信息，不超过256个字节。
//不能以http:>实例的描述或备注信息，不超过256个字节。
//不能以http:// , https:// 开头。
//以中文、英文字母开头。
//可以包含中文、英文字符、””，” -”，数字字符长度2~256。
if (isset($info['DBInstanceDescription']))
{
    $request->setDBInstanceDescription($info['DBInstanceDescription']);
}

//若付费类型为Prepaid时该参数为必传参数。指定预付费实例为包年或者包月类型，Year：包年；Month：包月。
if (isset($info['Period']))
{
    $request->setPeriod($info['Period']);
}

//当参数Period为Year时，UsedTime可取值为[1,9]。
//当参数Period为Month时，UsedTime可取值为1、2、3。
if (isset($info['UsedTime']))
{
    $request->setUsedTime($info['UsedTime']);
}

//VPC：创建VPC实例
//Classic：创建Classic实例
//不填，默认创建Classic实例
if (isset($info['InstanceNetworkType']))
{
    $request->setInstanceNetworkType($info['InstanceNetworkType']);
}

//Performance为标准访问模式；Safty为高安全访问模式；默认为RDS系统分配。
if (isset($info['ConnectionMode']))
{
    $request->setConnectionMode($info['ConnectionMode']);
}

//VPC ID。
if (isset($info['VPCId']))
{
    $request->setVPCId($info['VPCId']);
}

//VSwitch Id，多个值用英文逗号“,”隔开。
if (isset($info['VSwitchId']))
{
    $request->setVSwitchId($info['VSwitchId']);
}

//用户可以指定VSwitchId下的vpcIp，如果不输入，系统自动分配。
if (isset($info['PrivateIpAddress']))
{
    $request->setPrivateIpAddress($info['PrivateIpAddress']);
}


//发起请求并处理返回
try {
    $response = $client->getAcsResponse($request);
    echo json_encode($response);
} catch(ServerException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
} catch(ClientException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
}
