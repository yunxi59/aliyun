<?php

/**
 * 该接口用于查看实例列表或被RAM授权的实例列表。
 *
 * @DateTime 2018-12-25 17:10:23
 */

include_once '../path/aliyun-php-sdk-core/Config.php';
use Rds\Request\V20140815\DescribeDBInstancesRequest;

date_default_timezone_set('Asia/Shanghai'); 
$info = $_GET;

$iClientProfile = DefaultProfile::getProfile($info['RegionId'], $info['AccessKeyId'], $info['AccessSecret']);
$client = new DefaultAcsClient($iClientProfile);
$request = new DescribeDBInstancesRequest();

// -------------------------必填参数---------------------------

//*实例所属地域ID，通过接口 aliyuns/rds/DescribeRegions 获取
$request->setRegionId($info['RegionId']);

// --------------------------------非必填参数-----------------------------------

//数据库类型，取值范围为
//MySQL；
//SQLServer；
//PostgreSQL；
//PPAS。
//不填，默认返回所有数据库类型。
if (isset($info['Engine']))
{
    $request->setEngine($info['Engine']);
}

//实例类型，取值范围如下：
//Primary：主实例；
//Readonly：只读实例；
//Guard：灾备实例；
//Temp：临时实例。
//不填，默认返回所有类型的实例。
if (isset($info['DBInstanceType']))
{
    $request->setDBInstanceType($info['DBInstanceType']);
}

//实例的网络类型，取值范围如下：
//VPC：VPC网络下的实例；
//Classic：经典网络下的实例。
//不填，默认返回所有网络类型下的实例。
if (isset($info['InstanceNetworkType']))
{
    $request->setInstanceNetworkType($info['InstanceNetworkType']);
}

//实例的访问模式：
//Standard：标准访问模式；
//Safe：高安全访问模式。
//不填，默认返回所有访问模式下的实例。
if (isset($info['ConnectionMode']))
{
    $request->setConnectionMode($info['ConnectionMode']);
}

//查询绑定有该标签的实例：
//传入值格式为Json string，包括TagKey和TagValue；
//TagKey不能为空，TagValue可以为空。
//示例：{“key1”:”value1”}。
if (isset($info['Tags']))
{
    $request->setTags($info['Tags']);
}

//每页记录数，取值：
//30；
//50；
//100；
//默认值：30。
if (isset($info['PageSize']))
{
    $request->setPageSize($info['PageSize']);
}

//页码，取值为：大于0且不超过Integer数据类型的的最大值，默认值为1。
if (isset($info['PageNumber']))
{
    $request->setPageNumber($info['PageNumber']);
}


//发起请求并处理返回
try {
    $response = $client->getAcsResponse($request);
    echo json_encode($response);
} catch(ServerException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
} catch(ClientException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
}
