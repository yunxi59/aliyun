<?php

/**
 *  该接口用于重启实例，重启一般在10S钟内完成。如果有大量的事务需要提交或回滚，可能会延长1分钟左右。重启实例必须满足以下条件，否则将操作失败：
    实例使用中
    实例没有进行中的备份
 *
 * @DateTime 2018-12-25 17:07:07
 */

include_once '../path/aliyun-php-sdk-core/Config.php';
use Rds\Request\V20140815\RestartDBInstanceRequest;

date_default_timezone_set('Asia/Shanghai'); 
$info = $_GET;

$iClientProfile = DefaultProfile::getProfile($info['RegionId'], $info['AccessKeyId'], $info['AccessSecret']);
$client = new DefaultAcsClient($iClientProfile);
$request = new RestartDBInstanceRequest();

// -------------------------必填参数---------------------------

//*实例ID。
$request->setDBInstanceId($info['DBInstanceId']);

// --------------------------------非必填参数-----------------------------------

//用于保证幂等性。
if (isset($info['ClientToken']))
{
    $request->setClientToken($info['ClientToken']);
}


//发起请求并处理返回
try {
    $response = $client->getAcsResponse($request);
    echo json_encode($response);
} catch(ServerException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
} catch(ClientException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
}
