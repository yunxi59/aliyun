<?php

/**
 * 该接口用于查看用户可选的RDS地域和可用区。调用创建实例接口之前，先用该接口查询RegionId。
 *
 * @DateTime 2018-12-25 15:16:15
 */

include_once '../path/aliyun-php-sdk-core/Config.php';
use Rds\Request\V20140815\DescribeRegionsRequest;

date_default_timezone_set('Asia/Shanghai'); 
$info = $_GET;

$iClientProfile = DefaultProfile::getProfile(NULL, $info['AccessKeyId'], $info['AccessSecret']);
$client = new DefaultAcsClient($iClientProfile);
$request = new DescribeRegionsRequest();

//发起请求并处理返回
try {
    $response = $client->getAcsResponse($request);
    echo json_encode($response);
} catch(ServerException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
} catch(ClientException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
}