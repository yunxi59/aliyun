<?php

/**
 *  该接口用于为某个实例创建一个只读实例。

    只读实例数量：
    如果主实例内存大于等于64GB，则最多允许创建10个只读实例。
    如果主实例内存小于64GB，则最多允许创建5个只读实例。
    主实例的版本必须为以下其中一种：
    MySQL 5.5
    MySQL 5.6
    MySQL 5.7高可用版（本地SSD盘）
 */

include_once '../path/aliyun-php-sdk-core/Config.php';
use Rds\Request\V20140815\CreateReadOnlyDBInstanceRequest;

date_default_timezone_set('Asia/Shanghai'); 
$info = $_GET;

$iClientProfile = DefaultProfile::getProfile($info['RegionId'], $info['AccessKeyId'], $info['AccessSecret']);
$client = new DefaultAcsClient($iClientProfile);
$request = new CreateReadOnlyDBInstanceRequest();

// -------------------------必填参数---------------------------

//*地域。只读实例的地域必须和主实例相同。通过接口 aliyuns/rds/DescribeRegions 获取
$request->setRegionId($info['RegionId']);

//*可用区ID，通过接口 aliyuns/rds/DescribeRegions 获取
$request->setZoneId($info['ZoneId']);

//*主实例ID。
$request->setDBInstanceId($info['DBInstanceId']);

//*数据库版本号。必须与主实例相同。取值：
//5.6
//5.7
$request->setEngineVersion($info['EngineVersion']);

//*实例规格，详见实例规格表。建议只读实例规格不小于主实例规格，否则易导致只读实例延迟高、负载高等现象。
$request->setDBInstanceClass($info['DBInstanceClass']);

//*自定义存储空间，取值范围：[5,1000]，每5GB进行递增，单位：GB。
$request->setDBInstanceStorage($info['DBInstanceStorage']);

//*付费类型：  取值为：Postpaid。
$request->setPayType($info['PayType']);

//*用于保证幂等性。
$request->setClientToken($info['ClientToken']);

// --------------------------------非必填参数-----------------------------------

//实例的描述或备注信息，不超过256个字节
//不能以或https://开头，以中文、英文字母开头。可以包含中文、英文字符、“_”、“-”，数字，长度为2~256个字符
if (isset($info['DBInstanceDescription']))
{
    $request->setDBInstanceDescription($info['DBInstanceDescription']);
}

//Classic：经典网络，不填时创建Classic实例；
//VPC：VPC网络。
if (isset($info['InstanceNetworkType']))
{
    $request->setInstanceNetworkType($info['InstanceNetworkType']);
}

//VPC ID。
if (isset($info['VPCId']))
{
    $request->setVPCId($info['VPCId']);
}

//VSwitch Id，多个值用英文逗号“,”隔开。
if (isset($info['VSwitchId']))
{
    $request->setVSwitchId($info['VSwitchId']);
}

//用户可以指定VSwitchId下的vpcIp。如果不输入，系统通过vpcId和VSwitchId自动分配。
if (isset($info['PrivateIpAddress']))
{
    $request->setPrivateIpAddress($info['PrivateIpAddress']);
}


//发起请求并处理返回
try {
    $response = $client->getAcsResponse($request);
    echo json_encode($response);
} catch(ServerException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
} catch(ClientException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
}
