<?php

/**
 *  该接口用于对按时付费实例的规格或者存储空间变更配置，使用该接口时必须满足如下条件，否则实例规格会变更失败：

    实例状态为运行中
    实例当前无正在运行的备份
    请求参数中必须至少指定实例规格（DBInstanceClass）或存储空间（DBInstanceStorage）。
    若降低磁盘空间配置，输入的磁盘空间不能小于实际使用空间大小的1.1倍。
    当前只支持对常规实例和只读实例变更配置，不支持灾备实例和临时实例。
 *
 * @DateTime 2018-12-25 17:03:47
 */

include_once '../path/aliyun-php-sdk-core/Config.php';
use Rds\Request\V20140815\ModifyDBInstanceSpecRequest;

date_default_timezone_set('Asia/Shanghai'); 
$info = $_GET;

$iClientProfile = DefaultProfile::getProfile($info['RegionId'], $info['AccessKeyId'], $info['AccessSecret']);
$client = new DefaultAcsClient($iClientProfile);
$request = new ModifyDBInstanceSpecRequest();

// -------------------------必填参数---------------------------

//*待升降级的实例。
$request->setDBInstanceId($info['DBInstanceId']);

//*计费方式，取值为Postpaid，按时付费。
$request->setPayType($info['PayType']);

// --------------------------------非必填参数-----------------------------------

//实例规格，详见实例规格表。
if (isset($info['DBInstanceClass']))
{
    $request->setDBInstanceClass($info['DBInstanceClass']);
}

//自定义存储空间，取值范围如下且取值必须为5的整数倍：
//MySQL为[5,2000]
//SQL Server为[10,2000]
//PostgreSQL和PPAS为[5,2000]
if (isset($info['DBInstanceStorage']))
{
    $request->setDBInstanceStorage($info['DBInstanceStorage']);
}


//发起请求并处理返回
try {
    $response = $client->getAcsResponse($request);
    echo json_encode($response);
} catch(ServerException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
} catch(ClientException $e) {
    echo "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
}
