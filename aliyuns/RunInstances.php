<?php
    date_default_timezone_set('Asia/Shanghai'); 
	$info = $_GET;
	include_once '../path/aliyun-php-sdk-core/Config.php';
	use Ecs\Request\V20140526\RunInstancesRequest;
	$iClientProfile = DefaultProfile::getProfile($info['url'],$info['accesskeyid'],$info['accesssecret']);
	$client = new DefaultAcsClient($iClientProfile);

	$request = new RunInstancesRequest();
	$request -> regionId($info['url']);
	if(!empty($info['size'])){
	    $request -> DataDisks($info['size'],$info['category']);
	}
	$request -> tagvalue($info['tagvalue']);
	$request -> tagkey($info['tagkey']);
	$request -> setImageId($info['imageid']);
	if(!empty($info['zoneid'])){
	    $request -> setZoneId($info['zoneid']);
	}
	$request -> setInstanceType($info['instancetype']);
	$request -> setInternetChargeType($info['internetchargetype']);
	$request -> setAutoRenew("False");
	$request -> setIoOptimized("optimized");
	$request -> setInstanceChargeType($info['instancechargetype']);
	$request -> setPeriod($info['period']);
	$request -> setPeriodUnit($info['periodunit']);
	$request -> setInternetMaxBandwidthOut($info['setinternetmaxbandwidthout']);
	$request -> setPassword($info['password']);
	$request -> setSystemDiskSize($info['setsystemdisksize']);
	$request -> setSystemDiskCategory($info['setsystemdiskcategory']);
	echo json_encode($request);
	# 发起请求并处理返回
	/*try {
	    $response = $client->getAcsResponse($request);
		echo json_encode($response);
	} catch(ServerException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	} catch(ClientException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	}*/
?>
