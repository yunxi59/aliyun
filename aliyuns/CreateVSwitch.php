<?php
    date_default_timezone_set('Asia/Shanghai'); 
	$info = $_GET;
	include_once '../path/aliyun-php-sdk-core/Config.php';
	use Vpc\Request\V20160428\CreateVSwitchRequest;
	$iClientProfile = DefaultProfile::getProfile($info['url'],$info['accesskeyid'],$info['accesssecret']);
	$client = new DefaultAcsClient($iClientProfile);

	$request = new CreateVSwitchRequest();
	//地域
	$request -> regionId($info['url']);
    
    //交换机所属区的ID。
	$request -> setZoneId($info['zoneid']);

	//目标网段
	$request -> setCidrBlock($info['cidrblock']);

	//交换机所属的VPC ID
	$request -> setVpcId($info['vpcid']);

	//交换机的名称
	$request -> setVSwitchName($info['vswitchname']);

	//交换机的描述信息
	if(!empty($info['description'])){
	    $request -> setDescription($info['description']);
	}
	
/*	echo json_encode($request);*/
	# 发起请求并处理返回
	try {
	    $response = $client->getAcsResponse($request);
		echo json_encode($response);
	} catch(ServerException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	} catch(ClientException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	}
?>
