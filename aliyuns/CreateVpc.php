<?php
    date_default_timezone_set('Asia/Shanghai'); 
	$info = $_GET;
	include_once '../path/aliyun-php-sdk-core/Config.php';
	use Vpc\Request\V20160428\CreateVpcRequest;
	$iClientProfile = DefaultProfile::getProfile($info['url'],$info['accesskeyid'],$info['accesssecret']);
	$client = new DefaultAcsClient($iClientProfile);

	$request = new CreateVpcRequest();
	//地域
	$request -> regionId($info['url']);
    
    //VPC的名称
	$request -> setVpcName($info['vpcname']);

	//目标网段
	$request -> setCidrBlock($info['cidrblock']);

	//VPC的描述信息
	if(!empty($info['description'])){
	    $request -> setDescription($info['description']);
	}
	# 发起请求并处理返回
	try {
	    $response = $client->getAcsResponse($request);
		echo json_encode($response);
	} catch(ServerException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	} catch(ClientException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	}
?>
