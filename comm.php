<?php
	$info = $_GET;
	include_once 'path/aliyun-php-sdk-core/Config.php';
	use Ecs\Request\V20140526\DescribeAvailableResourceRequest;
	$iClientProfile = DefaultProfile::getProfile($info['url'],$info['accesskeyid'],$info['accesssecret']);
	$client = new DefaultAcsClient($iClientProfile);

	$request = new DescribeAvailableResourceRequest();
	$request -> regionId($info['url']);
	$request -> setDestinationResource("InstanceType");
	$request -> setIoOptimized($info['optimized']);
	$request -> setInstanceType($info['instancetype']);
	# 发起请求并处理返回
	try {
	    $response = $client->getAcsResponse($request);
		echo json_encode($response);
	} catch(ServerException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	} catch(ClientException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	}
?>
