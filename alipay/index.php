<?php
header("Content-type: text/html; charset=utf-8");
date_default_timezone_set('Asia/Shanghai'); 
/*$testurl = 'http://'.$_SERVER['HTTP_HOST'].'/index.php?m=home&c=index&a=alipay';  */
$testurl = 'http://'.$_SERVER['HTTP_HOST'].'/index.php?m=home&c=pay&a=alipay';  
$ch = curl_init();    
curl_setopt($ch, CURLOPT_URL, $testurl);    
//参数为1表示传输数据，为0表示直接输出显示。  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
//参数为0表示不带头文件，为1表示带头文件  
curl_setopt($ch, CURLOPT_HEADER,0);  
$output = curl_exec($ch);   
curl_close($ch); 
$output = json_decode($output,true);
$orderid = $_GET['orderid'];
$money = $_GET['money'];
require_once("AopSdk.php");
//构造参数  
$aop = new AopClient ();  
/*$aop->gatewayUrl = '[https://openapi.alipay.com/gateway.do](https://openapi.alipay.com/gateway.do)';  */
$aop->gatewayUrl = $output['gatewayUrl'];  
$aop->appId = $output['app_id'];  
$aop->rsaPrivateKey = $output['merchant_private_key'];  
$aop->apiVersion = '1.0';  
$aop->signType = 'RSA2';  
$aop->postCharset= 'utf-8';  
$aop->format='json';  
$request = new AlipayTradePagePayRequest ();  
/*$request->setReturnUrl('http://'.$_SERVER['HTTP_HOST'].'/alipay/notify_url.php'); */ 
$request->setReturnUrl($output['return_url'].'&orderid='.$orderid);
$request->setNotifyUrl($output['notify_url']);  

$request->setBizContent('{"product_code":"FAST_INSTANT_TRADE_PAY","out_trade_no":'.$orderid.',"subject":"云平台","total_amount":'.$money.',"body":"云平台"}');
//请求  
$result = $aop->pageExecute ($request);
//输出  
echo $result;
?>