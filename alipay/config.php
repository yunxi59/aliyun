<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2018102961911229",

		//商户私钥
		'merchant_private_key' => "MIIEowIBAAKCAQEAz7nAC+ZRpde9o+HxWSmzf9XWeXvrpz5MNmVNbu290209V+nO4Ru3rublAKU8/fXlBZIWKYStZW9C5vR44Xh8+vlSBoodU1G1rwbjlXUPQlnIxvoTBMzg4o/O+GCAmyOkhsn/dBOYh/22u+O1lqvtzt0y2pgFLFCg24akno2Lb3HgSrH5yEm93K9ezfr1wNpKPA8LNSaMSRvioB3CgTNamYzadL6GOvzQzviF7C67mwjSPjukOsYqs9dFSouAwIBGrKGiTD+XYNA1eq0sD0oYIPfKrLKNGqycQPvO0v+6uk7T+VIc17N9tPV8TXfn61ebmBQXgYeEctV+jW55VWlThQIDAQABAoIBAGbPFFHPaFGgeWeHCCCNFx4KA+CUiIl5t9akRLLGT9E4m8fLHG0lfr+931F2dr1q2jLrnrRlyjwN2RrdnIZbT+AXTZWW/fSi2D0MN89c5HR+Y3S3iHozvInlixY7DOu7tE5R43Db0f5cf/h2NwWfik98AxNVchoAvdxow0kF3SEGYyvO36hjHJ+Mod+8X7UFTuGJxNWMLhv/ThojKjeyRWIJ4DrrOXraV3kBDCeVCtuI3HvAkKohYvTFmLlTFZI7hPstGTVEzVvNZQ0nDTq8ZfG23wuDtgAQWlfHplj9ORO/CkodHaUQWSNlBcMoQQwnQk+fI5gIefSKcJzVxuZ/FAECgYEA5tTZe8LCP4PwNNJYIy+9NK5t03rLM5/v9kES9C0M0lLDAtD4oIokbKIg0sIWMBgPwzRorqlDU++WabrMBe7HZOqS481nrQ7DHxEvDJXXaMSYsaGU665SaURDFLlp8++D8sJ3saeHSYTMbDGtCn6bCbs1aVsnemkWXPIAT9IZikECgYEA5l/znXB/Z1fCFrjxV1azjAMw14InivtWNdrBJpNRZUtvYHUNz2rrYMVSJnPdfXVeVa/XeG+Oq0cjO7tY8xcKfwSPEpQzShEvVYgdV6+sEtoz11Pk4zd0Sde80HJEqEGm7+quCIv2S/ZWxFboKP/xxvDHa2LcyaClvXEThvUjEEUCgYAzpE77tzjKCPSGfMCnLpj/3j5H3+LyK8f9qdVZt3J043speCMHl6BKhYMeY7mfDOMJ4wHAgbHEZ4eZI+ddNGx7kVdHi760IudYXfnOsP8+Oy2GIAY3LArX7iOr0Ud5AY7dktBAeKB1vDYXxo1WGbkJ1k4+wdv1MOmF5cZYFUMNgQKBgEW5Rqcg1D53zhNIeCrB3+eBU7f1+XfT59y2z3yYW6s3sIbWBo7/QivwrJNcv+1np1xxstoNkXV/O0epxRsVnySgQZkS548EMnpFjvPqBD9UcRo7luu57eUneUrxOpxd+mNkeLW3fpyw+sPDtdIq3IckeoaizkXr7B2w6oMPEEStAoGBAKPJPeBHf2V1PLOp9n6JcJ5OVyNyWh7KZ1ETDsCQA1IFLgjPzoVBkEg4HyeJs/E72sRAzamF0grluS2FGdr4/5iWlA3gYREuEJnwRQzjX4XW2UibbN+3Ep0825XPE9q6mwqdCKDUaWiD+LcRcOQpTU+udDb2a9ZP6djqfI1GNIHA",
		
		//异步通知地址
		'notify_url' =>  'http://'.$_SERVER['HTTP_HOST'].'/index.php?m=home&c=recharge&a=notify_url',
		
		//同步跳转
		'return_url' => 'http://'.$_SERVER['HTTP_HOST'],

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAz7nAC+ZRpde9o+HxWSmzf9XWeXvrpz5MNmVNbu290209V+nO4Ru3rublAKU8/fXlBZIWKYStZW9C5vR44Xh8+vlSBoodU1G1rwbjlXUPQlnIxvoTBMzg4o/O+GCAmyOkhsn/dBOYh/22u+O1lqvtzt0y2pgFLFCg24akno2Lb3HgSrH5yEm93K9ezfr1wNpKPA8LNSaMSRvioB3CgTNamYzadL6GOvzQzviF7C67mwjSPjukOsYqs9dFSouAwIBGrKGiTD+XYNA1eq0sD0oYIPfKrLKNGqycQPvO0v+6uk7T+VIc17N9tPV8TXfn61ebmBQXgYeEctV+jW55VWlThQIDAQAB",
);