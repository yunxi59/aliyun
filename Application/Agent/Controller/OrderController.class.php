<?php
namespace Agent\Controller;
class OrderController extends BaseController
{

    public function index()
    {

        $keyword = trim(I('get.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if (!empty($keyword)) {
            //设置查询条件，按会员id或名称模糊查询
            $map['u.id|u.username'] = array('like', "%$keyword%");
        }

        //按订单号查询
        $order_id = trim(I('get.order_id', '', 'htmlspecialchars'));
        if (!empty($order_id)) {
            //设置查询条件，模糊查询
            $map['o.orderid'] = $order_id;
        }

        //设置过滤条件：按(会员)绑定的代理商id进行过滤
        $agent_id = session(('agent.admin_id'));
        $map['u.agent_id'] = $agent_id;

        //查询订单数量
        $total = M('order')
            ->alias('o')
            ->field("o.*,u.agent_id,u.username")
            ->join('__USER__ u ON o.uid = u.id', 'LEFT')
            ->where($map)
            ->count();

        //设置分页
        $pageSize = 10;
        $page = new \Think\Page($total, $pageSize);
        pages($page, $map);
        if ($pageSize < $total) {
            $show = $page->show();
        }

        //查询订单列表
        $order_list = M('order')
            ->alias('o')
            ->field("o.*,u.agent_id,u.username")
            ->join('__USER__ u ON o.uid = u.id', 'LEFT')
            ->where($map)
            ->limit($page->firstRow, $page->listRows)
            ->order('endtime desc')
            ->select();
        foreach ($order_list as $k => $v) {
            $order_list[$k]['endtime'] = date('Y-m-d H:i:s', $v['endtime']);
            if ($order_list[$k]['state'] == 1) {
                $order_list[$k]['state'] = '已付款';
            } else {
                $order_list[$k]['state'] = '待付款';
            }
        }
        $this->assign('num', $total);
        $this->assign('order_id', $order_id); //数据回显
        $this->assign('keyword', $keyword); //数据回显
        $this->assign('list', $order_list);
        $this->assign('show', $show);//分页
        $this->display();
    }

    public function get_order_list()
    {
        $order_id = trim(I('post.order_id'));
        if (!empty($order_id)) {
            $map['order_id'] = $order_id;
        }
        $agent_id = session('agent.admin_id');
        $map['agent_id'] = $agent_id;
        $agent_ids = M('agent')->where('admin_id='.$agent_id)->getField('admin_id', true);

        if (!empty($agent_ids)) {
            $commission_order = M('agent_commission_order_record');
            $total = $commission_order->where($map)->count();

            $pageSize = 10;
            $page = new \Think\Page($total, $pageSize);
            pages($page, $map);
            if ($pageSize < $total) {
                $show = $page->show();
            }
            $order_list = $commission_order->where($map)->limit($page->firstRow, $page->listRows)->order('create_time asc')->select();
        } else {
            //查询结果为空
            $order_list = array();
        }

        $this->assign('keyword', $order_id);
        $this->assign('list', $order_list);
        $this->assign('num', $total);
        $this->assign('show', $show);//分页
        $this->display();
    }

}