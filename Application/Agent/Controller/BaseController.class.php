<?php
namespace Agent\Controller;
use Think\Controller;
class BaseController extends Controller{
	public function _initialize(){
		//验证用户身份
		if(empty(session('agent.admin_id'))){//没有登录，跳转到登录页面
            $this->error('未登录账号，请前往登录...',U('Login/index',3));
		}
	}
}