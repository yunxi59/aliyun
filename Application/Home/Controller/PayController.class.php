<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/24
 * Time: 15:26
 */

namespace Home\Controller;
use Think\Controller;
class PayController extends Controller
{  
    //基本配置
    public function weixin(){
        $values = aliyun();
        $this->ajaxReturn($values);
    }
    
    //支付宝支付基本配置
    public function alipay(){
        $list = M('alipay')->field('keys,value')->select();
        $info = array();
        foreach ($list as $key => $value) {
            $info[$value['keys']] = $value['value'];
        }
        $info['app_id'] = $info['appid'];
        $info['merchant_private_key'] = $info['merchant'];
        $info['notify_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/alipay/notify_url.php';
        $info['return_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/index.php?m=home&c=recharge&a=rechargesuccess';
        $info['charset'] = "UTF-8";
        $info['sign_type'] = "RSA2";
        $info['gatewayUrl'] = "https://openapi.alipay.com/gateway.do";
        $info['alipay_public_key'] = $info['alipaykey'];
        unset($info['appid']);
        unset($info['merchant']);
        unset($info['alipaykey']);
        $this->ajaxReturn($info);
    }

    //回调微信支付
    public function notify()
    {
        if($_GET['orderid']){
            $orderid['orderid'] = $_GET['orderid'];
            $count = M('recharge')->where($orderid)->find();
            if($count){
                if($count['state'] != 1){
                    $arr['pay'] = $_GET['pay']; 
                    $arr['endtime'] = time();   
                    $arr['state'] = 1;
                    $edit = M('recharge')->where($orderid)->save($arr);
                    if($edit){
                        $users = M('user')->where('id='.$count['uid'])->setInc('money',$count['money']);
                        if($users){ 
                            $user = M('user')->field('money')->where('id='.$count['uid'])->find();
                            $water = M('water')->where($orderid)->find();
                            $where['newmoney'] = $user['money'];
                            $where['time'] = time();
                            $where['pay'] = 1;
                            $info = M('water')->where($orderid)->save($where);
                        }
                    }
                }
            }
        }
    }
}