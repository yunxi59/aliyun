<?php
namespace Home\Controller;

use Think\Controller;

class RegisterController extends Controller
{

    //个人注册
    public function personage()
    {
        if (IS_POST) {
            $model = D('Personage');
            if (!$data = $model->create()) {
                $this->error($model->getError(), '', 1);
            } else {
                //注册成功
                if ($inser_id = $model->add()) {
                    $this->success('用户注册成功', U('home/login/index'), 1);
                } else {
                    //注册失败
                    $this->error('用户注册失败', U('home/register/personage'), 1);
                }
            }
        } else {
            $this->display();
        }
    }

    //企业注册
    public function company()
    {
        if (IS_POST) {
            $model = D('Company');
            if (!$data = $model->create()) {
                $this->error($model->getError(), '', 1);
            } else {
                //注册成功
                if ($inser_id = $model->add()) {
                    $this->success('用户注册成功', U('home/login/index'), 1);
                } else {
                    //注册失败
                    $this->error('用户注册失败', U('home/register/company'), 1);
                }
            }
        } else {
            $this->display();
        }
    }

    //推广（代理商）链接
    public function promoteAgent()
    {
        if (IS_POST) {
            $agent_id = trim(I('post.agent_id'));
            $agent = M('agent');
            $condition['admin_id'] = $agent_id;
            $list = $agent->where($condition)->find();

            $admin_name = trim(I('post.admin_name'));
            $admin_password = trim(I('post.admin_password'));
            $phone = trim(I('post.phone'));
            $email = trim(I('post.email'));
            $dengji = $list['dengji'] + 1;
            if (empty($admin_name) || !isNames($admin_name, 6, 20)) {
                $this->error('请输入6-20位的账号');
            }
            if (empty($admin_password) || !isPWD($admin_password, 6, 20)) {
                $this->error('请输入6-20位的密码');
            }
            //检查账号是否已存在
            $admin_info = M('Agent')->field('admin_id')->where(array('admin_name' => $admin_name))->find();
            if (!empty($admin_info)) {
                $this->error('账号已存在');
            }
            $data = array(
                'admin_name' => $admin_name,
                'admin_password' => md5($admin_password),
                'phone' => $phone,
                'email' => $email,
                'register_time' => time(),
                'dengji' => $dengji,
                'shangji' => $agent_id,
            );
            $result = D('Agent')->addAgent($data);
            if ($result) {
                //设置默认折扣率，并保存代理商的折扣率
                $data = M('operator')->field('id')->select();
                foreach ($data as $index => $datum) {
                    $one = array();
                    $one['agent_id'] = $result;
                    $one['is_agent'] = 1;
                    $one['operator_id'] = $datum['id'];
                    $result1 = M('mid_relation')->add($one);
                }
                $this->success('注册成功,跳到登陆页 ！', U('agent/login/index'), 3);
            } else {
                $this->error('注册失败');
            }
        } else {
            $agent_id = trim(I('get.agent_id'));
            if (empty($agent_id)) $this->error("链接地址错误，请重试。。。。");
            $condition['admin_id'] = $agent_id;
            $agent = M('agent');
            $list = $agent->where($condition)->find();
            if ($list['dengji'] == 3) {
                $this->error('请联系管理员，三级代理商，无法添加下级代理', U('home/index/index'));
            }
            $this->assign('agent_id', $agent_id);
            $this->display();
        }
    }

    //推广（会员）链接
    public function promoteUser()
    {
        if (IS_POST) {
            $agent_id = trim(I('post.agent_id'));
            $agent = M('agent');
            $condition['admin_id'] = $agent_id;
            $list = $agent->where($condition)->find();
            if (empty($list)) {
                $this->error('参数错误，代理商id不存在。。。');
            }
            $data = $_POST;
            $data['add_agent_time'] = date('Y-m-d H:i:s', time());

            $model = D('Company');
            if (!$data = $model->create($data)) {
                $this->error($model->getError(), '', 1);
            } else {
                //注册成功
                if ($insert_id = $model->add()) {
                    //设置默认折扣率，并保存会员的折扣率
                    $data = M('operator')->field('id')->select();
                    foreach ($data as $index => $datum) {
                        $one = array();
                        $one['agent_id'] = $insert_id;
                        $one['is_agent'] = 0;
                        $one['operator_id'] = $datum['id'];
                        $result1 = M('mid_relation')->add($one);
                    }
                    $this->success('用户注册成功', U('home/login/index'), 1);
                } else {
                    //注册失败
                    $this->error('用户注册失败');
                }
            }
        } else {
            $agent_id = trim(I('get.agent_id'));
            if (empty($agent_id)) $this->error("链接地址错误，请重试。。。。");
            $condition['admin_id'] = $agent_id;
            $condition['status'] = 1;
            $agent = M('agent');
            $list = $agent->where($condition)->find();
            if (empty($list)) {
                $this->error('代理商账户不存在', U('home/index/index'));
            }
            $this->assign('agent_id', $agent_id);
            $this->display();
        }

    }
}