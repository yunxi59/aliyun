<?php
namespace Home\Controller;
use Think\Controller;
class BaseController extends Controller{
	public function _initialize(){
		$config = aliyun();
		$this->assign('config',$config);
		//验证用户身份
        if(!isset($_SESSION['id'])){//没有登录，跳转到登录页面
            $this->error('未登录账号，请前往登录...',U('home/login/index',1));
        }
	}
}