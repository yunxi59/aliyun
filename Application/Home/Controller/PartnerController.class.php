<?php
namespace Home\Controller;
class PartnerController extends BaseController {

	//客户案例
    public function index()
    {
    	$case = M('excellent_case')->where('type=1')->limit(9)->order('id desc')->select();
    	foreach ($case as $key => $value) {
    		$names = $this->name($value['name']);
    		$case[$key]['logo'] = $names;
    	}
    	$this->assign('case',$case);//首页轮播
    	$this->display();
    }

    public function name($names){
        $name = M('excellent_case')->where('type=2')->select();
        foreach ($name as $key => $value) {
        	if($names == $value['name']){
        		return $value['img'];
        	}
        }
    }

    //客户案例列表
    public function lists()
    {
        //excellent_case
        $total = M('excellent_case')->where('type=1')->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,'');
        if($pageSize < $total){
            $show = $page->show();
        }
        $list = M('excellent_case')->where('type=1')->limit($page->firstRow,$page->listRows)->order('id desc')->select();
        $this->assign('list',$list);
        $this->assign('show',$show);
    	$this->display();
    }
    
    //客户案例详情
    public function detail()
    {
        if($_GET['id']){
           $where['id'] = $_GET['id'];
           $list = M('excellent_case')->where($where)->find();
           $this->assign('hot',$hot);
    	   $this->display();
        }else{
            $this->error("该新闻资讯不存在");
        }
    }
}