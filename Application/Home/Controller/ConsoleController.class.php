<?php
namespace Home\Controller;
class ConsoleController extends BaseController {
    public function index()
    {
        $list = M('user')->where(['id'=>session('id')])->find();
        $this->assign('list',$list);
    	$this->display();
    }
    
    //订单管理
    public function order()
    {
        $map['uid'] = session('id');
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if(!empty($keyword)){
        //设置查询条件,模糊查询
            $map['orderid'] = array('like',"%$keyword%");
        }
        //开始时间
        $starttime = strtotime(I('param.starttime')); 
        //结束时间
        $endtime = strtotime(I('param.endtime')); 
        if(!empty($starttime) && !empty($endtime)){
            $map['ordertime'] = array('between',[$starttime,$endtime]);
        }  
        //分类搜索
        if(is_numeric(I('param.state'))){
            $map['state'] = I('param.state');
        }  
        $order = M('order');
        $total = $order->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $orderlist = M('order')->where($map)->limit($page->firstRow,$page->listRows)->order('id desc')->select();
        $this->assign('orderlist',$orderlist);
        $this->assign('show',$show);
        $this->display();
    }

    //订单详情
    public function orderinfo()
    {
        $where['orderid'] = I('get.orderid');
        if(!empty($where)){
            $info = M('order')->where($where)->find();
            $info['ecs'] = M('ecs')->field('normsname')->where('id='.$info['eid'])->find();
            $shuju = explode(',', $info['shuju']);
            $shu = array();
            foreach ($shuju as $key => $value) {
                if($value == "cloud_efficiency"){
                    $shu[] = "高效云盘";
                }else{
                    $shu[] = "SSD云盘"; 
                }
            }
            $info['shuju'] = join(',',$shu);
            $arr['ImageId'] = $info['imageid'];
            $info['areaimg'] = M('areaimg')->field('osname')->where($arr)->find();
            $this->assign('list',$info);
        }  
        $this->display();
    }
    
    //充值管理
    public function recharge()
    {
        $map['uid'] = session('id');
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if(!empty($keyword)){
        //设置查询条件,模糊查询
            $map['orderid'] = array('like',"%$keyword%");
        } 
        //开始时间
        $starttime = strtotime(I('param.starttime')); 
        //结束时间
        $endtime = strtotime(I('param.endtime')); 
        if(!empty($starttime) && !empty($endtime)){
            $map['starttime'] = array('between',[$starttime,$endtime]);
        }   
        $map['state'] = 1;
        $order = M('recharge');
        $total = $order->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $orderlist = M('recharge')->where($map)->limit($page->firstRow,$page->listRows)->order('id desc')->select();
        $this->assign('orderlist',$orderlist);
        $this->assign('show',$show);
        $this->display();
    }

    //磁盘管理
    public function cipan()
    {
        $map['uid'] = session('id');
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if(!empty($keyword)){
        //设置查询条件,模糊查询
            $map['diskid'] = array('like',"%$keyword%");
        }  
        $total = M('disks')->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $list = M('disks')->where($map)->limit($page->firstRow,$page->listRows)->order('id desc')->select();
        foreach ($list as $key => $value) {
            $infos =  $this->infos($value['oid']);
            $list[$key]['ipaddress'] =  $infos['ipaddress'];
            $creationtime = $infos['creationtime'];
            $creationtime = date('Y-m-d',$creationtime);
            $time = $infos['period'];
            $list[$key]['stoptime'] = date("Y/m/d", strtotime("+".$time."months", strtotime($creationtime)));
            $list[$key]['pay'] = $infos['pay'];
        }
        $this->assign('list',$list);
        $this->assign('show',$show);
    	$this->display();
    }

    //订单信息
    public function infos($oid)
    {
        $info = M('order')->where(['id'=>$oid])->find();
        return $info;

    }

    public function editpwd()
    {
    	$this->display();
    }

    //IP带宽
    public function ip()
    {
    	$map['uid'] = session('id');
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if(!empty($keyword)){
        //设置查询条件,模糊查询
            $map['IpAddress|instanceid'] = array('like',"%$keyword%");
        }  
        //分类搜索
        if(is_numeric(I('param.state'))){
            $map['state'] = I('param.state');
        }
        $order = M('order');
        $total = $order->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $map['instanceid'] = ['neq',''];
        $orderlist = M('order')->where($map)->limit($page->firstRow,$page->listRows)->order('id desc')->select();
        foreach ($orderlist as $key => $value) {
            $creationtime = $value['creationtime'];
            $creationtime = date('Y-m-d',$creationtime);
            $time = $value['period'];
            $orderlist[$key]['stoptime'] = date("Y/m/d", strtotime("+".$time."months", strtotime($creationtime)));
        }
        $this->assign('orderlist',$orderlist);
        $this->assign('show',$show);
        $this->display();
    }

    //账单信息
    public function moneyInfo()
    {
        $map['uid'] = session('id');
        $time = I('param.time');
        //搜索关键词
        if(!empty($time) && is_numeric($time)){
        //设置查询条件,模糊查询
            if($time == 1){
               /* $map*/
            }
/*          dump(1);*/
        }  
        $map['pay'] = 1;
        $order = M('water');
        $total = $order->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $orderlist = M('water')->where($map)->limit($page->firstRow,$page->listRows)->order('id desc')->select();
        $this->assign('water',$orderlist);
        $this->assign('show',$show);
    	$this->display();
    }

    //云服务器
    public function yun()
    {
        $map['uid'] = session('id');
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if(!empty($keyword)){
        //设置查询条件,模糊查询
            $map['IpAddress|instanceid'] = array('like',"%$keyword%");
        }  
        //分类搜索
        if(is_numeric(I('param.state'))){
            $map['state'] = I('param.state');
        }
        $order = M('order');
        $total = $order->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $map['instanceid'] = ['neq',''];
        $orderlist = M('order')->where($map)->limit($page->firstRow,$page->listRows)->order('id desc')->select();
        foreach ($orderlist as $key => $value) {
            $creationtime = $value['creationtime'];
            $creationtime = date('Y-m-d',$creationtime);
            $time = $value['period'];
            $orderlist[$key]['stoptime'] = date("Y/m/d", strtotime("+".$time."months", strtotime($creationtime)));
        }
        $this->assign('orderlist',$orderlist);
        $this->assign('show',$show);
    	$this->display();
    }

    //查看云服务器详情
    public function yuninfo()
    {
        $where['orderid'] = I('get.orderid');
        if(!empty($where)){
            $info = M('order')->where($where)->find();
            $creationtime = $info['creationtime'];
            $creationtime = date('Y-m-d',$creationtime);
            $time = $info['period'];
            $info['stoptime'] = date("Y-m-d H:i:s", strtotime("+".$time."months", strtotime($creationtime)));
            $info['ecs'] = M('ecs')->field('normsname')->where('id='.$info['eid'])->find();
            $shuju = explode(',', $info['shuju']);
            $shu = array();
            foreach ($shuju as $key => $value) {
                if($value == "cloud_efficiency"){
                    $shu[] = "高效云盘";
                }else{
                    $shu[] = "SSD云盘"; 
                }
            }
            $info['shuju'] = join(',',$shu);
            $arr['ImageId'] = $info['imageid'];
            $info['areaimg'] = M('areaimg')->field('osname')->where($arr)->find();
            $this->assign('list',$info);
        }  
        $this->display();
    }

    //专有网络
    public function vpc()
    {
        $rarea = M('area')->select();
        $areaid = I('get.areaId');
        $map['uid'] = session('id');
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if(!empty($keyword)){
        //设置查询条件,模糊查询
            $map['routetableid|vpcid|vrouterid'] = array('like',"%$keyword%");
        }  
        if($areaid){
            $where['keyname'] = $areaid;
            $map['regionid'] = $areaid;
        }else{
            $where['keyname'] = "cn-qingdao";
            $map['regionid'] = "cn-qingdao";
        }
        $area = M('areainfo')->where($where)->select();
        $areas = M('area')->where($where)->find();
        $total = M('route')->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $list = M('vpc')->where($map)->select();
        $this->assign('rarea',$rarea);
        $this->assign('area',$area);
        $this->assign('areas',$areas);
        $this->assign('list',$list);
        $this->assign('show',$show);
        $this->display();
    }

    //路由表
    public function route()
    {
        $areaid = I('get.areaId');
        $user['uid'] = session('id');
        $map['uid'] = session('id');
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if(!empty($keyword)){
        //设置查询条件,模糊查询
            $map['routetableid|vpcid|vrouterid'] = array('like',"%$keyword%");
        }  
        if($areaid){
            $where['keyname'] = $areaid;
            $user['regionid'] = $areaid;
            $map['regionid'] = $areaid;
        }else{
            $where['keyname'] = "cn-qingdao";
            $user['regionid'] = "cn-qingdao";
            $map['regionid'] = "cn-qingdao";
        }
        //地区
        $area = M('areainfo')->where($where)->select();
        //分区
        $areas = M('area')->where($where)->find();
        //专有网络
        $route = M('vpc')->where($user)->select();

        $total = M('route')->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $list = M('route')->where($map)->select();
        foreach ($list as $key => $value) {
            $list[$key]['vpcname'] = $this->vpcidname($value['vpcid']);
            $list[$key]['vswitchid'] = $this->switchinfo($value['vpcid']);
        }
        $this->assign('area',$area);
        $this->assign('areas',$areas);
        $this->assign('route',$route);
        $this->assign('list',$list);
        $this->assign('show',$show);
        $this->display();
    }
    
    //vpc名字
    public function vpcidname($vpcid)
    {
        $info = M('vpc')->field('vpcid,vpcname')->select();
        foreach ($info as $key => $value) {
            if($vpcid == $value['vpcid']){
                return $value['vpcname'];
            }
        }
    }

    //已绑定的交换机
    public function switchinfo($vpcid)
    {
        $info = M('switch')->field('vpcid,vswitchid')->where(['vpcid'=>$vpcid])->select();
        $arr = [];
        foreach ($info as $key => $value) {
            $arr[] = $value['vswitchid'];
        }
        $res = join(';<br/>',$arr);
        return  $res ;
    }

    //删除交换机
    public function switchdel()
    {
        $data['id'] = 4;
        $switch = M('switch')->field('regionid,vswitchid')->where($data)->find();
        $vinfos = $this->DeleteVSwitch($switch['regionid'],$switch['vswitchid']);
        if($vinfos){
            $res = M('switch')->where($data)->delete();
            if($res){
                $this->ajaxReturn(['code'=>1,'msg'=>'删除成功']);
            }else{
                $this->ajaxReturn(['code'=>0,'msg'=>'删除失败']);
            }
        }else{
            $this->ajaxReturn(['code'=>0,'msg'=>'删除失败']);
        }
    }

    //交换机
    public function switchs()
    {
        $areaid = I('get.areaId');
        $user['uid'] = session('id');
        $map['uid'] = session('id');
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if(!empty($keyword)){
        //设置查询条件,模糊查询
            $map['vswitchid|vswitchname|vpcid'] = array('like',"%$keyword%");
        }  
        if($areaid){
            $where['keyname'] = $areaid;
            $user['regionid'] = $areaid;
            $map['regionid'] = $areaid;
        }else{
            $where['keyname'] = "cn-qingdao";
            $user['regionid'] = "cn-qingdao";
            $map['regionid'] = "cn-qingdao";
        }
        //地区
        $area = M('areainfo')->where($where)->select();
        //分区
        $areas = M('area')->where($where)->find();
        //专有网络
        $switchs = M('vpc')->where($user)->select();

        $total = M('switch')->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $list = M('switch')->where($map)->select();
        $this->assign('area',$area);
        $this->assign('areas',$areas);
        $this->assign('switchs',$switchs);
        $this->assign('list',$list);
        $this->assign('show',$show);
        $this->display();
    }

    //创建交换机
    public function switchadd()
    {
        $url1 = "cn-qingdao";
        $zoneid1 = "cn-qingdao-c";
        $cidrblock1 = "172.16.4.0/24";
        $vpcid1 = "vpc-m5ehwrqlfcmg7tzr428br";
        $vswitchname1 = "dfhtyuyyuyjuyiuy";
        $description1 = "";
        $resadd = M('switch')->where(['vpcid'=>$vpcid1,'cidrblock'=>$cidrblock1])->find();
        if($resadd){
            $this->ajaxReturn(['code'=>0,'msg'=>'创建失败,该网段已经存在']);
        }
        $infos = $this->CreateVSwitch($url1,$zoneid1,$cidrblock1,$vpcid1,$vswitchname1,$description1);
        if($infos['VSwitchId']){
            $datas1['uid'] = session('id');
            $datas1['username'] = session('username');
            $datas1['regionid'] = $url1;
            $area = M('area')->field('valuename')->where(['keyname'=>$datas1['regionid']])->find();
            $datas1['regionidname'] = $area['valuename'];
            $datas1['zoneid'] = $zoneid1;
            $zoneids = M('areainfo')->field('fenvaluename')->where(['fenname'=>$datas1['zoneid']])->find();
            $datas1['zoneidname'] = $zoneids['fenvaluename'];
            $datas1['cidrblock'] = $cidrblock1;
            $datas1['vpcid'] = $vpcid1;
            $datas1['vswitchname'] = $vswitchname1;
            $datas1['description'] = $description1;
            $datas1['vswitchid'] = $infos['VSwitchId'];
            $insert2 = M('switch')->add($datas1);
            if($insert2){
                $vswitchid1 = M('switch')->field('id,regionid,vswitchid')->where(['id'=>$insert2])->find();
                sleep(4);
                $vswitchid = $this->DescribeVSwitches($vswitchid1['regionid']);
                foreach ($vswitchid as $key => $value) {
                    if($value['VSwitchId'] == $vswitchid1['vswitchid']){ 
                        $time = strtotime($value['CreationTime']);
                        $datas['creationtime'] = $time;
                        $datas['routetableid'] = $value['RouteTable']['RouteTableId'];
                        $datas['routetabletype'] = $value['RouteTable']['RouteTableType'];
                        $datas['status'] = $value['Status'];  
                        if($value['IsDefault']){
                            $datas['isdefault'] = "true";
                        }else{
                            $datas['isdefault'] = "false";
                        }
                        $datas['resourcegroupid'] = $value['ResourceGroupId'];
                        $datas['availableipaddresscount'] = $value['AvailableIpAddressCount'];
                    }
                }
                $save = M('switch')->where(['id'=>$vswitchid1['id']])->save($datas);
                if($save){
                    $this->counts();
                    $this->ajaxReturn(['code'=>1,'msg'=>'创建成功']);
                }else{
                    $this->ajaxReturn(['code'=>0,'msg'=>'创建失败']);
                }
            }else{
                $this->ajaxReturn(['code'=>0,'msg'=>'交换机创建失败']);
            }
        }else{
            $this->ajaxReturn(['code'=>0,'msg'=>'交换机创建失败']);
        }
    }

    //开机
    public function start()
    {  
        if(IS_GET){
            $res['orderid'] = I('get.orderid');
            $order = M('order')->field('diyu,instanceid')->where($res)->find();
            if($order){
                $info =$this->StartInstance($order['diyu'],$order['instanceid']);
               /* $info['RequestId'] = "DEF711DA-3922-449F-A72F-94431EE58EEF";*/
                if(!empty($info['RequestId'])){
                    $data['Status'] = "Running";
                    $edit = M('order')->where($res)->save($data);
                    if($edit){
                       /* $this->redirect('home/console/yun');*/
                        $this->ajaxReturn(['code'=>1]);
                    }else{
                        $this->ajaxReturn(['code'=>0]);
                    }
                }
            }
        }
    }

    //关机
    public function stop()
    {
        if(IS_GET){
            $res['orderid'] = I('get.orderid');
            $order = M('order')->field('diyu,instanceid')->where($res)->find();
            if($order){
                $info =$this->StopInstance($order['diyu'],$order['instanceid']);
                if(!empty($info['RequestId'])){
                    $data['Status'] = "Stopped";
                    $edit = M('order')->where($res)->save($data);
                    if($edit){
                        /*$this->redirect('home/console/yun');*/
                        $this->ajaxReturn(['code'=>1]);
                    }else{
                        $this->ajaxReturn(['code'=>0]);
                    }
                }
            }
        }
    }

    //重启
    public function reboot()
    {
        if(IS_GET){
            $res['orderid'] = I('get.orderid');
            $order = M('order')->field('diyu,instanceid')->where($res)->find();
            if($order){
                $info = $this->RebootInstance($order['diyu'],$order['instanceid']);
                $this->ajaxReturn(['code'=>1]);
               /* if(!empty($info['RequestId'])){
                    $this->ajaxReturn(['code'=>1]);
                }else{
                    $this->ajaxReturn(['code'=>0]); 
                }*/
            }
        }
    }

    //启动一台指定的实例。实例状态必须为 已停止（Stopped），才可以调用该接口。
    public function StartInstance($url,$instanceid)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/StartInstance.php?url='.$url.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&instanceid='.$instanceid;  
        $tmpInfo = $this->curlget($url);
        return $tmpInfo;
    }

    //停止运行一台指定的实例。只有状态为 运行中（Running）的实例才可以进行此操作。
    public function StopInstance($url,$instanceid)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/StopInstance.php?url='.$url.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&instanceid='.$instanceid;  
        $tmpInfo = $this->curlget($url);
        return $tmpInfo;
    }

    //重启指定的实例。您只能重启状态为 运行中（Running）的 ECS 实例。
    public function RebootInstance($url,$instanceid)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/RebootInstance.php?url='.$url.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&instanceid='.$instanceid;  
        $tmpInfo = $this->curlget($url);
        return $tmpInfo;
    }
    
    //修改一台实例的部分信息(密码)
    public function passinfo()
    {
        if(IS_POST){
            $password = I('post.password');
            $where['orderid'] =  I('post.orderid');
            $order = M('order')->where($where)->find();
            $info = $this->ModifyInstanceAttribute($order['diyu'],$order['instanceid'],$password);
            if($info['RequestId']){
                $data['password'] = $password;
                $res = M('order')->where($where)->save($data);
                if($res){
                    $this->ajaxReturn(1); 
                }
            }else{
                $this->ajaxReturn(0); 
            }
        }
    }

    //修改一台实例的部分信息(密码)
    public function ModifyInstanceAttribute($url,$instanceid,$password)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/aliyuns/ModifyInstanceAttribute.php?url='.$url.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&instanceid='.$instanceid.'&password='.$password;  
        $tmpInfo = $this->curlget($url);
        return $tmpInfo;
    }

      //创建VPC
    public function CreateVpc($territory,$cidrblock,$vpcname,$description)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/aliyuns/CreateVpc.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&cidrblock='.$cidrblock.'&vpcname='.$vpcname.'&description='.$description;  
        $tmpInfo = $this->curlget($url);
  /*      $tmpInfo = $tmpInfo['Instances']['Instance'];*/
        return $tmpInfo;
    }
    
    //创建专有网络
    public function vpcadd()
    {
        $url = "cn-qingdao";
        $cidrblock = "172.16.0.0/12";
        $vpcname = "dhtytuytujy";
        $description = "";
        //创建专有网络
        $info = $this->CreateVpc($url,$cidrblock,$vpcname,$description);
/*        $info['VpcId'] = "vpc-m5ey686squ9dnfardcga7";*/
        if($info['VpcId']){
            $data1['uid'] = session('id');
            $data1['username'] = session('username');
            $data1['regionid'] = $url;
            $area = M('area')->field('valuename')->where(['keyname'=>$data1['regionid']])->find();
            $data1['regionidname'] = $area['valuename'];
            $data1['cidrblock'] = $cidrblock;
            $data1['vpcname'] = $vpcname;
            $data1['description'] =  $description;
            $data1['vpcid'] = $info['VpcId'];
           

            $data2['uid'] = session('id');
            $data2['username'] = session('username');
            $data2['regionid'] = $url;
            $area = M('area')->field('valuename')->where(['keyname'=>$data2['regionid']])->find();
            $data2['regionidname'] = $area['valuename'];
            $data2['vpcid'] = $info['VpcId'];
            $data2['vrouterid'] = $info['VRouterId'];
            $insert1 = M('vpc')->add($data1);
            $inserts1 = M('route')->add($data2);
/*            $insert1 = 4;*/
            if($insert1 && $inserts1){
                sleep(4);
                $vpcids1 = M('vpc')->field('id,regionid,vpcid')->where(['id'=>$insert1])->find();
                $vpcid = $this->DescribeVpcs($vpcids1['regionid']);
                foreach ($vpcid as $key => $value) {
                    if($value['VpcId'] == $vpcids1['vpcid']){
                        if($value['IsDefault']){
                            $data['isdefault'] = "true";
                        }else{
                            $data['isdefault'] = "false";
                        }
                        $data['resourcegroupid'] = $value['ResourceGroupId'];
                        $data['routertableids'] = $value['RouterTableIds']['RouterTableIds'][0];
                        $data['vpcid'] = $value['VpcId'];
                        $data['vrouterid'] = $value['VRouterId'];
                        $data['status'] = $value['Status'];
                        $time = strtotime($value['CreationTime']);
                        $data['creationtime'] = $time;
                    }
                }
                $save = M('vpc')->where(['id'=>$vpcids1['id']])->save($data);
                sleep(3);
                $rous = M('route')->field('id,regionid,vrouterid')->where(['id'=>$inserts1])->find();
                $rou = $this->DescribeVRouters($rous['regionid']);
                foreach ($rou as $key => $value) {
                    if($value['VRouterId'] == $rous['vrouterid']){
                        $datar['vroutername'] = $value['VRouterName'];
                        $datar['routetableid'] = $value['RouteTableIds']['RouteTableId'][0];
                        $datar['description'] = $value['Description'];
                        $time = strtotime($value['CreationTime']);
                        $datar['creationtime'] = $time;
                    }
                }
                $saver = M('route')->where(['id'=>$rous['id']])->save($datar);

                sleep(5);
                //创建交换机
                $vpcinfo = M('vpc')->field('vpcid')->where(['uid'=>session('id')])->order('id desc')->find();
                $url1 = "cn-qingdao";
                $zoneid1 = "cn-qingdao-c";
                $cidrblock1 = "172.16.0.0/24";
                $vpcid1 = $vpcinfo['vpcid'];
                $vswitchname1 = "dfhtyuyyuyjuyiuy";
                $description1 = "";
                $infos = $this->CreateVSwitch($url1,$zoneid1,$cidrblock1,$vpcid1,$vswitchname1,$description1);
                if($infos['VSwitchId']){
                    $datas1['uid'] = session('id');
                    $datas1['username'] = session('username');
                    $datas1['regionid'] = $url1;
                    $area = M('area')->field('valuename')->where(['keyname'=>$datas1['regionid']])->find();
                    $datas1['regionidname'] = $area['valuename'];
                    $datas1['zoneid'] = $zoneid1;
                    $zoneids = M('areainfo')->field('fenvaluename')->where(['fenname'=>$datas1['zoneid']])->find();
                    $datas1['zoneidname'] = $zoneids['fenvaluename'];
                    $datas1['cidrblock'] = $cidrblock1;
                    $datas1['vpcid'] = $vpcid1;
                    $datas1['vswitchname'] = $vswitchname1;
                    $datas1['description'] = $description1;
                    $datas1['vswitchid'] = $infos['VSwitchId'];
                    $insert2 = M('switch')->add($datas1);
                    if($insert2){
                        $vswitchid1 = M('switch')->field('id,regionid,vswitchid')->where(['id'=>$insert2])->find();
                        sleep(4);
                        $vswitchid = $this->DescribeVSwitches($vswitchid1['regionid']);
                        foreach ($vswitchid as $key => $value) {
                            if($value['VSwitchId'] == $vswitchid1['vswitchid']){ 
                                $time = strtotime($value['CreationTime']);
                                $datas['creationtime'] = $time;
                                $datas['routetableid'] = $value['RouteTable']['RouteTableId'];
                                $datas['routetabletype'] = $value['RouteTable']['RouteTableType'];
                                $datas['status'] = $value['Status'];  
                                if($value['IsDefault']){
                                    $datas['isdefault'] = "true";
                                }else{
                                    $datas['isdefault'] = "false";
                                }
                                $datas['resourcegroupid'] = $value['ResourceGroupId'];
                                $datas['availableipaddresscount'] = $value['AvailableIpAddressCount'];
                            }
                        }
                        $save = M('switch')->where(['id'=>$vswitchid1['id']])->save($datas);
                        if($save){
                            $this->counts();
                            $this->ajaxReturn(['code'=>1,'msg'=>'创建成功']);
                        }else{
                            $this->ajaxReturn(['code'=>0,'msg'=>'创建失败']);
                        }
                    }else{
                        $this->ajaxReturn(['code'=>0,'msg'=>'交换机创建失败']);
                    }
                }else{
                    $this->ajaxReturn(['code'=>0,'msg'=>'交换机创建失败']);
                }
            }else{
                $this->ajaxReturn(['code'=>0,'msg'=>'专有网络创建失败']);
            }
        }else{
            $this->ajaxReturn(['code'=>0,'msg'=>'专有网络创建失败']);
        }
    }
    
    //总量
    public function counts()
    {
        $vpcinfo = M('vpc')->field('uid,vpcid')->where(['uid'=>session('id')])->order('id desc')->find();
        $arr['rcount'] = M('switch')->where($vpcinfo)->count();
        $arr['scount'] = M('route')->where($vpcinfo)->count();
        $vpscount = M('vpc')->where(['vpcid'=>$vpcinfo['vpcid']])->save($arr);
    }

    public function cc()
    { 
/*        dump($arr);*/
/*         dump($vpcinfo['vpcid']);
        $url = "cn-qingdao";
        $cidrblock = "172.16.0.0/12";
        $vpcname = "dhtytuytujy";
        $description = "";*/
      /*  $info = $this->CreateVpc($url,$cidrblock,$vpcname,$description);*/
/*        $vpcid = "vpc-m5eo3ckvzxd9j8gaifzlm";
        $zoneid = "cn-qingdao-c";
        $cidrblock = "172.16.0.0/18";
        $vswitchname = "dfhtyuyytuyytu";*/
/*        $infos = $this->CreateVSwitch($url,$zoneid,$cidrblock,$vpcid,$vswitchname,$description);*/
/*        dump($infos);*/

       /* $a = $this->DescribeVpcs("cn-qingdao");
        dump($a);*/
        $rou = $this->DescribeVSwitches("cn-qingdao");
        dump($rou);
    }

    public function gg()
    {
        $vinfos = $this->DeleteVSwitch("cn-qingdao","vsw-m5ekm4zxp4blakqi5yeic");
        dump($vinfos);
        sleep(3);
        $vinfo = $this->DeleteVpc("cn-qingdao","vpc-m5exyr38wifu81xw90g6p");
        dump($vinfo);
    }

    //查询已创建的VPC
    public function DescribeVpcs($territory)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/aliyuns/DescribeVpcs.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'];  
        $tmpInfo = $this->curlget($url);
        $tmpInfo = $tmpInfo['Vpcs']['Vpc'];
        return $tmpInfo;
    }
    
    //删除一个专有网络（VPC）
    public function DeleteVpc($territory,$vpcid)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/aliyuns/DeleteVpc.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&vpcid='.$vpcid;  
        $tmpInfo = $this->curlget($url);
/*        $tmpInfo = $tmpInfo['Vpcs']['Vpc'];*/
        return $tmpInfo;
    }

    //查询指定地域的路由器列表
    public function DescribeVRouters($territory)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/aliyuns/DescribeVRouters.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'];  
        $tmpInfo = $this->curlget($url);
        $tmpInfo = $tmpInfo['VRouters']['VRouter'];
        return $tmpInfo;
    }

    //创建交换机
    public function CreateVSwitch($territory,$zoneid,$cidrblock,$vpcid,$vswitchname,$description)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/aliyuns/CreateVSwitch.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&zoneid='.$zoneid.'&cidrblock='.$cidrblock.'&vpcid='.$vpcid.'&vswitchname='.$vswitchname.'&description='.$description;  
        $tmpInfo = $this->curlget($url);
  /*      $tmpInfo = $tmpInfo['Instances']['Instance'];*/
        return $tmpInfo;
    }

    //查询已创建的交换机
    public function DescribeVSwitches($territory)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/aliyuns/DescribeVSwitches.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'];  
        $tmpInfo = $this->curlget($url);
        $tmpInfo = $tmpInfo['VSwitches']['VSwitch'];
        return $tmpInfo;
    }

    //删除交换机
    public function DeleteVSwitch($territory,$vswitchid)
    {
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/aliyuns/DeleteVSwitch.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&vswitchid='.$vswitchid;  
        $tmpInfo = $this->curlget($url);
/*        $tmpInfo = $tmpInfo['Vpcs']['Vpc'];*/
        return $tmpInfo;
    }

    //模拟GET请求
    public function curlget($url)
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        $tmpInfo = curl_exec($curl);     //返回api的json对象
        //关闭URL请求
        curl_close($curl);
        $tmpInfo = json_decode($tmpInfo,true);
        return $tmpInfo;
    }
    // 控制台实名认证
    public function realnameAuth(){
        $this->display();
    }

    // 云数据库RDS 的实例列表
    public function instancelist(){
        $this->display();
    }
    // 云数据库RDS 的回收站
    public function recyclebin(){
        $this->display();
    }
    // 云数据库RDS 的待处理的事
    public function dealtwith(){
        $this->display();
    }
    // 云数据库RDS 的历史事件
    public function historicalevent(){
        $this->display();
    }
    // 云数据库RDS 的ssl服务端
    public function sllserver(){
        $this->display();
    }
    // 云数据库RDS 的ssl客户端
    public function sllserverkh(){
        $this->display();
    }
    // 云数据库RDS 的对象储存OSS
    public function objectstoreoss(){
        $this->display();
    }
    // 云数据库RDS 的CDN
    public function yuncdn(){
        $this->display();
    }
    // 云数据库RDS 的弹性公网IP
    public function eips(){
        $this->display();
    }
    // 云数据库RDS 的申请弹性公网IP预付费
    public function publiceips(){
        $this->display();
    }
    // 云数据库RDS 的申请弹性公网IP后付费
    public function afterpubliceips(){
        $this->display();
    }
}