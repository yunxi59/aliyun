<?php
namespace Home\Controller;
class RechargeController extends BaseController {
    public function index()
    {
    	$list = M('user')->where(['id'=>session('id')])->find();
        $this->assign('list',$list);
    	$this->display();
    }


    //充值下单
    public function recharge()
    {
    	if(!session(id)){
        	$this->ajaxReturn(['code'=>0,'msg' => '未进行登录，无法下单，请前往下单']);
        }
        $orderid = date('YmdHis').rand(0,999999);
        $where['orderid'] = $orderid;//订单号
        $where['uid'] = session(id);//用户ID
        $where['username'] = session(username);//用户名
        $where['money'] = I('post.money');
        $where['starttime'] = time();
        $info = M('recharge')->add($where);
    	if($info){
                $wheres['uid'] = session(id);//用户ID
                $wheres['username'] = session(username);//用户名
                $wheres['orderid'] = $orderid;
                $wheres['money'] = "+".$where['money'];
                $wheres['state'] = 1;
                $info = M('water')->add($wheres);
            $this->ajaxReturn(['code'=>1,'msg' => '下单成功,正前往支付','data'=>$where['orderid']]);
    	}else{
            $this->ajaxReturn(['code'=>0,'msg' => '下单失败']);
    	}
    }

    //微信扫码支付
    public function weipay()
    {   
    	if(IS_GET){
    		$orderid = I('get.orderid');
    		$money = I('get.money');
    	    $url = $this->weipayinfo($orderid ,$money);
    	}
        $this->assign('url',$url);
        $this->display();
    }

    public function weipayinfo($orderid,$money){
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/pay/example/native.php?orderid='.$orderid.'&money='.$money;  
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        $tmpInfo = curl_exec($curl);     //返回api的json对象
        //关闭URL请求
        curl_close($curl);
        $tmpInfo = json_decode($tmpInfo,true);
        return $tmpInfo;
    }
    
    //判断微信是否已经支付
    public function paystatus(){
        if(IS_POST){
            $where = I('post.');
            $count = M('recharge')->field('state')->where($where)->find();
            if($count['state'] == 1){
                $this->ajaxReturn(1);
            }
        }
    }

    //充值成功页面
    public function rechargesuccess()
    {
    	$this->display();
    }

    //返回订单号
    public function order()
    {
        if(IS_GET){
            $where['orderid'] = I('get.orderid');
            $where['uid'] = session('id');
            $order = M('order')->where($where)->find();
            if($order){
                $recharge = M('recharge')->field('endtime')->where($where)->find();
                if($recharge['endtime']){
                    $this->ajaxReturn(['code'=>0]);
                }else{
                    $this->ajaxReturn(['code'=>1]);
                }
            }else{
                $this->ajaxReturn(['code'=>1]);
            }
        }
    }
}