<?php
namespace Home\Controller;
class NewController extends BaseController {
    //新闻资讯列表
    public function index()
    {
        //行业资讯
        $total = M('article')->where(['category_id'=>2])->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,'');
        if($pageSize < $total){
            $show = $page->show();
        }
        $list = M('article')->where(['category_id'=>2])->limit($page->firstRow,$page->listRows)->order('id desc')->select();
        $hot = M('article')->where(['category_id'=>2])->limit(5)->order('id desc')->select();
        $this->assign('hot',$hot);
        $this->assign('list',$list);
        $this->assign('show',$show);
    	$this->display();
    }
    
    //新闻资讯详情
    public function detail()
    {
        if($_GET['id']){
           $where['id'] = $_GET['id'];
           $list = M('article')->where($where)->find();
           $hot = M('article')->where(['category_id'=>2])->limit(5)->order('id desc')->select();
           $this->assign('hot',$hot);
           $this->assign('list',$list);
    	   $this->display();
        }else{
            $this->error("该新闻资讯不存在");
        }
    }

    public function joinus()
    {
    	$this->display();
    }
}