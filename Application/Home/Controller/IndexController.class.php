<?php
namespace Home\Controller;
ini_set('max_execution_time',0);
set_time_limit(0);
class IndexController extends BaseController {
    public function index(){
        //首页轮播
        $banner = M('excellent_case')->where('type=3')->limit(3)->order('id desc')->select();

        //案例轮播
        $case = M('excellent_case')->where('type=1')->limit(10)->order('id desc')->select();

        //网站公告
        $notice = M('article')->where(['category_id'=>1])->limit(3)->order('id desc')->select();
        foreach ($notice as $key => $value) {
            $time = strtotime($value['add_time']);
            $notice[$key]['month'] = date('Y.m',$time);
            $notice[$key]['day'] = date('d',$time);
        }

        //行业资讯
        $hangye = M('article')->where(['category_id'=>2])->limit(3)->order('id desc')->select();
        foreach ($hangye as $key => $value) {
            $time = strtotime($value['add_time']);
            $hangye[$key]['month'] = date('Y.m',$time);
            $hangye[$key]['day'] = date('d',$time);
        }
        $this->assign('banner',$banner);//首页轮播
        $this->assign('case',$case);//首页轮播
        $this->assign('hangye',$hangye);//行业资讯
        $this->assign('notice',$notice);//网站公告
    	$this->display();
    }
    //实例规格
    public function example(){
    	$url = file_get_contents("http://g.alicdn.com/aliyun/ecs-price-info/2.0.11/price/download/instancePrice.json?spm=0.6883001.instance.1.709eSyc8Syc8o8&file=instancePrice.json");
	    $res = json_decode($url,true);
	    $arr = $res['pricingInfo'];
	    foreach ($arr as $key => $value) {
	    	$data = explode('::', $key);
	    	$res1 = explode('.', $data[1]);
	    	$str = strpos($res1[1],"-");
	    	if($str){
	    	    $res2= explode('-', $res1[1]);
	    	    $res1[1] = $res2[0];
	    	}
	    	$name1 = "";   
	    	$king = "";
	    	/*企业级x86计算规格族群*/
	    	//通用型
	    	if($res1[1] == "g5"){
	    		$name1 = "通用型";
	    		$king = 1;
	    	}
	    	//通用网络增强型
	    	if($res1[1] == "sn2ne"){
	    		$name1 = "通用网络增强型";
	    		$king = 1;
	    	}
	    	//密集计算型
	    	if($res1[1] == "ic5"){
	    		$name1 = "密集计算型";
	    		$king = 1;
	    	}
	    	//计算型
	    	if($res1[1] == "c5"){
	    		$name1 = "计算型";
	    		$king = 1;
	    	}
	    	//计算网络增强型
	    	if($res1[1] == "sn1ne"){
	    		$name1 = "计算网络增强型";
	    		$king = 1;
	    	}
	    	//内存型
	    	if($res1[1] == "r5"){
	    		$name1 = "内存型";
	    		$king = 1;
	    	}
	    	//内存增强型
	    	if($res1[1] == "re4"){
	    		$name1 = "内存增强型";
	    		$king = 1;
	    	}
	    	//内存网络增强型
	    	if($res1[1] == "se1ne"){
	    		$name1 = "内存网络增强型";
	    		$king = 1;
	    	}
	    	//内存型
	    	if($res1[1] == "se1"){
	    		$name1 = "内存型";
	    		$king = 1;
	    	}
	    	//大数据网络增强型
	    	if($res1[1] == "d1ne"){
	    		$name1 = "大数据网络增强型";
	    		$king = 1;
	    	}
	    	//大数据型
	    	if($res1[1] == "d1"){
	    		$name1 = "大数据型";
	    		$king = 1;
	    	}
	    	//本地SSD型
	    	if($res1[1] == "i2" || $res1[1] == "i2g" || $res1[1] == "i1"){
	    		$name1 = "本地SSD型";
	    		$king = 1;
	    	}
	    	//高主频计算型
	    	if($res1[1] == "hfc5"){
	    		$name1 = "高主频计算型";
	    		$king = 1;
	    	}
	    	//高主频通用型
	    	if($res1[1] == "hfg5"){
	    		$name1 = "高主频通用型";
	    		$king = 1;
	    	}

	    	/*企业级异构计算规格族群*/
	    	//GPU计算型
	    	if($res1[1] == "gn6v" || $res1[1] == "gn5" || $res1[1] == "gn5i" || $res1[1] == "gn4"){
	    		$name1 = "GPU计算型";
	    		$king = 2;
	    	}
	    	//GPU可视化计算型
	    	if($res1[1] == "ga1"){
	    		$name1 = "GPU可视化计算型";
	    		$king = 2;
	    	}
	    	//FPGA计算型
	    	if($res1[1] == "f1" || $res1[1] == "f2"){
	    		$name1 = "FPGA计算型";
	    		$king = 2;
	    	}

	    	/*弹性裸金属服务器（神龙）和超级计算集群（SCC）实例规格族群*/
	    	//高主频型弹性裸金属服务器
	    	if($res1[1] == "ebmhfg5"){
	    		$name1 = "高主频型弹性裸金属服务器";
	    		$king = 3;
	    	}
	    	//计算型弹性裸金属服务器
	    	if($res1[1] == "ebmc4"){
	    		$name1 = "计算型弹性裸金属服务器";
	    		$king = 3;
	    	}
	    	//通用型弹性裸金属服务器
	    	if($res1[1] == "ebmg5"){
	    		$name1 = "";
	    		$king = 3;
	    	}
	    	//高主频型超级计算集群
	    	if($res1[1] == "scch5"){
	    		$name1 = "高主频型超级计算集群";
	    		$king = 3;
	    	}
	    	//通用型超级计算集群
	    	if($res1[1] == "sccg5"){
	    		$name1 = "通用型超级计算集群";
	    		$king = 3;
	    	}

	    	/*入门级x86计算规格族群*/
	    	//突发性能
	    	if($res1[1] == "t5"){
	    		$name1 = "突发性能实例";
	    		$king = 4;
	    	}
	    	//共享基本型实例
	    	if($res1[1] == "xn4"){
	    		$name1 = "共享基本型实例";
	    		$king = 4;
	    	}
	    	//共享计算型实例
	    	if($res1[1] == "n4"){
	    		$name1 = "共享计算型实例";
	    		$king = 4;
	    	}
	    	//共享通用型实例
	    	if($res1[1] == "mn4"){
	    		$name1 = "共享通用型实例";
	    		$king = 4;
	    	}
	    	//共享内存型实例
	    	if($res1[1] == "e4"){
	    		$name1 = "共享内存型实例";
	    		$king = 4;
	    	}
	    	$arrs['rules'] = $key;
	    	$arrs['rule'] = $data[1];
	    	$arrs['names'] = $name1;
	    	if($res1[1] == "xn4" || $res1[1] == "n4" || $res1[1] == "mn4" || $res1[1] == "e4"){
	    		$arrs['comking'] = 0;
	    	}else{
	    		$arrs['comking'] = 1;
	    	}
	    	$arrs['nameking'] = $king;
		    $arrs['normsname'] = $name1.'('.$res1[1].')'.$data[1];
		    $arrs['territory'] = $data[0];
		    $arrs['system'] = $data[3];
		    $arrs['optimized'] = $data[4];
	    	$arrs['hours'] = $value['hours'][0]['price'];
	    	$arrs['weeks'] = $value['weeks'][0]['price'];
	    	$arrs['months'] = $value['months'][0]['price'];
	    	$arrs['years1'] = $value['years'][0]['price'];
	    	$arrs['years2'] = $value['years'][1]['price'];
	    	$arrs['years3'] = $value['years'][2]['price'];
	    	$arrs['years4'] = $value['years'][3]['price'];
	    	$arrs['years5'] = $value['years'][4]['price'];
            $arrs['wang'] = $data['2'];
	    	$count = M('ecs')->where(['rules'=>$key])->find();
	    	if(!$count){
	    		M('ecs')->add($arrs);
	    	}else{
	    		$count = M('ecs')->where($arrs)->find();
	    		if(!$count){
                   M('ecs')->where(['rules'=>$key])->save($arrs);
	    		}
	    	}
	    }
    }
    
    //实例数据规格遍历
    public function getinfo(){
    	/*for ($i=0; $i < 6000; $i++) { 
    		$info = $this->much($i);
    		if($i%10 == 0){
    		   $info = $this->much($i);
    		   sleep(10);
    		}
    	}*/
    	$this->much(17,19);
    }
    
    //实例数据修改
    public function much(/*$i*/$start,$end){;
    	$res = M('ecs')->field('id,rules')->limit($start,$end)->select();
    /*	$res = M('ecs')->field('id,rules')->where('id='.$i)->select();*/
        if($res){
	    	foreach ($res as $key => $value) {
	    		$data = explode('::', $value['rules']);
		    	$res1 = explode('.', $data[1]);
		    	$str = strpos($res1[1],"-");
		    	if($str){
		    	    $res2= explode('-', $res1[1]);
		    	    $res1[1] = $res2[0];
		    	}
		    	$msg['id'] = $value['id'];
                $msg['territorys'] = $this->DescribeRegions($data[0]);
                $arr = $res1[0].'.'.$res1[1];
                $infomsg = $this->DescribeInstanceTypes($data[0],$data[1],$arr);
                $msg['cpu'] = $infomsg['cpu'];
                $msg['ram'] = $infomsg['ram'];
                $msg['state'] = $this->DescribeAvailableResource($data[0],$data[4],$data[1]);
	    	    $count = M('ecs')->where($msg)->find();
		    	if(!$count){
		    		M('ecs')->where(['id'=>$value['id']])->save($msg);
		    	}  
	    	}
    	}
    }

    public function aa(){
        $aa = $this->DescribeAvailableResource("cn-hangzhou","optimized","");
        dump($aa);
    }

    //产品是否停售
    public function DescribeAvailableResource($url,$optimized,$instancetype){
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/DescribeAvailableResource.php?url='.$url.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&optimized='.$optimized.'&instancetype='.$instancetype;  
        $tmpInfo = $this->curlget($url);
        $tmpInfo = $tmpInfo['AvailableZones']['AvailableZone'][0]['Status'];
        return $tmpInfo;
    }

    //产品是否停售
    public function DescribeAvailableResources($url,$optimized){
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/DescribeAvailableResources.php?url='.$url.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&optimized='.$optimized;  
        $tmpInfo = $this->curlget($url);
        $tmpInfo = $tmpInfo['AvailableZones']['AvailableZone'];
        return $tmpInfo;
    }
    
    //地区匹配
    public function DescribeRegions($territory){
    	$values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/DescribeRegions.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'];  
        $tmpInfo = $this->curlget($url);
        $tmpInfo = $tmpInfo['Regions']['Region'];
        foreach ($tmpInfo as $key => $value) {
            if($value['RegionId'] == $territory){
                return $value['LocalName'];
            }
        }
    }
    
    //cpu、内存匹配
    public function DescribeInstanceTypes($url,$datas,$areas){
    	$values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/DescribeInstanceTypes.php?url='.$url.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'].'&InstanceTypeFamily='.$areas;  
        $tmpInfo = $this->curlget($url);
        $tmpInfo = $tmpInfo['InstanceTypes']['InstanceType'];
        foreach ($tmpInfo as $key => $value) {
            if($value['InstanceTypeId'] == $datas){
                $msg['cpu'] = $value['CpuCoreCount'];
                $msg['ram'] = $value['MemorySize'];
                return $msg;;
            }
        }
    }

    //镜像
    public function DescribeImages($url){
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/DescribeImages.php?url='.$url.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'];
        $tmpInfo = $this->curlget($url);
        $tmpInfo = $tmpInfo['Images']['Image'];
        return  $tmpInfo;
    }
    
    //地区分配
    public function area(){
        $info = $this->diqu("cn-hangzhou");
    	foreach ($info as $key => $value) {
    		$arrs['keyname'] = $value['RegionId'];
    		$arrs['valuename'] = $value['LocalName'];
    		$count = M('area')->where(['keyname'=>$value['RegionId']])->find();
    		$msg = explode('-', $value['RegionId']);
            if($msg[0] == 'cn'){
                $arrs['status'] = 0;
            }else{
                $arrs['status'] = 1;
            }
            $arrs['fen'] = 0;
            if($msg[0] == 'us' || $msg[0] == 'eu'){
                $arrs['fen'] = 1;//欧洲与梅州
            }
            if($msg[0] == 'me'){
                $arrs['fen'] = 2;
            }
            if($value['RegionId'] == 'ap-south-1'){
                $arrs['fen'] = 2;
            }
	    	if(!$count){
	    		M('area')->add($arrs);
	    	}else{
	    		$count = M('area')->where($arrs)->find();
	    		if(!$count){
                   M('area')->where(['keyname'=>$value['RegionId']])->save($arrs);
	    		}
	    	}
    	}
    }

    //磁盘价格
    public function disk(){
    	$res = file_get_contents("https://g.alicdn.com/aliyun/ecs-price-info/2.0.13/price/download/diskPrice.json?spm=0.6883001.disk.1.4ef5ElxwElxwyw&file=diskPrice.json");
    	$info = json_decode($res,true);
    	$infos = $info['pricingInfo'];
    	foreach ($infos as $key => $value) {
    		$arrs['url'] = $key;
    		$abs = explode("::", $key);
    		$arrs['area'] = $abs[0];
    		$arrs['play'] = $abs[2];
    		$valueh = $value['hours'][0]['value'];
    		if($abs[2] == "system"){
    			$name = "系统盘";
    		}else{
    			$name = "数据盘";
    		}
    		if($valueh == "40"){
    			$name1 = "（40 GB起售价）";
    		}else{
    			$name1 = "（线性计费）";
    		}
    		$arrs['playname'] = $name.$name1;
    		$arrs['king'] = $abs[1];
    		if($abs[1] == "cloud"){
    			$arrs['kingname'] = "普通云盘";
    		}elseif($abs[1] == "cloud_efficiency"){
    			$arrs['kingname'] = "高效云盘";
    		}elseif($abs[1] == "cloud_ssd"){
    			$arrs['kingname'] = "SSD云盘";
    		}elseif($abs[1] == "ephemeral_ssd"){
    			$arrs['kingname'] = "本地SSD盘";
    		}else{
    			$arrs['kingname'] = "本地盘";
    		}
    		$arrs['hoursb'] = $value['hours'][0]['value'];
    		if($value['hours'][1]['value'] != ""){
    		    $arrs['hourss'] = $value['hours'][1]['value'];
    		}else{
    			$arrs['hourss'] = "";
    		}
    		$arrs['hourspriceb'] = $value['hours'][0]['price'];
    		if($value['hours'][1]['price'] != ""){
    		    $arrs['hoursprices'] = $value['hours'][1]['price'];
    		}else{
    			$arrs['hoursprices'] = "";
    		}
    		$arrs['hoursperiodb'] = $value['hours'][0]['period'];
    		if($value['hours'][1]['price'] != ""){
    		    $arrs['hoursperiods'] = $value['hours'][1]['period'];
    		}else{
    			$arrs['hoursperiods'] = "";
    		}
    		$arrs['hoursunitb'] = $value['hours'][0]['unit'];
    		if($value['hours'][1]['unit'] != ""){
    		    $arrs['hoursunits'] = $value['hours'][1]['unit'];
    		}else{
    			$arrs['hoursunits'] = "";
    		}

    		$arrs['weeksb'] = $value['weeks'][0]['value'];
    		if($value['weeks'][1]['value'] != ""){
    		    $arrs['weekss'] = $value['weeks'][1]['value'];
    		}else{
    			$arrs['weekss'] = "";
    		}
    		$arrs['weekspriceb'] = $value['weeks'][0]['price'];
    		if($value['weeks'][1]['price'] != ""){
    		    $arrs['weeksprices'] = $value['weeks'][1]['price'];
    		}else{
    			$arrs['weeksprices'] = "";
    		}
    		$arrs['weeksperiodb'] = $value['weeks'][0]['period'];
    		if($value['weeks'][1]['period'] != ""){
    		    $arrs['weeksperiods'] = $value['weeks'][1]['period'];
    		}else{
    			$arrs['weeksperiods'] = "";
    		}
    		$arrs['weeksunitb'] = $value['weeks'][0]['unit'];
    		if($value['weeks'][1]['unit'] != ""){
    		    $arrs['weeksunits'] = $value['weeks'][1]['unit'];
    		}else{
    			$arrs['weeksunits'] = "";
    		}

    		$arrs['monthsb'] = $value['months'][0]['value'];
    		if($value['months'][1]['value'] != ""){
    		    $arrs['monthss'] = $value['weeks'][1]['value'];
    		}else{
    			$arrs['monthss'] = "";
    		}
    		$arrs['monthspriceb'] = $value['months'][0]['price'];
    		if($value['months'][1]['price'] != ""){
    		    $arrs['monthsprices'] = $value['weeks'][1]['price'];
    		}else{
    			$arrs['monthsprices'] = "";
    		}
    		$arrs['monthsperiodb'] = $value['months'][0]['period'];
    		if($value['months'][1]['period'] != ""){
    		    $arrs['monthsperiods'] = $value['weeks'][1]['period'];
    		}else{
    			$arrs['monthsperiods'] = "";
    		}
    		$arrs['monthsunitb'] = $value['months'][0]['unit'];
    		if($value['months'][1]['unit'] != ""){
    		    $arrs['monthsunits'] = $value['weeks'][1]['unit'];
    		}else{
    			$arrs['monthsunits'] = "";
    		}
    		$count = M('disk')->where(['url'=>$key])->find();
	    	if(!$count){
	    		M('disk')->add($arrs);
	    	}else{
	    		$count = M('disk')->where($arrs)->find();
	    		if(!$count){
                   M('disk')->where(['url'=>$key])->save($arrs);
	    		}
	    	}
    	}
    }

    //磁盘数据修改
    public function diskdata(){
    	$res = M('disk')->field('id,area')->select();
    	foreach ($res as $key => $value) {
    		$msg['id'] = $value['id'];
    		$msg['areaname'] = $this->DescribeRegionsdisk($value['area']);
    	    $count = M('disk')->where($msg)->find();
	    	if(!$count){
	    		M('disk')->where(['id'=>$value['id']])->save($msg);
	    	} 
    	}
    }

    //磁盘地区匹配
    public function DescribeRegionsdisk($territory){
        $info = $this->diqu($territory);
    	foreach ($info as $key => $value) {
    		if($value['RegionId'] == $territory){
	    		return $value['LocalName'];
	    	}
    	}
    }

    //带宽价格
    public function bandwidth(){
    	$sql = file_get_contents("https://g.alicdn.com/aliyun/ecs-price-info/2.0.13/price/download/bandWidthPrice.json?spm=0.6883001.band.1.f7e1iDKqiDKqib&file=bandWidthPrice.json");
    	$res = json_decode($sql,true);
    	$info = $res['pricingInfo'];
    	foreach ($info as $key => $value) {
    		$arrs['area'] = $key;
    		$arrs['areaname'] = $this->DescribeRegionsdisk($key);
    		$arrs['hours1price'] = $value['hours'][0]['price'];
    		$arrs['hours1period'] = $value['hours'][0]['period'];

    		$arrs['hours2price'] = $value['hours'][1]['price'];
    		$arrs['hours2period'] = $value['hours'][1]['period'];

    		$arrs['months1value'] = $value['months'][0]['value'];
            $arrs['months1price'] = $value['months'][0]['price'];
            $arrs['months1period'] = $value['months'][0]['period'];
            $arrs['months1unit'] = $value['months'][0]['unit'];

            $arrs['months2value'] = $value['months'][1]['value'];
            $arrs['months2price'] = $value['months'][1]['price'];
            $arrs['months2period'] = $value['months'][1]['period'];
            $arrs['months2unit'] = $value['months'][1]['unit'];

            $arrs['months3value'] = $value['months'][2]['value'];
            $arrs['months3price'] = $value['months'][2]['price'];
            $arrs['months3period'] = $value['months'][2]['period'];
            $arrs['months3unit'] = $value['months'][2]['unit'];

            $arrs['months4value'] = $value['months'][3]['value'];
            $arrs['months4price'] = $value['months'][3]['price'];
            $arrs['months4period'] = $value['months'][3]['period'];
            $arrs['months4unit'] = $value['months'][3]['unit'];

            $arrs['months5value'] = $value['months'][4]['value'];
            $arrs['months5price'] = $value['months'][4]['price'];
            $arrs['months5period'] = $value['months'][4]['period'];
            $arrs['months5unit'] = $value['months'][4]['unit'];

            $arrs['months6value'] = $value['months'][5]['value'];
            $arrs['months6price'] = $value['months'][5]['price'];
            $arrs['months6period'] = $value['months'][5]['period'];
            $arrs['months6unit'] = $value['months'][5]['unit'];

            $arrs['trafficprice'] = $value['months'][5]['price'];
            $arrs['trafficperiod'] = $value['months'][5]['period'];
            $arrs['trafficunit'] = $value['months'][5]['unit'];

            $count = M('bandwidth')->where(['area'=>$key])->find();
	    	if(!$count){
	    		M('bandwidth')->add($arrs);
	    	}else{
	    		$count = M('bandwidth')->where($arrs)->find();
	    		if(!$count){
                   M('bandwidth')->where(['area'=>$key])->save($arrs);
	    		}
	    	}
    	}
    }

    //宽带地区匹配
    public function diqu($territory){
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/DescribeRegions.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'];  
        $tmpInfo = $this->curlget($url);
        $info = $tmpInfo['Regions']['Region'];
        return $info;
    }

    //镜像
    public function DescribeImagesinfo(){
    	$infos  = $this->diqu("cn-hangzhou");
        foreach ($infos as $k => $v) {
            $info = $this->DescribeImages($v['RegionId']);
    		foreach ($info as $key => $value) {
                $arr['keyname'] = $v['RegionId'];
                $arr['valuename'] = $v['LocalName'];
                $arr['ImageId'] = $value['ImageId'];
                $arr['OSName'] = $value['OSName'];
                $arr['Platform'] = $value['Platform'];
	    	    $count = M('areaimg')->where(['ImageId'=>$arr['ImageId']])->find();
	    	    if(!$count){
                    M('areaimg')->add($arr);
	    	    }else{
	    	    	$count = M('areaimg')->where($arr)->find();
		    		if(!$count){
	                   M('areaimg')->where(['ImageId'=>$arr['ImageId']])->save($arr);
		    		}
	    	    }
    		}
    	}
    }

    //地区分区
    public function areainfo(){
       $infos  = $this->diqu("cn-hangzhou");
       foreach ($infos as $key => $value) {
            $info = $this->DescribeZones($value['RegionId']);
            foreach ($info as $keys => $values) {
                $arr['keyname'] = $value['RegionId'];
                $arr['valuename'] = $value['LocalName'];
                $arr['fenname'] = $values['ZoneId'];
                $arr['fenvaluename'] = $values['LocalName'];
                $count =  M('areainfo')->where(['fenname'=>$arr['fenname']])->find();
                if(!$count){
                    M('areainfo')->add($arr);
                }else{
                    $count = M('areainfo')->where($arr)->find();
                    if(!$count){
                       M('areainfo')->where(['fenname'=>$arr['fenname']])->save($arr);
                    }
                }
            }
       }
    }

    //地区分区信息筛选
    public function ecsinfo(){
       $infos  = $this->diqu("cn-hangzhou");
       foreach ($infos as $key => $value) {
            $info = $this->DescribeZones($value['RegionId']);
            foreach ($info as $keys => $values) {
              
                $res = $values['AvailableResources']['ResourcesInfo'][0]['InstanceTypes']['supportedInstanceType'];
                foreach($res as $key => $v) {
                    $arr['keyname'] = $value['RegionId'];
                    $arr['valuename'] = $value['LocalName'];
                    $arr['fenname'] = $values['ZoneId'];
                    $arr['fenvaluename'] = $values['LocalName'];
                    $arr['rule'] = $v;
                    $count = M('ecsinfo')->where($arr)->find();
                    if(!$count){
                        M('ecsinfo')->add($arr);
                    }
                }
            }
       }
    }

    public function bb(){
       /* $info = M('ecsinfo')->where(['fenname'=>'cn-qingdao-c'])->select();*/
        $infos  = $this->diqu("cn-hangzhou");
        foreach ($infos as $key => $value) {
            $info = $this->DescribeAvailableResources($value['RegionId'],'optimized');
            foreach ($info as $key => $v) {
                $res = $v['AvailableResources']['AvailableResource'][0]['SupportedResources']['SupportedResource'];
                foreach ($res as $key => $va) {
                    $arr['keyname'] = $v['RegionId'];
                    $arr['valuename'] = $value['LocalName'];
                    $arr['fenname'] = $v['ZoneId'];
                    $arr['rule'] = $va['Value'];
                    $arr['status'] = $va['Status'];
                    $count = M('ecsinfo')->where($arr)->find();
                    if(!$count){
                        M('ecsinfo')->add($arr);
                    }
                }
            }
        }
    }  

    public function DescribeZones($territory){
        $values = aliyun();
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/DescribeZones.php?url='.$territory.'&accesskeyid='.$values['accesskeyid'].'&accesssecret='.$values['accesssecret'];  
        $tmpInfo = $this->curlget($url);
        $tmpInfo = $tmpInfo['Zones']['Zone'];
        return $tmpInfo;
    }

    //基本配置
    public function weixin(){
/*        $info = A('Pay')->weixin();
        dump($info);*/
        $values = aliyun();
        $this->ajaxReturn($values);
    }
    
    //支付宝支付基本配置
    public function alipay(){
        $list = M('alipay')->field('keys,value')->select();
        $info = array();
        foreach ($list as $key => $value) {
            $info[$value['keys']] = $value['value'];
        }
        $info['app_id'] = $info['appid'];
        $info['merchant_private_key'] = $info['merchant'];
        $info['notify_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/alipay/notify_url.php';
        $info['return_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/index.php?m=home&c=recharge&a=rechargesuccess';
        $info['charset'] = "UTF-8";
        $info['sign_type'] = "RSA2";
        $info['gatewayUrl'] = "https://openapi.alipay.com/gateway.do";
        $info['alipay_public_key'] = $info['alipaykey'];
        unset($info['appid']);
        unset($info['merchant']);
        unset($info['alipaykey']);
        $this->ajaxReturn($info);
    }

    public function cc(){
        $where['territory'] = "cn-qingdao";
        $b = M('ecs')->field('id,rule,territory')->where($where)->count();
        $where['territory'] = "cn-qingdao";
      /*  $where['fenname'] = "cn-qingdao-b";*/
      /*  $where['']*/
        $a = M('ecstu')->field('id,rule,fenname,status,territory')->where($where)->count();

/*
        $Model = M('ecs');
        $Model->alias('a')->join('ecsinfo b ON b.user_id= a.id')->select();*/
/*
        select a.*,b.fenname,b.status from h_ecs a left join h_ecsinfo b on a.rule=b.rule  and a.territory=b.keyname;*/
        /*select a.fenname,a.status,b.* from h_ecsinfo a left join h_ecs b on a.rule=b.rule and a.keyname=b.territory*/
       /* select b.fenname,a.*,b.status from h_ecsinfo b inner join h_ecs a on a.rule=b.rule  and a.territory=b.keyname*/
        dump($b);
        dump($a);
    }

      public function dd(){
        $where['keyname'] = "cn-qingdao";
        $where['fenname'] = "cn-qingdao-b";
        $b = M('ecsinfo')->where($where)->select();
        dump($b);
    }

    //模拟GET请求
    public function curlget($url)
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        $tmpInfo = curl_exec($curl);     //返回api的json对象
        //关闭URL请求
        curl_close($curl);
        $tmpInfo = json_decode($tmpInfo,true);
        return $tmpInfo;
    }


}