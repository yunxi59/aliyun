<?php
namespace Home\Controller;
use Think\Controller;
class ServiceController extends BaseController {
 /*   public function _initialize(){
        //验证用户身份
        if(!isset($_SESSION['id'])){//没有登录，跳转到登录页面
            $this->error('未登录账号，请前往登录...',U('home/login/index',1));
        }
    }
*/
    public function index()
    {
    	//国内地区
    	$area = M('area')->select();
    	//国外地区
    	$areas = M('area')->where('status = 1')->select();

        $keyname = "cn-qingdao";
        $info = M('areainfo')->where(['keyname'=>$keyname])->select();

    	//ecs信息列表
    /*	$rule = 'ecs.n4.2xlarge';*/
/*    	$where['rule'] = ['like','%'.$rule.'%'];
    	$where['names'] = ['neq',' '];
    	$where['territory'] = ['eq','cn-qingdao'];
    	$where['state'] = ['eq','Available'];
    	$ecs = M('ecs')->where($where)->limit(0,10)->select();
    	dump($ecs);*/
    	//国内地区
    	$this->assign('area',$area);
    	//国外地区
    	$this->assign('areas',$areas);
        $this->assign('info',$info);
    	$this->display();
    }

    public function info()
    {

    	//ecs信息列表
    /*	$rule = 'ecs.n4.2xlarge';*/
    /*	$where['rule'] = ['like','%'.$rule.'%'];*/
        if(IS_POST){
        	//ecs数据信息
        	$page = I('post.page');
            $limit = I('post.limit');
            $offset = ($page - 1) * $limit;
        	$area = I('post.jival');//地域
          
        	$nameking = I('post.jiagouval');//架构
        	if($nameking == 1){
        		$where['nameking'] = ['in','1,4'];
        	}else{
        		$where['nameking'] = ['eq',$nameking];
        	}
        	if(I('post.childval')){  		
             	$jffsval = I('post.childval');//仅显示最新一代
        	}else{
        		$jffsval = 1;//仅显示最新一代
        	}
        	$names = I('post.fenleival');
        	if($jffsval == 1 && $nameking == 1){
        		if($names == "高主频型"){
        			$where['names'] = ['eq',"高主频通用型"];
        		}elseif($names == "入门级（共享）"){
        			$where['names'] = ['eq',"突发性能实例"];
        		}else{
        			$where['names'] = ['eq',$names];
        		}
        	}elseif(($jffsval == 1 || $jffsval == 2) && $nameking == 2){
        		if($names == "GPU图形加速"){
        			$where['names'] = ['eq',"GPU可视化计算型"];
        		}else{
        			$where['names'] = ['eq',$names];
        		}
        	}elseif(($jffsval == 1 || $jffsval == 2) && $nameking == 3){
                if($names == "CPU型"){
        			$where['names'] = [['eq','高主频型弹性裸金属服务器'],['eq','计算型弹性裸金属服务器'],['eq','通用型弹性裸金属服务器'], 'or'];
        		}else{
        			$where['names'] = ['eq',$names];
        		}
        	}else{
        		if($names == "通用型"){
	        		$where['names'] = [['eq','通用型'],['eq','通用网络增强型'], 'or'];
	        	}elseif($names == "计算型"){
	        		$where['names'] = [['eq','计算型'],['eq','计算网络增强型'], 'or'];
	        	}elseif($names == "内存型"){
	        		$where['names'] = [['eq','内存型'],['eq','内存增强型'],['eq','内存网络增强型'], 'or'];
	        	}elseif($names == "高主频型"){
	        		$where['names'] = [['eq','高主频通用型'],['eq','高主频计算型'], 'or'];
	        	}elseif($names == "入门级（共享）"){
	        		$where['names'] = [['eq','突发性能实例'],['eq','共享基本型实例'],['eq','共享计算型实例'],['eq','共享通用型实例'],['eq','共享内存型实例'], 'or'];
	        	}else{
	        		$where['names'] = ['eq',$names];
	        	}
        	}
            
            //实例规格搜索
        	$rule = trim(I('post.instanceVal'));
        	/*$rule = "ecs.n4.2xlarge";*/
        	if($rule){
        		unset($where['names']);
        		$where['names'] = ['neq',' '];
        		$where['rule'] = ['like','%'.$rule.'%'];
        	}
            
            //根据cpu返回内存
        	$optionid = I('post.optionid');
        	if($optionid != 0){
        		unset($where['names']);
        		$where['cpu'] = ['eq',$optionid];
        	}

        	//根据内存返回cpu
        	$optionids = I('post.optionids');
        	if($optionids != 0){
        		unset($where['names']);
        		$where['ram'] = ['eq',$optionids];
        	}

        	/*if(($optionid != 0) && ($optionids != 0)){
        		unset($where['names']);
        		$where['cpu'] = $optionid;
        		$where['ram'] = $optionids;
        	}elseif($optionid != 0){
        		unset($where['names']);
        		$where['cpu'] = $optionid;
        	}else{
        		unset($where['names']);
        		$where['ram'] = $optionids;
        	}*/

        	/*if($optionid != 0){
        		unset($where['names']);
        		$where['cpu'] = ['eq',$optionid];
        	}elseif($optionids != 0){
        		unset($where['names']);
        		$where['ram'] = ['eq',$optionids];
        	}elseif($optionid != 0 && $optionids != 0){
                unset($where['names']);
                $where['cpu'] = ['eq',$optionid];
                $where['ram'] = ['eq',$optionids];
        	}
*/
	  /*  	$where['names'] = ['neq',' '];*/
	    	$where['territory'] = ['eq',$area];
	    	$where['state'] = ['eq','Available'];
            $where['wang'] = ['eq','vpc'];
            $fenname = I('post.fenname');//地域分区
            $info['ecscount'] = M('ecs')->where($where)->count();

            //规格列表
            $info['ecs'] = M('ecs')->where($where)->order('names asc,cpu asc,months asc,hours asc')->limit($offset,$limit)->select();

            //根据cpu返回内存
            $cpu = M('ecs')->where($where)->field('ram')->select();
            $ram = M('ecs')->where($where)->field('cpu')->select();
            if(!empty($fenname)){
                $where['fenname'] = ['eq',$fenname];
                $where['status'] = ['eq','Available'];
                $where['wang'] = ['eq','vpc'];
                $info['ecscount'] = M('ecstu')->where($where)->count();
                $info['ecs'] = M('ecstu')->where($where)->order('names asc,cpu asc,months asc,hours asc')->limit($offset,$limit)->select();
                $cpu = M('ecstu')->where($where)->field('ram')->select();
                $ram = M('ecstu')->where($where)->field('cpu')->select();
            }
            array_multisort(array_column($cpu,'ram'),SORT_ASC,$cpu);
            $info['cpu'] = array_unique($cpu,SORT_REGULAR);

            //根据内存返回cpu
	    	/*$ram = M('ecs')->where($where)->field('cpu')->select();*/
            array_multisort(array_column($ram,'cpu'),SORT_ASC,$ram);
            $info['ram'] = array_unique($ram,SORT_REGULAR);
    
            //镜像
	    	$where1['keyname'] = ['eq',$area];
	    	/*$info['areaimg'] = M('areaimg')->where($where1)->select();*/
            $areaimgs = M('areaimg')->where($where1)->select();
            $info['areaimg'] = $this->a_array_unique($areaimgs,'platform');
          /*  dump($info['areaimg']);*/

	    	//公网带宽
	    	$info['bandwidth'] = M('bandwidth')->where(['area'=>$area])->find();

	    	//包年包月
	    	/*$childval = I('post.childval'); 
	    	$where2['area'] = $area;
	    	if($childval == 1){
	    		$info['disk'] = M('disk')->where($where2)->find();
	    	}*/

        	$info['where'] = $_POST;
        	$info['wheres'] = $where;
/*        	$msg = M('disk')->limit(10)->select();
        	$kingname = array();
        	foreach ($msg as $key => $value) {
        		$kingname[] = $value['kingname'];
        	}
        	$kingname = join(',',$kingname);
        	dump($kingname);
	    	exit;*/
            //数据盘
            $wheres['area'] = ['eq',$area];
            $wheres['kingname'] = ['eq','高效云盘'];
            $wheres['play'] = ['eq','data'];
            $info['gao'] = M('disk')->where($wheres)->find();

            $wheres1['area'] = ['eq',$area];
            $wheres1['kingname'] = ['eq','SSD云盘'];
            $wheres1['play'] = ['eq','data'];
            $info['ssd'] = M('disk')->where($wheres1)->find();

            //折扣
            $arra['agent_id'] = session(id);
            $arra['is_agent'] = 0;
            $info['bei'] = M('mid_relation')->where($arra)->find();
            if(!$info['bei']){
                $info['bei']['instance'] = 1;
                $info['bei']['system_disk'] = 1;
                $info['bei']['storage_disc'] = 1;
                $info['bei']['network'] = 1;
            }
	    	$this->ajaxReturn($info);
        }
    }

    //二维数组去掉重复值
    public function a_array_unique($arr, $key){
        $tmp_arr = array();  
        foreach($arr as $k => $v)  
        {  
            if(in_array($v[$key], $tmp_arr))   //搜索$v[$key]是否在$tmp_arr数组中存在，若存在返回true  
            {  
                unset($arr[$k]); //销毁一个变量  如果$tmp_arr中已存在相同的值就删除该值  
            }  
            else {  
                $tmp_arr[$k] = $v[$key];  //将不同的值放在该数组中保存  
            }  
       }  
       //ksort($arr); //ksort函数对数组进行排序(保留原键值key)  sort为不保留key值  
        return $arr;  
    }

    //分区查询
    public function areainfo(){
        if(IS_POST){
            $keyname = I('post.areanameid');
            $info = M('areainfo')->where(['keyname'=>$keyname])->select();
            $this->ajaxReturn($info);
        }   
    }

    //系统镜像改变时间
    public function areaimg(){
    	if(IS_POST){
    		$where['id'] = I('post.id');
    		$areaimg = M('areaimg')->field('keyname,platform')->where($where)->find();
    		$areaimgs = M('areaimg')->where($areaimg)->select();
    		$this->ajaxReturn($areaimgs);
    	}   
    }

    // 专有网络
    function zyInternet(){
        $this->display();
    }

    //系统盘
    public function system(){
    	if(IS_POST){	
	    	//系统盘
	    	$area = I('post.area');
	    	$system = I('post.system');
	    	$where['area'] = ['eq',$area];
	    	$where['play'] = ['eq','system'];
	    	$where['kingname'] = ['eq',$system];
	    	$info = M('disk')->where($where)->find();
	    	$this->ajaxReturn($info);
    	}
    }

    //数据盘
    public function data(){
    	if(IS_POST){	
	    	//数据盘
        	$area = I('post.area');
	    	$data = I('post.system');
        	$where['area'] = ['eq',$area];
        	$where['play'] = ['eq','data'];
        	$where['kingname'] = ['eq',$data];
        	$info = M('disk')->where($where)->find();
	    	$this->ajaxReturn($info);
    	}
    }

    public function sureorder()
    {   
        $where['orderid'] = I('get.orderid');
        if(!empty($where)){
            $info = M('order')->where($where)->find();
            $info['ecs'] = M('ecs')->field('normsname')->where('id='.$info['eid'])->find();
            $shuju = explode(',', $info['shuju']);
            $shu = array();
            foreach ($shuju as $key => $value) {
                if($value == "cloud_efficiency"){
                    $shu[] = "高效云盘";
                }else{
                    $shu[] = "SSD云盘"; 
                }
            }
            $info['shuju'] = join(',',$shu);
            $arr['ImageId'] = $info['imageid'];
            $info['areaimg'] = M('areaimg')->field('osname')->where($arr)->find();
            $this->assign('info',$info);
        }
    	$this->display();
    }

    // 进入购买页面之前的欢迎介绍页面
    public function welcomeservice(){
        $this->display();
    }
}