<?php
//阿里云基本配置
function aliyun()
{
    $list = M('config')->field('key,value')->select();
    $info = array();
    foreach ($list as $key => $value) {
        $info[$value['key']] = $value['value'];
    }
    return $info;
}
//分页设置
function pages($page,$map)
{
    //分页跳转的时候保证查询条件
	foreach($map as $key=>$val) {
	    $page->parameter[$key]   =   urlencode($val);
	}
    $page->setConfig('header','条留言');
    $page->setConfig('first','首页');
    $page->setConfig('prev','上一页');
    $page->setConfig('next','下一页');
    $page->setConfig('last','尾页');
    $page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%<div id="pageTips">第 '.I('p',1).' 页/共 %TOTAL_PAGE% 页 ( 共 %TOTAL_ROW% 条信息)</div>');
}

//重启指定的实例。
function RebootInstance($url,$accessKeyId,$accessSecret,$rule)
{
    $iClientProfile = DefaultProfile::getProfile($url,$accessKeyId,$accessSecret);
    $client = new DefaultAcsClient($iClientProfile);
	$request = new RebootInstance();
	$request->queryParameters["InstanceId"] = $rule;
	$response = $client->getAcsResponse($request);
	$response = json_encode($response);
	$response = json_decode($response,true);
	return $response;
}

//根据传入的实例名称释放您的实例资源。
function DeleteInstance($url,$accessKeyId,$accessSecret,$rule)
{
    $iClientProfile = DefaultProfile::getProfile($url,$accessKeyId,$accessSecret);
    $client = new DefaultAcsClient($iClientProfile);
	$request = new DeleteInstance();
	$request->queryParameters["InstanceId"] = $rule;
	$response = $client->getAcsResponse($request);
	$response = json_encode($response);
	$response = json_decode($response,true);
	return $response;
}
?>