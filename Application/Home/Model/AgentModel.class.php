<?php
namespace Home\Model;
use Think\Model;
class AgentModel extends Model{
    //添加代理商账户
    public function addAgent($agentInfo){
        $rules = array(
            //会员用户名
            array('admin_name','','帐号名称已经存在！',0,'unique',1), // 在新增的时候验证name字段是否唯一
            //密码
            array('admin_password','require','密码不能为空!'), // 自定义函数验证密码格式
            //确认密码
            array('repassword','admin_password','确认密码不正确',0,'confirm'), // 验证确认密码是否和密码一致
            //手机号
            array('phone','/^1[34578]\d{9}$/','手机号格式错误',0,'regex'),
            //邮箱
            array('email','email','邮箱格式错误',0,'regex'),
            //折扣
            array("zhekou","0,1","折扣不在范围内",0,"between"),//todo 还要验证小数0-1之间
            //状态
            array('status',array(1,2),'值的范围不正确！',0,'in'),

        );
        $Agent = M("Agent"); // 实例化User对象
        if (!$Agent->validate($rules)->create()){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($agentInfo);
            return $result;
        }
    }
}