<?php
// +----------------------------------------------------------------------
// | 后台系统设置模块
// +----------------------------------------------------------------------
// | date:2017-05-16
// +----------------------------------------------------------------------
// | Author: lzb
// +----------------------------------------------------------------------
namespace Admin\Controller;

use Common\Controller\AdminBaseController;

class ConfigController extends AdminBaseController
{
    //后台首页
    public function index()
    {
        if (IS_POST) {
            $data = I('post.');
            if ($_FILES['logo']['name'] != "") {
                $upload = new \Think\Upload();// 实例化上传类
                $upload->maxSize = 3145728;// 设置附件上传大小
                $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
                //图片储存的路径
                $urls = "Uploads";
                if (!file_exists($urls)) {
                    mkdir($urls, 0777, true);
                }
                $upload->rootPath = './Uploads/'; // 设置附件上传根目录
                // 上传文件 
                $info = $upload->upload();
                if (!$info) {// 上传错误提示错误信息
                    $this->error($upload->getError());
                } else {
                    // 上传成功 获取上传文件信息
                    foreach ($info as $file) {
                        $data['logo'] = $file['savepath'] . $file['savename'];
                    }
                }
            }
            foreach ($data as $key => $value) {
                $config = M('config')->where(['key' => $key])->save(['value' => $value]);
            }
            $this->success("设置成功", U('config/index'), 1);
        } else {
            $list = M('config')->field('key,value')->select();
            $info = array();
            foreach ($list as $key => $value) {
                $info[$value['key']] = $value['value'];
            }
            $this->assign('info', $info);
            $this->display();
        }
    }

    //阿里云基本设置
    public function aliyun()
    {
        if (IS_POST) {
            $data = I('post.');
            foreach ($data as $key => $value) {
                $config = M('config')->where(['key' => $key])->save(['value' => $value]);
            }
            $this->success("设置成功", U('config/aliyun'), 1);
        } else {
            $list = M('config')->field('key,value')->where(['cate' => 'aliyun'])->select();
            $info = array();
            foreach ($list as $key => $value) {
                $info[$value['key']] = $value['value'];
            }
            $this->assign('info', $info);
            $this->display();
        }
    }

    //微信基本设置
    public function weixin()
    {
        if (IS_POST) {
            $data = I('post.');
            foreach ($data as $key => $value) {
                $config = M('config')->where(['key' => $key])->save(['value' => $value]);
            }
            $this->success("设置成功", U('config/weixin'), 1);
        } else {
            $list = M('config')->field('key,value')->where(['cate' => 'weixin'])->select();
            $info = array();
            foreach ($list as $key => $value) {
                $info[$value['key']] = $value['value'];
            }
            $this->assign('info', $info);
            $this->display();
        }
    }

    //支付宝基本设置
    public function alipay()
    {
        if (IS_POST) {
            $data = I('post.');
            foreach ($data as $key => $value) {
                $config = M('alipay')->where(['keys' => $key])->save(['value' => $value]);
            }
            $this->success("设置成功", U('config/alipay'), 1);
        } else {
            $list = M('alipay')->field('keys,value')->select();
            $info = array();
            foreach ($list as $key => $value) {
                $info[$value['keys']] = $value['value'];
            }
            $this->assign('info', $info);
            $this->display();
        }
    }

    /**
     * 运营商列表
     */
    public function operator()
    {
        $operator = M('operator');
        $map['id'] = ['gt', 0];
        $total = $operator->where($map)->count();
        $pageSize = 20;
        $page = new \Think\Page($total, $pageSize);
        pages($page, $map);
        if ($pageSize < $total) {
            $show = $page->show();
        }
        $data = $operator->where($map)->limit($page->firstRow, $page->listRows)->order('id asc')->select();

        $this->assign('show', $show);//分页
        $this->assign('list', $data);
        $this->display();
    }

    /**
     * 查看运营商详情
     */
    public function ReadOperator()
    {
        $operator_id = trim(I('get.id', 0, 'intval'));
        if (empty($operator_id)) $this->error('参数错误');
        $cate = M('operator')->where('id=' . $operator_id)->find();
        $con['cate'] = $cate['cate'];
        $operator_info = M('config')->where($con)->getField('id,key,value,desc', true);
        $this->assign('name', $cate['operator_name']);  //运营商名称
        $this->assign('data', $operator_info);
        $this->display();
    }

    /**
     * 编辑运营商信息
     */
    public function EditOperator()
    {
        if (IS_POST) {
            $data = I('post.');
            foreach ($data as $index => $item) {
                $result = M('config')->where(['key' => $index])->save(['value' => $item]);
            }
            $this->success("设置成功", U('config/operator'), 1);
        } else {
            $operator_id = trim(I('get.id', 0, 'intval'));
            if (empty($operator_id)) $this->error('参数错误');
            $cate = M('operator')->where('id=' . $operator_id)->find();
            $con['cate'] = $cate['cate'];
            $operator_info = M('config')->where($con)->getField('id,key,value,desc', true);
            $this->assign('name', $cate['operator_name']);  //运营商名称
            $this->assign('data', $operator_info);
            $this->display();
        }
    }

    public function add_operator()
    {
        if (IS_POST) {
            $cate = $_POST['cate'];
            $cate_name = $_POST['cate_name'];
            if (empty($cate)) $this->error('cate不能为空！');

            $flag = M('operator')->where(['cate'=>$cate])->getField('id');
            if(!empty($flag)) $this->error('此运营商已经存在，请核对 ');

            $keys = $_POST['key'];
            $values = $_POST['value'];
            $descs = $_POST['desc'];
            $arr_key = array_values($keys);
            $map['key'] = ['in', $arr_key];
            $info = M('config')->where($map)->getField('id', true);
            if (!empty($info)) $this->error('参数键名已经存在，请核对 ');

            //保存运营商配置参数
            foreach ($keys as $index => $k) {
                if (!empty($k)) {
                    $result = M('config')->add(['cate' => $cate, 'key' => $k, 'value' => $values[$index], 'desc' => $descs[$index]]);
                }
            }
            //保存运营商表
            $insert_id = M('operator')->add(['cate' => $cate, 'operator_name' => $cate_name]);

            //每次新增一个运营商，则在中间表中给所有的代理商 生成一条默认的记录
            $agent_id_list = M('mid_relation')->where(['is_agent'=>1])->distinct(true)->getField('agent_id', true);
            if(!empty($agent_id_list)){
                foreach ($agent_id_list as $k => $item){
                    $result = M('mid_relation')->add(['agent_id'=>$item, 'is_agent'=>1, 'operator_id'=>$insert_id]);
                }
            }
            //每次新增一个运营商，则在中间表中给所有的会员 生成一条默认的记录
            $user_id_list = M('mid_relation')->where(['is_agent'=>0])->distinct(true)->getField('agent_id', true);
            if(!empty($user_id_list)){
                foreach ($user_id_list as $k => $item){
                    $result = M('mid_relation')->add(['agent_id'=>$item, 'is_agent'=>0, 'operator_id'=>$insert_id]);
                }
            }
            $this->success("设置成功", U('config/operator'), 1);
        } else {
            $this->display();
        }

    }
}

?>