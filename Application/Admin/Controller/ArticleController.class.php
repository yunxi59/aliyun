<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/1
 * Time: 15:17
 */

namespace Admin\Controller;

use Common\Controller\AdminBaseController;

//文章分类和文章内容管理
class ArticleController extends AdminBaseController
{
    /**
     * 文章分类管理
     */
    public function ArticleCat()
    {
        //编辑某分类【update】
        if (IS_POST) {

            $cate_id = trim(I('post.cat_id'));
            $name = trim(I('post.cat_name'));
            $state = trim(I('post.cat_state'));

            if (empty($cate_id)) $this->error('抱歉！参数不全');
            //查看该分类是否已存在
            $article_category = M('article_category');
            $exist_id = $article_category->getById($cate_id);
            if (empty($exist_id)) {
                $this->error('该文章分类 不存在');
            }

            $map['id'] = $cate_id;
            //更新数据
            $data = array(
                'id' => $cate_id,
                'name' => $name,
                'state' => $state,
            );

            $record_num = $article_category->save($data);
            if (!empty($record_num)) {
                $this->success('编辑成功！', U('article/articleCat'), 1);
            } else {
                $this->error('编辑失败！');
            }

        } //查询过滤并展示文章列表【search】
        else {
            //设置默认过滤条件
            $map['id'] = ['gt', 0];

            $cat_list = M('article_category')->where($map)->select();
            if (!empty($cat_list)) {
                foreach ($cat_list as $index => $item) {
                    if ($item['state'] == 1) {
                        $cat_list[$index]['state'] = '启用';
                    } else {
                        $cat_list[$index]['state'] = '禁用';
                    }
                }
            }
            $this->assign('list', $cat_list);
            $this->display();
        }
    }

    //添加一个文章分类
    public function addArticleCat()
    {
        $name = trim(I('param.name'));
        $state = trim(I('param.state'));

        $addDate = [
            'name' => $name,
            'state' => $state,
            'add_time' => date('Y-m-d H:i:s', time()),
        ];
        $record_num = M('article_category')->add($addDate);
        if (empty($record_num)) {
            $this->error('插入失败 请稍后重试。。。');
        } else {
            $this->success('新增成功', U('article/articlecat'));
        }
    }

    //删除一个文章分类
    public function delArticleCat()
    {
        $id = trim(I('param.id'));
        $record_num = M('article_category')->delete($id);
        if (empty($record_num)) {
            $this->error('删除失败 请稍后重试。。。');
        } else {
            $this->success('删除成功', U('article/articlecat'), 1);
        }
    }


    /**
     * 文章内容管理
     */


    public function Article()
    {
        //删除某条文章【delete】
        if (IS_POST) {
            $article_id = trim(I('post.art_id'));
            $record_num = M('article')->delete($article_id);
            if (empty($record_num)) {
                $this->error('删除失败 请稍后重试。。。');
            } else {
                $this->success('删除成功', U('article/article'), 1);
            }

            //查询过滤并展示文章列表【search】
        } else {

            //设置默认过滤条件
            $map['id'] = ['gt', 0];

            //按文章分类过滤
            $cat_id = trim(I('get.cat_id'));
            if (!empty($cat_id)) {
                $map['category_id'] = $cat_id;
            }

            $state = trim(I('get.state'));
            //按照审核状态进行过滤
            if (!empty($state)) {
                $map['state'] = $state;
            }

            $title = trim(I('get.title'));
            //按照审核状态进行过滤
            if (!empty($title)) {
                $map['title'] = $title;
            }

            $article = M('article');
            $map['id'] = array('GT', '0');
            $total = $article->where($map)->count();

            $pageSize = 5;
            $page = new \Think\Page($total, $pageSize);
            pages($page, $map);
            if ($pageSize < $total) {
                $show = $page->show();
            }

            $article_list = $article->where($map)->select();

            $this->assign('cat_id', $cat_id);
            $this->assign('state', $state);
            $this->assign('title', $title);
            $this->assign('list', $article_list);
            $this->assign('num', $total);
            $this->assign('show', $show);//分页
            $this->display();
        }
    }

    //文章详情页（起始页，可衍生其他页面）
    public function readOneArticle()
    {
        $type = trim(I('get.type'));
        if (!empty($type) && $type == 'read') {
            $res['type'] = 'read';
            $res['btn'] = '返回';
            $res['action'] = 'Article';
            $res['topic'] = '文章详情页';
        } elseif (!empty($type) && $type == 'edit') {
            $res['type'] = 'edit';
            $res['btn'] = '保存';
            $res['action'] = 'saveOneArticle';
            $res['topic'] = '文章修改页';
        } elseif (!empty($type) && $type == 'add') {
            $res['type'] = 'add';
            $res['btn'] = '提交';
            $res['action'] = 'saveOneArticle';
            $res['topic'] = '文章上传页';
        }

        if ($type == 'read' || $type == 'edit') {
            $article_id = trim(I('get.art_id'));
            if (empty($article_id)) $this->error('文章id为空。。。');

            $article_info = M('article')->find($article_id);
            if (empty($article_info)) {
                $this->error('该文章id不存在。。。');
            } else {
                $this->assign('article_id', $article_id);
                $this->assign('res', $res);
                $this->assign('data', $article_info);
                $this->display();
            }
        } else {
            $this->assign('res', $res);
            $this->display();
        }
    }

    public function deleteOneArticle()
    {
        $id = trim(I('param.art_id'));
        $record_num = M('article')->delete($id);
        if (empty($record_num)) {
            $this->error('删除失败 请稍后重试。。。');
        } else {
            $this->success('删除成功', U('article/article'), 1);
        }

    }

    //新增和编辑文章内容
    public function saveOneArticle()
    {
        $article_id = trim(I('post.art_id'));
        $cat_id = trim(I('post.cat_id'));
        $title = trim(I('post.title'));
        $from = trim(I('post.from'));
        $content = trim(I('post.content'));
        $order = trim(I('post.order'));
        $state = trim(I('post.state'));

        if (empty($title)) $this->error('表单为空，请填写', U('article/readOneArticle',array('type'=>'add')));

        $data = [
            'id' => $article_id,
            'category_id' => $cat_id,
            'title' => $title,
            'add_time' => date('Y-m-d H:i:s', time()),
            'from' => $from,
            'content' => $content,
            'order' => $order,
            'state' => $state,
        ];

        //去掉为空的键值
        foreach ($data as $index => $datum) {
            if (empty($datum)) unset($data[$index]);
        }

        $record_num = empty($article_id) ? M('article')->add($data) : M('article')->save($data);
        if (empty($record_num)) {
            $this->error('保存失败 请稍后重试。。。');
        } else {
            $this->success('保存成功', U('article/article'), 1);
        }
    }


    //删除一个记录
    public function deleteOneDate($table, $key, $value)
    {
        $major_key = $key;
        $map[$major_key] = $value;
        $record_num = M($table)->where($map)->delete();
        return $record_num;
    }
}