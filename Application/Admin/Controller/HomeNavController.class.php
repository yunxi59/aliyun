<?php
namespace Admin\Controller;

use Common\Controller\AdminBaseController;

/**
 * //前台导航栏目管理
 */
class HomeNavController extends AdminBaseController
{
    //导航栏目查看和修改
    public function index()
    {
        $nav = M('nav');
        $first_nav = $nav->where('parent=0')->getField('id, parent, title, url, order', true);
        if (empty($first_nav)) {
            $this->error('前台栏目为空');
        }
        $res = [];
        foreach ($first_nav as $index => $item) {
            $res[$index] = $item;

            $second_nav = $nav->where('parent=' . $index)->getField('id, parent, title, url, order', true);
            if (!empty($second_nav)) {
                foreach ($second_nav as $k => $v) {
                    $res[$k] = $v;
                }
            }
        }

        $this->assign('data', $res);
        $this->display();
    }

    /**
     * 添加菜单
     */
    public function add()
    {
        if (IS_POST) {
            $pid = I('post.pid');
            $name = trim(I('post.name'));
            $url = trim(I('post.url'));

            if (empty($name) || empty($url))
                $this->error('抱歉！参数不全');
            //查看导航链接是否已存在
            $HomeNav = D('Nav');
            $exist_id = $HomeNav->getFieldByUrl($url, 'id');
            if (!empty($exist_id)) {
                $this->error('链接已存在，请核对');
            }

            //插入数据
            $data = array(
                'parent' => $pid,
                'title' => $name,
                'url' => $url,
            );
            $result = $HomeNav->add($data);
            if ($result) {
                $this->success('操作成功！', U('HomeNav/index'), 1);
            } else {
                $this->error('写入错误！');
            }

        } else {
            $this->pid = I('id', 0);
            $this->type = $this->pid ? '添加子菜单' : '添加菜单';
            $this->display();
        }
    }

    /**
     * 修改菜单
     */
    public function edit()
    {

        if (IS_POST) {
            $nav_id = I('post.id');
            $pid = I('post.pid');
            $name = trim(I('post.name'));
            $url = trim(I('post.url'));

            if (empty($name) || empty($url)) $this->error('抱歉！参数不全');

            //查询信息是否存在
            $HomeNav = D('Nav');
            $exist_id = $HomeNav->getFieldById($nav_id, 'id');
            if (empty($exist_id)) {
                $this->error('信息异常');
            }

            $data = array(
                'id' => $nav_id,
                'parent' => $pid,
                'title' => $name,
                'url' => $url,
            );
            $result = $HomeNav->save($data);
            if ($result) {
                $this->success('操作成功！', U('HomeNav/index'), 1);
            } else {
                $this->error('修改失败！', U('HomeNav/index'), 1);
            }
        } else {
            $this->error('nav_id 非法', U('HomeNav/index'), 1);
        }
    }

    /**
     * 删除菜单
     */
    public function delete()
    {
        $nav_id = I('get.id');

        //查询信息是否存在
        $HomeNav = D('Nav');
        $data = $HomeNav->getById($nav_id);
        if (empty($data)) $this->error('nav_id 不存在', U('HomeNav/index'), 1);
        //删除数据
        $HomeNav->delete($nav_id);
        $this->success('操作成功！', U('HomeNav/index'), 1);
    }


}