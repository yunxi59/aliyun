<?php
// +----------------------------------------------------------------------
// | 后台系统设置模块
// +----------------------------------------------------------------------
// | date:2017-05-16
// +----------------------------------------------------------------------
// | Author: lzb
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class UserController extends AdminBaseController{
    //会员列表信息
    public function index()
    {
        //分类搜索
        if(is_numeric(I('param.diff'))){
            $map['diff'] = I('param.diff');
        }
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if(!empty($keyword)){
             //设置查询条件,模糊查询
            $map['id|username'] = array('like',"%$keyword%");
        }    
        $total = M('user')->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total,$pageSize);
        pages($page,$map);
        if($pageSize < $total){
            $show = $page->show();
        }
        $list = M('user')->where($map)->limit($page->firstRow,$page->listRows)->order('id desc')->select();
        $this->assign('list',$list);
        $this->assign('show',$show);
        $this->display();
    }

    //添加个人会员
    public function add()
    {
        if(IS_POST){
            $model = D('Personage');
            if(!$data = $model -> create()){
                $this->error($model->getError(),'',1);  
            }else{
                //注册成功
                if($inser_id = $model->add()){
                    $this->success('添加成功',U('index'),1);
                }else{
                    //注册失败
                    $this->error('添加失败',U('add'),1);
                }
            }
        }else{  
            $this->display();
        }
    }

    //添加企业会员
    public function addc()
    {
        if(IS_POST){
            $model = D('Company');
            if(!$data = $model -> create()){
                $this->error($model->getError(),'',1);  
            }else{
                //注册成功
                if($inser_id = $model->add()){
                    $this->success('添加成功',U('index'),1);
                }else{
                    //注册失败
                    $this->error('添加失败',U('add'),1);
                }
            }
        }else{
            $this->display();
        }
    }

    //修改会员信息
    public function edit()
    {
        if(IS_POST){
            $id = I('post.id');
          /*  $data = I('post.');*/
           dump($id);
        }else{          
            $id = I('get.id');
            $user = M('user')->where('id='.$id)->find();
            $this->assign('user',$user);
            $this->display();
        }
    }


    //删除
    public function del()
    {
        $id=I('get.id');
        $del=M('user')->where('id='.$id)->delete();
        if ($del > 0) {
            $this->success('删除成功', U('index'),1);
        }else{
            $this->error('删除失败');
        }
    }
}
