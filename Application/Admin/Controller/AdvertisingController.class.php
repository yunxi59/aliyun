<?php
/**
 * Created by PhpStorm.
 * User: ywk
 * Date: 2018/10/31
 * Time: 9:50
 */
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
//网站广告位管理
class AdvertisingController extends AdminBaseController
{
    //定义图片类型
    protected $head_Slideshow = 3;              //首页头部（轮播图）
    protected $mid_CustomerCase_Slideshow = 1;  //首页中间（客户案例，轮播图）
    protected $mid_CustomerCase_Logo = 2;       //首页中间（客户案例，Logo图）

    //首页头部（轮播图）广告

    public function Headshow()
    {
        $map['type'] = $this->head_Slideshow;
        $slideshow_list = M('excellent_case')->where($map)->order('order asc')->getField('id,name, depict, img, order', true);

        $this->assign('list', $slideshow_list);
        $this->display();
    }

    public function addHeadshow()
    {
        $this->display();
    }

    protected function upload_one_photo($img_name ,$path)
    {
        if($_FILES[$img_name]['name'] != "") {
            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize = 3145728;// 设置附件上传大小
            $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
            //图片储存的路径
            $urls = "Uploads";
            if (!file_exists($urls)) {
                mkdir($urls, 0777, true);
            }
            $upload->rootPath = './Uploads/'; // 设置附件上传根目录
            $upload->savePath  =     $path; // 设置附件上传（子）目录
            // 上传文件
            $info = $upload->upload();
            if (!$info) {// 上传错误提示错误信息
                $this->error($upload->getError());
            } else {
                // 上传成功 获取上传文件信息
                $url = $info[$img_name]['savepath'] . $info[$img_name]['savename'];
                return $url;
            }
        }
    }

    public function addData()
    {
        if (IS_POST) {
            $name = I('post.name');
            $depict = trim(I('post.introduce'));
            $img = $_FILES['img']['name'];
            $profession = trim(I('post.profession'));
            $company = trim(I('post.company'));
            $website = trim(I('post.website'));
            $type = trim(I('post.type'));
            $order = trim(I('post.order'));

            if (empty($name) || empty($img)) $this->error('抱歉！参数不全');

            //查看图片链接是否已存在
            $excellent_case = D('excellent_case');
            $exist_id = $excellent_case->getFieldByImg($img, 'id');
            if (!empty($exist_id)) {
                $this->error('图片不能重复，请核对');
            }

            //开始上传图片，返回上传后的图片链接并保存
            $img = $this->upload_one_photo('img' ,'Slideshow');

            //插入数据
            $data = array(
                'name' => $name,
                'profession' => $profession,
                'company' => $company,
                'website' => $website,
                'img' => $img,
                'type' => $type,
                'depict' => $depict,
                'order' => $order,
            );
            foreach ($data as $index => $datum) {
                if(empty($datum)) unset($data[$index]);
            }
            $result = $excellent_case->add($data);
            if ($result) {
                $this->success('操作成功！', U('advertising/Headshow'), 1);
            } else {
                $this->error('写入错误！');
            }
        } else {
            $this->display();
        }
    }

    public function updateData()
    {
        if (IS_POST) {
            $id = I('post.id');
            $name = I('post.name');
            $depict = trim(I('post.introduce'));
            $img = $_FILES['img']['name'];//第一个img是前端的键名字，第二个name是$_FILES固定的参数
            $profession = trim(I('post.profession'));
            $company = trim(I('post.company'));
            $website = trim(I('post.website'));
            $type = trim(I('post.type'));
            $order = trim(I('post.order'));

            if (empty($id)) $this->error('抱歉！参数不全');
            //查看图片链接是否已存在
            $excellent_case = D('excellent_case');
            $exist_id = $excellent_case->getById($id);
            if (empty($exist_id)) {
                $this->error('图片id 不存在');
            }

            $map['id'] = $id;
            //开始上传图片，返回上传后的图片链接并保存
            if(!empty($img)){
                $img = $this->upload_one_photo('img' ,'Slideshow');
            }

            //插入数据
            $data = array(
                'name' => $name,
                'profession' => $profession,
                'company' => $company,
                'website' => $website,
                'img' => $img,
                'type' => $type,
                'depict' => $depict,
                'order' => $order,
            );

            foreach ($data as $index => $datum) {
                if(empty($datum)) unset($data[$index]);
            }

            $result = $excellent_case->where($map)->save($data);
            if ($result) {
                $this->success('操作成功！', U('advertising/Headshow'), 1);
            } else {
                $this->error('写入错误！');
            }

        } else {
            $this->display();
        }
    }

    public function upload_ajax()
    {
        if (IS_POST) {
            $id = I('post.id');
            $img = $_FILES['img']['name'];//第一个img是前端的键名字，第二个name是$_FILES固定的参数

            if (empty($id)) $this->error('抱歉！参数不全');
            //查看图片链接是否已存在
            $excellent_case = D('excellent_case');
            $exist_id = $excellent_case->getById($id);
            if (empty($exist_id)) {
                $this->error('图片id 不存在');
            }

            $map['id'] = $id;
            //开始上传图片，返回上传后的图片链接并保存
            if(!empty($img)){
                $img = $this->upload_one_photo('img' ,'Slideshow');
            }

            //插入数据
            $data = array(
                'img' => $img,
            );

            $result = $excellent_case->where($map)->save($data);
            if ($result) {
                $res['state'] = true;
                $res['url'] = $img;
                echo json_encode($res);
            } else {
                $res['state'] = false;
                echo json_encode($res);
            }

        }
    }

    public function deleteOneDate()
    {
        $ad_id = I('get.id');

        //查询信息是否存在
        $excellent_case = D('excellent_case');
        $data = $excellent_case->getById($ad_id);
        if (empty($data)) $this->error('该广告位图片 不存在', U('Advertising/Headshow'), 1);
        //删除数据
        $excellent_case->delete($ad_id);
        $this->success('操作成功！', U('Advertising/Headshow'), 1);
    }

    //首页中部案例（轮播图）广告

    public function Caseshow()
    {
        $map['type'] = $this->mid_CustomerCase_Slideshow;
        $slideshow_list = M('excellent_case')->where($map)->order('order asc')->getField('id,name, profession, company, website, img, order', true);

        $this->assign('list', $slideshow_list);
        $this->display();
    }

    public function addCaseshow()
    {
        $this->display();
    }

}