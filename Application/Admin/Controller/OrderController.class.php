<?php
// +----------------------------------------------------------------------
// | 后台系统设置模块
// +----------------------------------------------------------------------
// | date:2017-05-16
// +----------------------------------------------------------------------
// | Author: lzb
// +----------------------------------------------------------------------
namespace Admin\Controller;

use Common\Controller\AdminBaseController;

class OrderController extends AdminBaseController
{
    //订单管理：已付款订单
    public function index()
    {
        //已付款订单
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if (!empty($keyword)) {
            //设置查询条件,模糊查询
            $map['id|orderid|username'] = array('like', "%$keyword%");
        }

        //开始时间
        $starttime = strtotime(I('param.starttime'));
        //结束时间
        $endtime = strtotime(I('param.endtime'));
        if (!empty($starttime) && !empty($endtime)) {
            $map['ordertime'] = array('between', [$starttime, $endtime]);
        }
        $map['state'] = 1;
        $total = M('order')->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total, $pageSize);
        pages($page, $map);
        if ($pageSize < $total) {
            $show = $page->show();
        }
        $list = M('order')->where($map)->limit($page->firstRow, $page->listRows)->order('id desc')->select();
        $this->assign('list', $list);
        $this->assign('show', $show);
        $this->display();
    }

    //订单管理：未付款订单
    public function nopay()
    {
        //未付款订单
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if (!empty($keyword)) {
            //设置查询条件,模糊查询
            $map['id|orderid|username'] = array('like', "%$keyword%");
        }

        //开始时间
        $starttime = strtotime(I('param.starttime'));
        //结束时间
        $endtime = strtotime(I('param.endtime'));
        if (!empty($starttime) && !empty($endtime)) {
            $map['ordertime'] = array('between', [$starttime, $endtime]);
        }
        $map['state'] = 0;
        $total = M('order')->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total, $pageSize);
        pages($page, $map);
        if ($pageSize < $total) {
            $show = $page->show();
        }
        $list = M('order')->where($map)->limit($page->firstRow, $page->listRows)->order('id desc')->select();
        $this->assign('list', $list);
        $this->assign('show', $show);
        $this->display();
    }

    //订单管理：取消订单
    public function cancel()
    {
        //取消订单
        $keyword = trim(I('param.keyword', '', 'htmlspecialchars'));
        //搜索关键词
        if (!empty($keyword)) {
            //设置查询条件,模糊查询
            $map['id|orderid|username'] = array('like', "%$keyword%");
        }

        //开始时间
        $starttime = strtotime(I('param.starttime'));
        //结束时间
        $endtime = strtotime(I('param.endtime'));
        if (!empty($starttime) && !empty($endtime)) {
            $map['ordertime'] = array('between', [$starttime, $endtime]);
        }
        $map['state'] = 2;
        $total = M('order')->where($map)->count();
        $pageSize = 15;
        $page = new \Think\Page($total, $pageSize);
        pages($page, $map);
        if ($pageSize < $total) {
            $show = $page->show();
        }
        $list = M('order')->where($map)->limit($page->firstRow, $page->listRows)->order('id desc')->select();
        $this->assign('list', $list);
        $this->assign('show', $show);
        $this->display();
    }

    //已付款订单删除
    public function del()
    {
        $id = I('get.id');
        /* $del=M('order')->where('id='.$id)->delete();
         if ($del > 0) {
             $this->success('删除成功', U('nopay'),1);
         }else{
             $this->error('删除失败');
         }*/
    }

    //未付款订单删除
    public function nopaydel()
    {
        $id = I('get.id');
        $del = M('order')->where('id=' . $id)->delete();
        if ($del > 0) {
            $this->success('删除成功', U('nopay'), 1);
        } else {
            $this->error('删除失败');
        }
    }

    //取消订单订单删除
    public function canceldel()
    {
        $id = I('get.id');
        $del = M('order')->where('id=' . $id)->delete();
        if ($del > 0) {
            $this->success('删除成功', U('nopay'), 1);
        } else {
            $this->error('删除失败');
        }
    }

    //查看订单详情
    public function see()
    {
        $id = trim(I('get.id'));
        $order_info = M('order')
            ->alias('o')
            ->field('o.*, a.admin_name as agent_name')
            ->join('__AGENT__ a ON o.uid=a.admin_id', 'LEFT')
            ->order('o.ordertime DESC')
            ->where('id=' . $id)
            ->find();

        $this->assign('data', $order_info);
        $this->display();
    }

    //查看订单详情
    public function settlement()
    {
        $id = trim(I('get.id'));
        if (empty($id)) $this->error('订单号为空！');

        $is_settlement = M('order')->getFieldById($id, 'is_settlement');
        if (!empty($is_settlement)) {
            $this->error('当前订单已经提现过了，请勿重复操作！');
        }

        $con['o.id'] = $id;
        $db_data = M('settlement')
            ->alias('s')
            ->field('s.*, o.orderid')
            ->join('__ORDER__ o ON s.order_id=o.orderid', 'LEFT')
            ->order('s.agent_id ASC')
            ->where($con)
            ->select();
        if (empty($db_data)) $this->error('收入表为空');
        //结算：为所有相关的代理商账户进行提现
        foreach ($db_data as $index => $db_datum) {
            $con = array();
            $con['admin_id'] = $db_datum['agent_id'];
            //提现成功，余额加上，可提现金额减去
            $result = M('agent')->where($con)->setInc('account', $db_datum['increase']);// 提现成功，余额增加
            $result = M('agent')->where($con)->setDec('withdrawn', $db_datum['increase']);// 扣除可提现金额
        }
        $save = array();
        $save['id'] = $id;
        $save['is_settlement'] = 1; //设置为已提现
        $save['settlement_time'] = date('Y-m-d H:i:s', time()); //设置为已提现
        M('order')->save($save);
        $this->success('结算成功 ', U('order/index'), 1);
    }
}
