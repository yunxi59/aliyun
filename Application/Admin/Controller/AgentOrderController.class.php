<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/26
 * Time: 18:10
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;

/**
 * 订单分润记录
 */
class AgentOrderController extends AdminBaseController
{
    public function index(){

    }

    public function get_order_list(){
        //按代理商名称模糊查询
        $agent_name = trim(I('post.agent_name'));
        if(!empty($agent_name)){
            $map['agent_name'] = ['like','%'.$agent_name.'%'];
        }

        $commission_order = M('agent_commission_order_record');
        $total = $commission_order->where($map)->count();

        $pageSize = 10;
        $page = new \Think\Page($total, $pageSize);
        pages($page, $map);
        if ($pageSize < $total) {
            $show = $page->show();
        }
        $order_list = $commission_order->where($map)->limit($page->firstRow, $page->listRows)->order('create_time asc')->select();

        $this->assign('agent_name', $agent_name);
        $this->assign('list', $order_list);
        $this->assign('num', $total);
        $this->assign('show', $show);//分页
        $this->display();
    }

}