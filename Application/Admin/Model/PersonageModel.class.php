<?php
namespace Admin\Model;
use Think\Model;
class PersonageModel extends Model{
	protected $tableName='user';//重定义表名
	protected $tablePrefix='h_';//重定义表前缀
	//自动验证
	protected $_validate=array(
			//会员用户名
			array('username','','用会员用户名已存在',0,'unique',1),
			//手机号
			array('phone','/^1[34578]\d{9}$/','手机号格式错误',0,'regex'),
			//邮箱
			array('email','email','邮箱格式错误',0,'regex'),
			//密码
			array('password','/^[a-zA-Z0-9_]{6,20}$/','密码为6-20为字母、数字、下划线组成',0,'regex'),
			array('repassword','password','确认密码不正确',0,'confirm'), // 验证确认密码是否和密码一致
		);

	//自动完成
	protected  $_auto=array(
			//填充因子
			//array(填充字段,填充规则,[填充条件,附加规则]);
			array('password','md5',1,'callback'),
			array('addtime','time',1,'callback'),
			array('diff','1'),
		);
	  /* 给密码加密 */
	public function md5() {
	    return md5(md5(I('post.password')));
	  }
	public function time(){
		return time();
	}
}
