<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/25
 * Time: 14:42
 */

namespace Common\Model;
use Common\Model\BaseModel;

class AgentModel extends BaseModel
{
    //添加代理商账户
    public function addAgent($agentInfo){
        $rules = array(
            //会员用户名
            array('admin_name','','帐号名称已经存在！',0,'unique',1), // 在新增的时候验证name字段是否唯一
            //密码
            array('admin_password','require','密码不能为空!'), // 自定义函数验证密码格式
            //确认密码
            array('repassword','admin_password','确认密码不正确',0,'confirm'), // 验证确认密码是否和密码一致
            //手机号
            array('phone','/^1[34578]\d{9}$/','手机号格式错误',0,'regex'),
            //邮箱
            array('email','email','邮箱格式错误',0,'regex'),
            //折扣
            array("zhekou","0,1","折扣不在范围内",0,"between"),//todo 还要验证小数0-1之间
            //状态
            array('status',array(1,2),'值的范围不正确！',0,'in'),

        );
        $Agent = M("Agent"); // 实例化User对象
        if (!$Agent->validate($rules)->create()){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($agentInfo);
            return $result;
        }
    }

    //修改代理商信息
    public function editAgent($agentInfo){
        $rules = array(
            //手机号
            array('phone','/^1[3|4|5|8][0-9]{9}$/','手机号码错误！','0','regex',1),
            //邮箱
            array('email','email','邮箱格式错误',0,'regex'),
            //状态
            array('status',array(1,2),'值的范围不正确！',0,'in'),

        );
        $Agent = M("Agent"); // 实例化User对象
        if (!$Agent->validate($rules)->create()){
            // 验证不通过返回错误
            $data['state'] = false;
            $data['msg'] = $Agent->getError();
            return $data;
        }else{
            // 验证通过
            $where['admin_id'] = $agentInfo['admin_id'];
            unset($agentInfo['admin_id']);
            M('agent')->where($where)->save($agentInfo);
            $data['state'] = true;
            $data['msg'] = "代理商资料更新成功";
            return $data;
        }

    }

    //修改代理商信息
    public function editAgentPwd($agentInfo){
        $rules = array(
            //密码
            array('admin_password','require','密码不能为空!'), // 自定义函数验证密码格式
            //确认密码
            array('repassword','admin_password','确认密码不正确',0,'confirm'), // 验证确认密码是否和密码一致
        );
        $Agent = M("Agent"); // 实例化User对象
        if (!$Agent->validate($rules)->create()){
            // 验证不通过返回错误
            $data['state'] = false;
            $data['msg'] = $Agent->getError();
            return $data;
        }else{
            // 验证通过
            $where['admin_id'] = $agentInfo['admin_id'];
            unset($agentInfo['admin_id']);
            unset($agentInfo['repassword']);
            M('agent')->where($where)->save($agentInfo);
            $data['state'] = true;
            $data['msg'] = "代理商密码更新成功";
            return $data;
        }
    }

}