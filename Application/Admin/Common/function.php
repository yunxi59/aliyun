<?php 
//分页设置
function pages($page,$map)
{
    //分页跳转的时候保证查询条件
	foreach($map as $key=>$val) {
	    $page->parameter[$key]   =   urlencode($val);
	}
    $page->setConfig('header','条留言');
    $page->setConfig('first','首页');
    $page->setConfig('prev','上一页');
    $page->setConfig('next','下一页');
    $page->setConfig('last','尾页');
    $page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%<div id="pageTips">第 '.I('p',1).' 页/共 %TOTAL_PAGE% 页 ( 共 %TOTAL_ROW% 条信息)</div>');
}
?>