<?php
return array(
	'SHOW_PAGE_TRACE'        => true,                           // 是否显示调试面板
    'URL_CASE_INSENSITIVE'   => true,                            // URL不区分大小写
    'LOAD_EXT_CONFIG'        => 'db,web',                            // 加载网站设置文件
    'TMPL_PARSE_STRING'      => array(                           // 定义常用路径
        '__HOME_CSS__'       => __ROOT__.'/Public/Home/css',
        '__HOME_JS__'        => __ROOT__.'/Public/Home/js',
        '__HOME_IMAGES__'    => __ROOT__.'/Public/Home/images',
        '__ADMIN_CSS__'      => __ROOT__.'/Public/Admin/css',
        '__ADMIN_JS__'       => __ROOT__.'/Public/Admin/js',
        '__ADMIN_IMAGES__'   => __ROOT__.'/Public/Admin/images',
        '__ADMIN_PLUGINS__'  => __ROOT__.'/Public/Admin/plugins',
        '__MOBILE_CSS__'     => __ROOT__.'/Public/Mobile/css',
        '__MOBILE_JS__'      => __ROOT__.'/Public/Mobile/js',
        '__MOBILE_IMAGES__'  => __ROOT__.'/Public/Mobile/images',
        '__COM_JS__'         => __ROOT__.'/Public/Com/js',   
        '__COM_CSS__'        => __ROOT__.'/Public/Com/css',   
        '__COM_IMAGES__'     => __ROOT__.'/Public/Com/images',   
    ),


    //***********************************URL设置**************************************
    'MODULE_ALLOW_LIST'     => array('Home','Admin','Api', 'Wechat','Agent'),    //允许访问列表
    'DEFAULT_MODULE'        => 'Home',
    'URL_MODEL'             => 0,
);