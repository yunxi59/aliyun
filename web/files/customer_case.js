$(function(){
    // var QY_ADMIN_CENTER = 'https://cache.yisu.com';
    var RESOURCE_PATH = 'https://cache.yisu.com/upload/';
    $('.get-case-list').click(function(){
        setTimeout(function(){
            getCustomerCaseList($(this));
        },100);
    })

    // 点击异步分页
    $(document).on('click', '.pagebox ul li a, .next-page, .pre-page', function(){
        var cur_page = parseInt($(this).attr('data-page'));
        if($(this).hasClass('pre-page')) --cur_page;
        if($(this).hasClass('next-page')) ++cur_page;
        getCustomerCaseList($(this), cur_page);
        console.log(cur_page);

    })

    function getCustomerCaseList(_this, cur_page){
        var params = {};
        // 遍历已指定的查询条件
        var _ind_obj = $('.industry-type').find('a');
        var _pro_obj = $('.product-type').find('a');
        var _create_time_obj = $('.case-create-time');
        if(_create_time_obj.hasClass('arrow-down')){
            params['create_time'] = 'create_time desc';
        }else if(_create_time_obj.hasClass('arrow-up')){
            params['create_time'] = 'create_time asc';
        }
        $.each(_ind_obj, function(i,item){
            if($(item).hasClass('defaultlink')){
                params['ind_id'] = isNaN($(item).attr('data-id'))?0:$(item).attr('data-id');
            }
        });
        $.each(_pro_obj, function(i,item){
            if($(item).hasClass('defaultlink')){
                params['pro_id'] = parseInt($(item).attr('data-id'));
            }
        });
        if(!cur_page || cur_page == 0){
            cur_page = 1;
        }
        params['cur_page'] = cur_page;
        $.ajax({
            type: 'GET',
            url : 'https://yisuapi.yisu.com/index.php/Speed/CustomerCase/getCateList/?callback=?',
            dataType: 'jsonp',
            data: params,
            beforeSend:function(){
                $('#total-result').text(0);
                $('.company-list').empty();
                $('.pagebox').empty();
            },
            success:function(res){
                $('#total-result').text(res.count?res.count:0);
                if(!res.status){
                    return false;
                }

                var _html = '';
                $.each(res.data.case_list, function(i,item){
                    _html +='<ul>';
                    _html +='<li>';
                    _html +='<div class="company-detailsbox">';
                    _html +='<div class="company-video">';
                    _html +='<div class="playbtn"></div>';
                    _html +='<a videohref="'+item.video_url+'" class="video-link"><img src="' + RESOURCE_PATH + item.logo_url + '" alt=""/></a>';
                    _html +='</div>';
                    _html +='<div class="company-msg">';
                    _html +='<a href="/case/id_'+item.id+'.html">';
                    _html +='<div class="company-name">'+item.company+'</div>';
                    _html +='<div class="user-msg">姓名：<span class="firstspan">'+item.represent_name+'</span>职业：<span>'+item.represent_name+'</span></div>';
                    _html +='<div class="user-msg">网址：<span>'+item.web_url+'</span></div>';
                    _html +='<div class="yisu-text">'+item.send_word+'</div>';
                    _html +='<div class="server-list">';
                    _html +='<div class="server-list-text">使用产品：</div>';
                    _html +='<div class="server-list-item">';
                    _html +='<ul>';
                    $.each(item.product_type, function(j, jtem){
                        _html +='<li>';
                        _html +='<div class="product-img cloud-sprite"></div>';
                        _html +='<div class="product-name">'+res['data']['product'][jtem]['name']+'</div>';
                        _html +='</li>';
                    });
                    _html +='</ul>';
                    _html +='</div>';
                    _html +='</div>';
                    _html +='</a>';
                    _html +='</div>';
                    _html +='</div>';
                    _html +='</li>';
                    _html +='</ul>';
                });
                $('.company-list').html(_html);
                $('.pagebox').empty().html(res.pager);
                console.log(res.pager);
                _this.attr('disabled', false);
            },
            error: function(){
                alert('网络出错');
                return false;
            }
        });
    }

})