(function(){
	var qyapiUrl = 'https://www.yisu.com',
		username,
		tokenkey,
		ERROR_LOGOUT = '退出失败，请稍候再试！',
		ms = Date.parse( new Date() ) + 1000*60*60*24*7, //cookie只保留7天
        expires = new Date( ms ),
		html = '';
	
	//记录当前站的当前url，为下一页面使用
	var href = window.location.href;             
    if ( window.navigator.cookieEnabled ){
        var cookieArr = [],
            hasHref = false;
        document.cookie = "href="+ encodeURI(href) +"; expires=" + expires +"; domain=.yisu.com; path=/; ";
    }

	//推广来源、推广业务员(chy/20170104)
	var href = window.location.href,
		match1 = href.match(/\?s=(.+?)$/i),
		match2 = href.match(/\?[fa]=(.+?)($|&)/i),
		cookieEnabled = window.navigator.cookieEnabled;

	if(cookieEnabled && match1 && match1[1]){
		var ms = Date.parse( new Date() ) + 1000*60*60*24*2,
			expires = new Date( ms );
		document.cookie = "qysalesman="+ match1[1] +"; expires=" + expires +"; domain=.yisu.com; path=/; ";
	}

	if(cookieEnabled && match2 && match2[1]){
		var ms = Date.parse( new Date() ) + 1000*60*60*24*7,
			expires = new Date( ms );
		document.cookie = "qyfrom="+ match2[1] + "||" + href +"; expires=" + expires +"; domain=.yisu.com; path=/; ";
	}	

	var tpl = '';
	tpl += '<div class="row">';
	tpl += 	'<span class="lt">{name}</span>';
	tpl += 	'{qq}';
	tpl += 	'{weixin}';
	tpl += '</div>';
	
	var fion = [
		{'name':'谢先生','qq':'980088805','weixin':''},
		// {'name':'易先生','qq':'858123038','weixin':'kfweixin/emwImg-yxs.jpg'},
		{'name':'易先生','qq':'3002009038','weixin':''},
		/*{'name':'亿速云','qq':'3385578182','weixin':''},
		{'name':'亿速云2','qq':'3074024342','weixin':''},*/
		{'name':'罗先生','qq':'3385578182','weixin':''},
		{'name':'朱先生','qq':'980088803','weixin':''},
		{'name':'钟先生','qq':'980088804','weixin':''},
		{'name':'李小姐','qq':'980088806','weixin':''},
        {'name':'杨小姐','qq':'980088810','weixin':''},
		{'name':'赖先生','qq':'980088832','weixin':''},
		{'name':'褚先生','qq':'980088807','weixin':''},
		{'name':'黄小姐','qq':'980088831','weixin':''}
	];
	
	var randArr = [];
	while(fion.length){
		var rand = Math.floor( Math.random()*fion.length );
		randArr.push(fion[rand]);
		fion.splice(rand,1);
	}	
	var fionHtml = '';
	for(var i=0,l=randArr.length;i<l;i++){
		var t = tpl,
			item = randArr[i],
			weixin = '',
			qq = item.qq,
			qqHtml = '';
		if(''!=item.weixin){
			weixin = '<a class="wx" ewmUrl="https://cache.yisu.com/www/images/'+ item.weixin +'"></a>';
		}

		//var qqHtml = '<a target="_blank" href="https://wpa.qq.com/msgrd?v=3&amp;uin='+ qq +'&amp;site=qq&amp;menu=yes" class="qq"></a>';
		var qqHtml = '<a target="_blank" href="https://wpa.qq.com/msgrd?v=3&amp;uin='+ qq +'&amp;site=qq&amp;menu=yes"><img border="0" alt="给我发消息" src="https://cache.yisu.com/www/images/qqol.gif"></a>';

		t = t.replace("{name}",item.name);
		t = t.replace("{qq}",qqHtml);
		t = t.replace("{weixin}",weixin);
		fionHtml += t;
	}



	html += '<div class="cIns">';
	html += '  <div class="csIn clearfix">';
	html += '    <div class="col flt_l"> <span class="tt">客服</span>';
	html += fionHtml;
	html += '    </div>';
	html += '    <div class="col flt_r"> <span class="tt austin">技术支持</span>';
	html += '      <div class="row" style="margin-left: 30px;"> <span class="lt tf">7*24 技术售后</span>';
	//html += '        <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=800811969&site=qq&menu=yes" class="qq"></a>';
	html += '        <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=800811969&site=qq&menu=yes"><img border="0" alt="给我发消息" src="https://cache.yisu.com/www/images/qqol.gif"></a></a>';
	//html += '        <a class="wx" ewmUrl="https://www.yisu.com/static/www/images/emwImg-QY-IDC.jpg"></a>';
	//html += '      </div>';
	//html += '      <div class="row"> <span class="lt">售后2</span>';
	//html += '        <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=1989319139&site=qq&menu=yes" class="qq"></a>';
	//html += '        <a class="wx" ewmUrl="https://www.yisu.com/static/www/images/emwImg-QY-IDC.jpg"></a>';
	html += '      </div>';
	html += '    </div>';
	html += '  </div>';
	html += '</div>';	
	$('.link .chis').append(html);

	$(".csIn .row .lt").click(QQClickEvent);

	checklogin();
    window.setLoginIn = function(){
        var login_in = href;
        //记录登入网址
        document.cookie = "login_in="+ encodeURI(login_in) +"; expires=" + expires +"; path=/; ";
        window.location.href = "https://www.yisu.com/login";
    };
	$('#logout').click(logout);


	function checklogin(){
		var url = 'https://yisuapi.yisu.com/index.php/Speed/Uc/checkLogin/?callback=?';
		$.ajax({
			type: "GET",
			url: url,
			dataType: "jsonp",
			success: function(res){
				if(res.status == 1){
					$('#username').html(res.data.username);
					$('.usercenter,.mtusercenter').attr('href','https://uc.yisu.com/index.php/vhost');
					$('#recoverd').attr('href','javascript:;');
					$('.yisu-login,.userbox-login,.login-reg-box').hide();
                    $('#user_info,.userbox').show();
					//if(window.location.pathname == '/recover' || window.location.pathname == '/reset_psw' ||  window.location.pathname == '/login'){window.location.href = '/';}
					if(window.location.pathname == '/login'){window.location.href = '/';}
				}
			}
		});
	}
	
	function logout(e){
		e.preventDefault();
		var url = 'https://yisuapi.yisu.com/index.php/Speed/Uc/logout/?callback=?';
		
		$.ajax({
            type: "GET",
            url: url,
            dataType: "jsonp",
            success: function(res){
				if( res == 1 ){
					alert( "退出登录成功！" );
                    $('#user_info,.userbox').hide();
                    $('.yisu-login,.login-reg-box').show();
                    $('#username').html('');
					$('#recoverd').attr('href','/recover');
				}else{
					alert(ERROR_LOGOUT);
				}
            }
        });
	}
	
	function QQClickEvent(e){
		var evt = e.currentTarget,
			next = evt.nextSibling;
		if(!next) return;
		if('#text'==next.nodeName) next = next.nextSibling;
		var href = next.getAttribute('href');
		window.open(href);
	}
})();