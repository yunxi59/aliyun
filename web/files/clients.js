/**
 * Created by QYWL on 2018/5/29.
 */

$(function(){
    //筛选条件切换
    $('.marketkind>div>a').click(function(){
        $(this).addClass('defaultlink').siblings().removeClass('defaultlink');
    });
    $('.market-condition>a').click(function(){
        $(this).addClass('select-condition').siblings().removeClass('select-condition arrow-down arrow-up');
    })
    $('.price-control').click(function(){
        $(this).siblings().removeClass('arrow-down arrow-up');
        if($(this).hasClass('arrow-down')){
            $(this).addClass('arrow-up').removeClass("arrow-down");
        }else{
            $(this).addClass('arrow-down').removeClass("arrow-up");
        }
    });
    $(document).on('click', '.playbtn', function(){
        $('.shadow').css('display','block');
        var vidoSrc = $(this).siblings('a').attr("videohref");
        $('.playnow video').attr('src',vidoSrc);
    })

    $('.closevideo span').click(function(){
        $('.shadow').css('display','none');
        $('.playnow video').trigger('pause');
    })
    $('video').bind('contextmenu',function() { return false; });
})