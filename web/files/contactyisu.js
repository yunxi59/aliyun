/**
 * Created by QYWL on 2018/6/9.
 * contactyisu.js
 */
$(function(){
    var navTop = $('#nav').offset().top;
    var navLeft = $('#nav').offset().left;
    $(window).scroll(function(){
        if($(this).scrollTop()>=navTop){
            $('#nav').css({ position:'fixed',left:navLeft+'px',marginTop:0+'px' });
        }else{
            $('#nav').css({ position:'absolute',left:0+'px',marginTop:90+'px' });
        }
        // console.log('con==='+$('#container').offset().top);
        // console.log($('#section-1').offset().top);
        if($(this).scrollTop() >= $('#section-1').offset().top){
            $('.section-1').addClass('current').siblings().removeClass('current');
        }
        if($(this).scrollTop() >= $('#section-2').offset().top){
            $('.section-2').addClass('current').siblings().removeClass('current');
        }
        if($(this).scrollTop() >= $('#section-3').offset().top){
            $('.section-3').addClass('current').siblings().removeClass('current');
        }
        if($(this).scrollTop() >= $('#section-4').offset().top){
            $('.section-4').addClass('current').siblings().removeClass('current');
        }
        if($(this).scrollTop() >= $('#section-5').offset().top){
            $('.section-5').addClass('current').siblings().removeClass('current');
        }
        if($(this).scrollTop() >= $('#section-6').offset().top){
            $('.section-6').addClass('current').siblings().removeClass('current');
        }
    });

    $('.section-1').click(function(){
        $("html,body").animate({scrollTop:$('#section-1').offset().top},300);
    });
    $('.section-2').click(function(){
        $("html,body").animate({scrollTop:$('#section-2').offset().top},300);
    });
    $('.section-3').click(function(){
        $("html,body").animate({scrollTop:$('#section-3').offset().top},300);
    });
    $('.section-4').click(function(){
        $("html,body").animate({scrollTop:$('#section-4').offset().top},300);
    });
    $('.section-5').click(function(){
        $("html,body").animate({scrollTop:$('#section-5').offset().top},300);
    });
    $('.section-6').click(function(){
        $("html,body").animate({scrollTop:$('#section-6').offset().top},300);
    });
})
