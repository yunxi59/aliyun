<?php
    date_default_timezone_set('Asia/Shanghai'); 
	$info = $_GET;
	include_once 'path/aliyun-php-sdk-core/Config.php';
	use Ecs\Request\V20140526\DescribeInstanceTypesRequest;
	$iClientProfile = DefaultProfile::getProfile($info['url'],$info['accesskeyid'],$info['accesssecret']);
	$client = new DefaultAcsClient($iClientProfile);

	$request = new DescribeInstanceTypesRequest();
	$request -> regionId($info['url']);
	$request -> setInstanceTypeFamily($info['InstanceTypeFamily']);
	# 发起请求并处理返回
	try {
	    $response = $client->getAcsResponse($request);
		echo json_encode($response);
	} catch(ServerException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	} catch(ClientException $e) {
	    print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
	}
?>
