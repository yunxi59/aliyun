/**
 * Created by QYWL on 2018/4/17.
 */
$(function(){
    //评论操作组的下滑线
    $(".product-item-titles>div").mouseover(function(){
        $(".bottom-line").css({
            left:$(this).offset().left-$(".product-item-titles").offset().left
        })
    });
    $(".product-item-titles>div").mouseout(function(){
        $(".bottom-line").css({
            left:$(".product-item-titles>div.active-item-title").offset().left-$(".product-item-titles").offset().left
        })
    });
    $(".product-item-titles>div").click(function(){
        $(this).addClass("active-item-title").siblings().removeClass("active-item-title");
        $(".product-kinds").eq($(this).index()).css("display","block").siblings().css("display","none");
    });

    //头像高亮显示
    $('.partner-des').mouseover(function(){
        $(this).addClass('currentuser').siblings().removeClass('currentuser');
    });

})