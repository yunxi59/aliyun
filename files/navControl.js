/**
 * 控制顶部导航
 * Created by QYWL on 2018/4/26.
 */
$(function(){
    //导航下拉菜单
    $('.yisu-link>ul>li').mouseenter(function(){
        $(this).children('.pulldown-banner').slideDown(200);
    })
    $('.yisu-link>ul>li').mouseleave(function(){
        $(this).children('.pulldown-banner').css('display','none');
    })

    $('.last-nav-link').mouseenter(function(){ $('.about-link').slideDown(100);$('.about-link').css('overflow','unset'); })
    $('.last-nav-link').mouseleave(function(){ $('.about-link').slideUp(100); })
})
