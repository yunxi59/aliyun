/**
 * Created by qy006 on 2016/2/20.
 */
var qyCreate  = function () {
    var that           = {};
    var calculateMoney = function () {
        var ins  = $('#create-region').val(), region = $('#create-region option[value="'+ins+'"]').attr('data-reg'), bandwidth = $('#create-bandwidth').val() * 1, num = $('#create-num').val() * 1, month = $('#create-month').val() * 1;
        //var unit    = bandwidth > 5 ? that.price['6m'][region] * 1 : that.price['5m'][region] * 1;
        var ipprice = that.ipprice[region * 1] ? that.ipprice[region * 1] * 1 : 0;//IP每月的费用，不包括带宽
        //var money   = ipprice * month * num + (unit * Math.max(0, bandwidth - 1)) * month * num;
        var money   = ipprice * month * num;
        $('#create-money').val(money);
        return money;
    };
    $('#createModel').on('shown.bs.modal', function () {
        $.getJSON(urls.create, {opt: 'show'}, function (dt) {
            if (dt.error * 1 == 1) {
                $('#create-footer').hide();
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.price   = dt.data.price;
                that.ipprice = dt.data.ipprice;
                that.account = dt.data.account;
                var i        = 0, tr = [];
                for (i in dt.data.insList) {
                    tr.push('<option value="' + dt.data.insList[i].sn + '" data-reg="' + dt.data.insList[i].region_id + '">【' + dt.data.insList[i].contact + dt.data.insList[i].region_name + '】 云服务器 - ' + dt.data.insList[i].ip + '</option>');
                }

                $('#create-region').html(tr.join(''));
                $('#create-account').val(dt.data.account);
                calculateMoney();

                $('#create-region').selectpicker({
                    //style: 'btn-info',
                    size: 4
                });
            }
        });
    });
                
    $('#create-month').selectpicker({
        //style: 'btn-info',
        size: 8
    });
    $('#create-region,#create-month').change(calculateMoney);
    $('#create-bandwidth').keyup(function () {
        var obj = $(this), v = $.trim(obj.val()), mi = 1, ma = 200;
        if (v == '') return;
        if (!/[0-9]+/.test(v)) {
            obj.val(mi);
            return;
        }
        v *= 1;
        if (v < mi) {
            obj.val(mi);
            return;
        }
        if (v > ma) {
            obj.val(ma);
            return;
        }
        calculateMoney();
    });
    $('#create-num').keyup(function () {
        var obj = $(this), v = $.trim(obj.val()), mi = 1, ma = 250;
        if (v == '') return;
        if (!/[0-9]+/.test(v)) {
            obj.val(mi);
            return;
        }
        v *= 1;
        if (v < mi) {
            obj.val(mi);
            return;
        }
        if (v > ma) {
            obj.val(ma);
            return;
        }
        calculateMoney();
    });
    $('#create-submit').click(function () {
        if ($('#create-bandwidth').val() * 1 < 1 || $('#create-num').val() * 1 < 1) {
            alert('带宽 与 数量必须填写');
            return false;
        }
        if (calculateMoney() > that.account * 1) {
            alert('帐户余额不足，请先充值！');
            return false;
        }
        $('#create-footer').hide();
        var ds = {
            ins_sn: $('#create-region').val(),
            bandwidth: $('#create-bandwidth').val() * 1,
            month: $('#create-month').val() * 1,
            num: $('#create-num').val() * 1
        };
        $.getJSON(urls.create, {ds:ds, opt: 'create'}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                alert('购买IP成功');
                window.location.reload();
            }
        });
        $('#createModel').modal('hide');
    });
    $('#btn-create').click(function () {
        $('#createModel').modal();
        $('#create-footer').show();
    });
}();
var renew     = function () {
    var that           = {};
    var calculateMoney = function () {
        var month = that.mouth;
        var money = that.data.price * month;
        $('#renew-money').text(money+'元');
        return money;
    };
    $('#renewModel').on('shown.bs.modal', function () {
        $.getJSON(urls.renew, {sn: that.sn, opt: 'show'}, function (dt) {
            that.mouth      = $('#charge-cycle .active').val();
            if (dt.error * 1 == 1) {
                $('#renew-footer').hide();
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                $('#renew-account').text(dt.data.account+'元');
                $('#renew-org-bandwidth').html(dt.data.bandwidth + 'M');
                calculateMoney();
            }
        });
        $('.renewmouth').click(function () {
            $('#charge-cycle .active').removeClass('active');
            $(this).addClass('active');
            that.mouth = $(this).val();
            calculateMoney();
        });
    });
    $('#renew-submit').click(function () {
        if (calculateMoney() > that.data.account * 1) {
            toastr.info('帐户余额不足，请先充值！', '', {positionClass: "toast-top-center"});
            return false;
        }
        $('#renew-footer').hide();
        $.getJSON(urls.renew, {sn: that.sn, month: that.mouth, opt: 'renew'}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                toastr.info('续费IP成功', '', {positionClass: "toast-top-center"});
                $('#ended-' + that.sn).html(dt.data.ended);
            }
        });
        $('#renewModel').modal('hide');
    });
    $('.btn-renew').click(function () {
        that.sn = $(this).attr('data-sn');
        that.tag = $(this).attr('data-tag');
        $('#renew-name').html(that.tag);
        $('#renewModel').modal();
        $('#renew-footer').show();
    });
}();
var qyBind    = function () {
    var that = {};
    $('#bindModel').on('shown.bs.modal', function () {
        $.getJSON(urls.bind, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                $('#bind-list').html('<tr><td>没有数据</td></tr>');
                $('#bind-footer').hide();
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                var i = 0, len = dt.data.length, o, tr = [];
                for (i; i < len; i++) {
                    o = dt.data[i];
                    tr.push('<tr><td><input type="radio" name="bind-instance" value="' + o.sn + '"></td><td>' + o.tag + '</td><td>' +
                        o.vcpu + ' 核，' + o.memory + 'G内存'
                        + '</td></tr>');
                }

                $('#bind-list').html(tr.join(''));
            }
        });
    });
    $('#bind-submit').click(function () {
        if ($('#bind-list input:checked').length < 1) {
            alert('请选择需要绑定的服务器');
            return false;
        }
        $('#bind-footer').hide();
        $.getJSON(urls.bind, {sn: that.sn, instance: $('#bind-list input:checked').val(), opt: 1}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#status-' + that.sn).html('<span class="label label-success">已绑定</span>');
            }
        });
        $('#bindModel').modal('hide');
    });
    $('.btn-bind').click(function () {
        that.sn = $(this).attr('data-sn');
        $('#bind-name').html($('#name-' + that.sn).text());
        $('#bindModel').modal();
        $('#bind-footer').show();
    });
}();
var qyUnbind  = function () {
    var that = {};
    $('#unbindModel').on('shown.bs.modal', function () {
        $.getJSON(urls.unbind, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                $('#unbind-list').html('<tr><td>没有数据</td></tr>');
                $('#unbind-footer').hide();
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#unbind-list').html('<tr><td>' + dt.data.tag + '</td><td>' + dt.data.vcpu + '核心，' + dt.data.memory + 'G内存</td></tr>');
            }
        });
    });
    $('#unbind-submit').click(function () {
        if (!confirm('确认解绑此IP')) {
            return false;
        }
        $('#unbind-footer').hide();
        $.getJSON(urls.unbind, {sn: that.sn, instance: $('#unbind-list input:checked').val(), opt: 1}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#status-' + that.sn).html('<span class="label label-info">未绑定</span>');
            }
        });
        $('#unbindModel').modal('hide');
    });
    $('.btn-unbind').click(function () {
        that.sn = $(this).attr('data-sn');
        $('#unbind-name').html($('#name-' + that.sn).text());
        $('#unbindModel').modal();
        $('#unbind-footer').show();
    });
}();
var qyUpgrade = function () {
    var that           = {};
    var calculateMoney = function () {
        var bw    = $('#upgrade-bandwidth').val() * 1;
        var unit  = bw > 5 ? that.data.price['6m'] : that.data.price['5m'];
        var money = Number(unit * bw * that.data.letDay - that.data.letMoney).toFixed(2);
        $('#upgrade-money').val(money);
        return money;
    };
    $('#upgradeModel').on('shown.bs.modal', function () {
        $.getJSON(urls.upgrade, {sn: that.sn, opt: 'show'}, function (dt) {
            if (dt.error * 1 == 1) {
                $('#upgrade-footer').hide();
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                $('#upgrade-org-bandwidth').html(dt.data.bandwidth + 'M');
                $('#upgrade-bandwidth').val(dt.data.bandwidth);
                $('#upgrade-money').val(0);
                $('#upgrade-account').val(dt.data.account);
            }
        });
    });
    $('#upgrade-bandwidth').keyup(function () {
        var obj = $(this), v = $.trim(obj.val()), mi = that.data.bandwidth * 1;
        if (v == '') return;
        if (!/[0-9]+/.test(v)) {
            obj.val(mi);
            return;
        }
        v *= 1;
        if (v < mi) {
            obj.val(mi);
            return;
        }
        if (v > 200) {
            obj.val(200);
            return;
        }
        calculateMoney();
    });
    $('#upgrade-submit').click(function () {
        if ($('#upgrade-bandwidth').val() <= that.data.bandwidth) {
            alert('没有改变带宽');
            return false;
        }
        if (calculateMoney() > that.data.account * 1) {
            alert('帐户余额不足，请先充值！');
            return false;
        }
        $('#create-footer').hide();
        $.getJSON(urls.upgrade, {sn: that.sn, bandwidth: $('#upgrade-bandwidth').val() * 1, opt: 'upgrade'}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#bandwidth-' + that.sn).html($('#upgrade-bandwidth').val());
                alert('扩容成功');
            }
        });
        $('#upgradeModel').modal('hide');
    });
    $('.btn-upgrade').click(function () {
        that.sn = $(this).attr('data-sn');
        $('#upgrade-name').html($('#name-' + that.sn).text());
        $('#upgradeModel').modal();
        $('#upgrade-footer').show();
    });
}();
var qyDel     = function () {
    var that = {};
    $('#delModel').on('shown.bs.modal', function () {
        $.getJSON(urls.del, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                $('#del-footer').hide();
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
            }
        });
    });
    $('#del-submit').click(function () {
        if (!confirm('确认删除此IP')) {
            return false;
        }
        $('#del-footer').hide();
        $.getJSON(urls.del, {sn: that.sn, instance: $('#del-list input:checked').val(), opt: 1}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#tr-' + that.sn).remove();
            }
        });
        $('#delModel').modal('hide');
    });
    $('.btn-del').click(function () {
        that.sn = $(this).attr('data-sn');
        $('#del-name').html($('#name-' + that.sn).text());
        $('#delModel').modal();
        $('#del-footer').show();
    });
}();
var qyBill    = function () {
    var that = {};
    $('#billModel').on('shown.bs.modal', function () {
        $.getJSON(urls.bill, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                $('#bill-list').html('<tr><td>没有数据</td></tr>');
                $('#bill-footer').hide();
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                var i = 0, len = dt.data.length, o, tr = [];


                $('#bill-list').html(tr.join(''));
            }
        });
    });
    $('#bill-submit').click(function () {
        $('#billModel').modal('hide');
    });
    $('.btn-bill').click(function () {
        that.sn = $(this).attr('data-sn');
        $('#bill-name').html($('#name-' + that.sn).text());
        $('#billModel').modal();
        $('#bill-footer').show();
    });
}();