/**
 * Created by qy006 on 2016/1/26.
 */
var _start = function (name) {
    $('#status_' + name).html('<span class="loading">启动中</span>');
    $.getJSON(urls.start, {sn: name, opt: 1}, function (dt) {
        if (dt.error * 1 == 1) {
            toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
        } else {
            $('#status_' + name).html(getStatusHtml('STATUS_START_ING'));
        }
    });
};

var getStatusHtml = function (id) {
    if (isNaN(id)) {
        id = qyStatusAsc[id] == undefined ? '' : '' + qyStatusAsc[id];
    }
    return (id && qyStatusHtml[id] == undefined) ? '' : qyStatusHtml[id];
};
var getStatus     = function () {
    var a = function () {
        $.getJSON(urls.status, {sn: needGetStatus}, function (dt) {
            if (dt.error * 1 != 1) {
                for (var sn in dt.data) {
                    $('#status_' + sn).html(getStatusHtml(dt.data[sn]));
                }
            }
            window.setTimeout(getStatus, 3000);
        });
    };
    a();
    return a;
}();
/*var renew         = function () {
    var that = {},buildCoupon = function(money, data){
        var i, ds, cond, t = ['<option value="">可用代金券</option>'];
        for (i in data) {
            ds = '';
            cond = '[无条件] ';
            if(data[i]['min_money']>0){
                cond = '[限续费'+data[i]['min_money']+'元或以上] ';
            }
            if(data[i]['min_money']>0 && money<data[i]['min_money']) {
                ds = ' disabled';
            }
            t.push('<option value="' + i + '" '+ds+'>' +cond+ data[i]['name'] + '</option>');
        }
        if(t.length<2) {
            t = ['<option value="">暂无代金券</option>'];
            $('#renew-coupon-tr').hide();
        }else{
            $('#renew-coupon-tr').show();
        }
        $('#renew-coupon').html(t.join(''));
    };
    $('#renewModel').on('show.bs.modal', function (e) {
        $.getJSON(urls.renew, {sn: that.sn}, function (dt) {
            console.log(dt);
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                if (dt.data.storage_money) {
                    $('#renew-storage-tr').show();
                }
                if (dt.data.ip_money) {
                    $('#renew-ip-tr').show();
                }
                if (dt.data.storage_money || dt.data.ip_money) {
                    $('#renew-instance-tr').show();
                }
                $('#renew-money').html(dt.data.money * 10);
                $('#renew-instance-money').html(dt.data.instance_money * 10);
                $('#renew-ip-money').html(dt.data.ip_money * 10);
                $('#renew-storage-money').html(dt.data.storage_money * 10);
                $('#renew-acount-money').html(dt.data.accout_money);
                $('#renew-cycle button').removeClass('active');
                $('#renew-cycle button[value="10"]').addClass('active');
                if (dt.data['coupon']) {
                    buildCoupon(dt.data.money * 10, dt.data['coupon']);
                }
                $('#renew-submit').prop('disabled', false);
            }
        });
    });
    $('#renewModel').on('hide.bs.modal', function (e) {
        $('#renew-instance-tr').hide();
        $('#renew-storage-tr').hide();
        $('#renew-ip-tr').hide();
    });
    $('#renewsModel').on('show.bs.modal', function (e) {
        var sns = that.sn.join(',');
        $.getJSON(urls.renews, {sns: sns}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                $('#renews-money').html(dt.data.money * 10);
                $('#renews-acount-money').html(dt.data.accout_money);
                $('#renews-cycle button').removeClass('active');
                $('#renews-cycle button[value="10"]').addClass('active');
                $('#renews-submit').prop('disabled', false);
            }
        });
    });

    $('#renew-cycle').click(function (e) {
        var obj = $(e.target), v = mt = obj.val() * 1;
        $('#renew-cycle .active').removeClass('active');
        obj.addClass('active');
        var m = that.data.money * v, cp = $('#renew-coupon').val();
        if(v == 6) v = 5;
        $('#renew-money').html(that.data.money * v);
        $('#renew-instance-money').html(that.data.instance_money * v);
        $('#renew-ip-money').html(that.data.ip_money * v);
        $('#renew-storage-money').html(that.data.storage_money * v);
        buildCoupon(that.data.money * v, that.data['coupon']);
    });

    $('#renews-cycle').click(function (e) {
        var obj = $(e.target), v = obj.val() * 1;
        if(v == 6) v = 5;
        $('#renews-cycle .active').removeClass('active');
        obj.addClass('active');
        var m = that.data.money * v;
        $('#renews-money').html(that.data.money * v);
    });
    $('#renew-coupon').change(function () {
        var cp = $(this).val(), mon = $('#renew-cycle .active').val();
        if(mon == 6) mon = 5;
        if (cp.length > 10) {
            $('#renew-money').html(Math.max(0.01,that.data.money * mon - that.data['coupon'][cp]['money']));
        }
    });
    $('#renew-submit').click(function () {
        if ($('#renew-money').text() * 1 > that.data.accout_money) {
            toastr.info('帐户余额不足，请先充值！', '', {positionClass: "toast-top-center"});
            return;
        }
        $('#renew-submit').prop('disabled', true);
        $.getJSON(urls.renew, {sn: that.sn, renew_cycle: $('#renew-cycle .active').val(), coupon:$('#renew-coupon').val()}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#expired_' + that.sn).html(dt.data.expired);
                toastr.info('续费成功', '', {positionClass: "toast-top-center"});
                $('#renewModel').modal('hide');
                /!*if (dt.data.status != qyStatusAsc.STATUS_RUN && confirm("确认开启服务器吗？")) {
                 _start(that.sn);
                 }*!/
            }
        });
    });
    $('#renews-submit').click(function () {
        if ($('#renews-money').text() * 1 > that.data.accout_money) {
            toastr.info('帐户余额不足，请先充值！', '', {positionClass: "toast-top-center"});
            return;
        }
    $('#renews_waiting').css('display','block');
        $('#renews-submit').prop('disabled', true);
        var sns = that.sn.join(',');
        $.getJSON(urls.renews, {sns: sns, renews_cycle: $('#renews-cycle .active').val(), coupon:$('#renews-coupon').val()}, function (dt) {
            console.log(dt);
            $('#renews_waiting').css('display','none');
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                for (var i in that.sn) {
                    $('#expired_' + that.sn[i]).html(dt.data[that.sn[i]].expired);
                }
                toastr.info('续费成功', '', {positionClass: "toast-top-center"});
                $('#renewsModel').modal('hide');
                /!*if (dt.data.status != qyStatusAsc.STATUS_RUN && confirm("确认开启服务器吗？")) {
                 _start(that.sn);
                 }*!/
            }
        });
    });

    $('.btn-renew').click(function () {
        that.sn  = $(this).attr('data-sn');
        that.tag = $('#tag_' + that.sn).text();
        var ip_reg = /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/;
        that.ip  = ip_reg.exec($('#ip_' + that.sn).html());
        $('#renew-name').html(that.tag);
        $('#renew-ip').html(that.ip);
        $('#renew-coupon').html('<option value="">暂无代金券</option>');
        $('#renew-coupon-tr').hide();
        $('#renewModel').modal();
    });

    $('.btn-renews').click(function () {
        that.sn = [];
        $('.selectable-item:checked').each(function() {
            that.sn.push($(this).val());
        });
        if (that.sn.length < 1) {
            toastr.info('请选择要续费的云服务器', '', {positionClass: "toast-top-center"});
            return;
        }
        $('#renews-num').html(that.sn.length);
        $('#renewsModel').modal();
    });
}();*/


var renew         = function () {
    var that = {},buildCoupon = function(money, data, type){
        var i, ds, cond, t = ['<option value="">可用代金券</option>'];
        for (i in data) {
            ds = '';
            cond = '[无条件] ';
            if(data[i]['min_money']>0){
                cond = '[限续费'+data[i]['min_money']+'元或以上] ';
            }
            if(data[i]['min_money']>0 && money<data[i]['min_money']) {
                ds = ' disabled';
            }
            t.push('<option value="' + i + '" '+ds+'>' +cond+ data[i]['name'] + '</option>');
        }
        if(type == 'sin'){
            if(t.length<2) {
                t = ['<option value="">暂无代金券</option>'];
                $('#renew-coupon-tr').hide();
            }else{
                $('#renew-coupon-tr').show();
            }
            $('#renew-coupon').html(t.join(''));
        }else{
            if(t.length<2) {
                t = ['<option value="">暂无代金券</option>'];
                $('#renews-coupon-tr').hide();
            }else{
                $('#renews-coupon-tr').show();
            }
            $('#renews-coupon').html(t.join(''));
        }
    };
    $('#renewModel').on('show.bs.modal', function (e) {
        var money = 0, instance_money = 0;
        $.getJSON(urls.renews, {sns: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                console.log(that.data.money);
                if (dt.data.storage_money) {
                    $('#renew-storage-tr').show();
                }
                if (dt.data.ip_money) {
                    $('#renew-ip-tr').show();
                }
                if (dt.data.storage_money || dt.data.ip_money) {
                    $('#renew-instance-tr').show();
                }
                if(dt.data.ins){
                    var ukey = [dt.data.ins['region_id'], dt.data.ins['cpu'], dt.data.ins['memory'], dt.data.ins['bandwidth'], dt.data.ins['defense'], 10].join('-');
                }
                if(dt.data.pg_price && dt.data.pg_price['ayear'] && ukey && ukey in price_activity && price_activity[ukey] != undefined){
                    money = dt.data.pg_money;
                    instance_money = dt.data.pg_amoney;
                }else{
                    money = dt.data.money;
                    instance_money = dt.data.instance_money;
                }
                that.ins_money = money;
                $('#renew-money').html(changeTwoDecimal(money * 10));
                $('#renew-instance-money').html(changeTwoDecimal(instance_money * 10));
                $('#renew-ip-money').html(dt.data.ip_money * 10);
                $('#renew-storage-money').html(dt.data.storage_money * 10);
                $('#renew-acount-money').html(dt.data.accout_money);
                $('#renew-cycle button').removeClass('active');
                $('#renew-cycle button[value="10"]').addClass('active');
                if (dt.data['coupon']) {
                    buildCoupon(money * 10, dt.data['coupon'], 'sin');
                }
                $('#renew-submit').prop('disabled', false);
                //paycross
                $('#renew-sn').val(that.sn);
                $('#renew-month').val(10);
                if ($('#renew-money').text() * 1 > dt.data.accout_money) {
                    var surplus = changeTwoDecimal($('#renew-money').text() * 1 - dt.data.accout_money);
                    $('#renew-submit-ol').show();
                    $('#renew-submit').hide();
                    $('.payway').show();
                    $('#paysup').html(surplus);
                }else{
                    $('#renew-submit').show();
                    $('.payway').hide();
                    $('#renew-submit-ol').hide();
                }
            }
        });
    });
    $('#renewModel').on('hide.bs.modal', function (e) {
        $('#renew-instance-tr').hide();
        $('#renew-storage-tr').hide();
        $('#renew-ip-tr').hide();
    });
    $('#renewsModel').on('show.bs.modal', function (e) {
        var sns = that.sn.join(',');
        $.getJSON(urls.renews, {sns: sns}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                $('#renews-money').html(dt.data.money * 10);
                $('#renews-acount-money').html(dt.data.accout_money);
                $('#renews-cycle button').removeClass('active');
                $('#renews-cycle button[value="10"]').addClass('active');
                if (dt.data['coupon']) {
                    buildCoupon(dt.data.money * 10, dt.data['coupon'], 'bal');
                }
                $('#renews-submit').prop('disabled', false);
                //paycross
                $('#renews-sn').val(sns);
                $('#renews-month').val(10);
                if ($('#renews-money').text() * 1 > dt.data.accout_money) {
                    var surplus = changeTwoDecimal($('#renews-money').text() * 1 - dt.data.accout_money);
                    $('#renews-submit-ol').show();
                    $('#renews-submit').hide();
                    $('.payways').show();
                    $('#paysups').html(surplus);
                }else{
                    $('#renews-submit').show();
                    $('.payways').hide();
                    $('#renews-submit-ol').hide();
                }
            }
        });
    });

    $('#renew-cycle').click(function (e) {
        var obj = $(e.target), v = mt = obj.val() * 1;
        $('#renew-cycle .active').removeClass('active');
        obj.addClass('active');
        var m = that.data.money * v, cp = $('#renew-coupon').val(), money = 0, instance_money = 0;
        if(that.data.ins){
            var ukey = [that.data.ins['region_id'], that.data.ins['cpu'], that.data.ins['memory'], that.data.ins['bandwidth'], that.data.ins['defense'], v].join('-');
        }
        if(that.data.pg_price && ukey && ukey in price_activity && price_activity[ukey] != undefined){
            if(v == 6 && that.data.pg_halfmoney){
                var pg_insmoney = that.data.pg_halfmoney;
                money = that.data.pg_money;
                instance_money = pg_insmoney;
            }else if(v == 10 && that.data.pg_amoney){
                var pg_insmoney = that.data.pg_amoney;
                money = that.data.pg_money;
                instance_money = pg_insmoney;
            }else{
                money = that.data.money;
                instance_money = that.data.instance_money;
            }
        }else{
            money = that.data.money;
            instance_money = that.data.instance_money;
        }
        that.ins_money = money;
        if(v == 6) v = 5;
        $('#renew-money').html(changeTwoDecimal(money * v));
        $('#renew-instance-money').html(changeTwoDecimal(instance_money * v));
        $('#renew-ip-money').html(that.data.ip_money * v);
        $('#renew-storage-money').html(that.data.storage_money * v);
        buildCoupon(money * v, that.data['coupon'], 'sin');
        //paycross
        $('#renew-month').val(mt);
        if ($('#renew-money').text() * 1 > that.data.accout_money) {
            var surplus = changeTwoDecimal($('#renew-money').text() * 1 - that.data.accout_money);
            $('#renew-submit-ol').show();
            $('#renew-submit').hide();
            $('.payway').show();
            $('#paysup').html(surplus);
        }else{
            $('#renew-submit').show();
            $('.payway').hide();
            $('#renew-submit-ol').hide();
        }
    });

    $('#renews-cycle').click(function (e) {
        var obj = $(e.target), v = mt = obj.val() * 1;
        if(v == 6) v = 5;
        $('#renews-cycle .active').removeClass('active');
        obj.addClass('active');
        var m = that.data.money * v;
        $('#renews-money').html(that.data.money * v);
        buildCoupon(that.data.money * v, that.data['coupon'], 'bal');
        //paycross
        $('#renews-month').val(mt);
        if ($('#renews-money').text() * 1 > that.data.accout_money) {
            var surplus = changeTwoDecimal($('#renews-money').text() * 1 - that.data.accout_money);
            $('#renews-submit-ol').show();
            $('#renews-submit').hide();
            $('.payways').show();
            $('#paysups').html(surplus);
        }else{
            $('#renews-submit').show();
            $('.payways').hide();
            $('#renews-submit-ol').hide();
        }
    });
    $('#renew-coupon').change(function () {
        var cp = $(this).val(), mon = $('#renew-cycle .active').val();
        if(mon == 6) mon = 5;
        if (cp.length > 10) {
            $('#renew-money').html(Math.max(0.01,that.ins_money * mon - that.data['coupon'][cp]['money']));
        }else{
            $('#renew-money').html(that.ins_money * mon);
        }
        //paycross
        $('#renew-coupon-ol').val(cp);
        if ($('#renew-money').text() * 1 > that.data.accout_money) {
            var surplus = changeTwoDecimal($('#renew-money').text() * 1 - that.data.accout_money);
            $('#renew-submit-ol').show();
            $('#renew-submit').hide();
            $('.payway').show();
            $('#paysup').html(surplus);
        }else{
            $('#renew-submit').show();
            $('.payway').hide();
            $('#renew-submit-ol').hide();
        }
    });
    $('#renews-coupon').change(function () {
        var cp = $(this).val(), mon = $('#renews-cycle .active').val();
        if(mon == 6) mon = 5;
        if (cp.length > 10) {
            $('#renews-money').html(Math.max(0.01,that.data.money * mon - that.data['coupon'][cp]['money']));
        }else{
            $('#renews-money').html(that.data.money * mon);
        }
        //paycross
        $('#renews-coupon-ol').val(cp);
        if ($('#renews-money').text() * 1 > that.data.accout_money) {
            var surplus = changeTwoDecimal($('#renews-money').text() * 1 - that.data.accout_money);
            $('#renews-submit-ol').show();
            $('#renews-submit').hide();
            $('.payways').show();
            $('#paysups').html(surplus);
        }else{
            $('#renews-submit').show();
            $('.payways').hide();
            $('#renews-submit-ol').hide();
        }
    });
    $('#renew-submit').click(function () {
        if ($('#renew-money').text() * 1 > that.data.accout_money) {
            toastr.info('帐户余额不足，请先充值！', '', {positionClass: "toast-top-center"});
            return;
        }
        $('#renew-submit').prop('disabled', true);
        $.getJSON(urls.renews, {sns: that.sn, renews_cycle: $('#renew-cycle .active').val(), coupon:$('#renew-coupon').val()}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#expired_' + that.sn).html(dt.data[that.sn].expired);
                toastr.info('续费成功', '', {positionClass: "toast-top-center"});
                $('#renewModel').modal('hide');
                /*if (dt.data.status != qyStatusAsc.STATUS_RUN && confirm("确认开启服务器吗？")) {
                 _start(that.sn);
                 }*/
            }
        });
    });
    $('#renews-submit').click(function () {
        if ($('#renews-money').text() * 1 > that.data.accout_money) {
            toastr.info('帐户余额不足，请先充值！', '', {positionClass: "toast-top-center"});
            return;
        }
        $('#renews_waiting').css('display','block');
        $('#renews-submit').prop('disabled', true);
        var sns = that.sn.join(',');
        $.getJSON(urls.renews, {sns: sns, renews_cycle: $('#renews-cycle .active').val(), coupon:$('#renews-coupon').val()}, function (dt) {
            $('#renews_waiting').css('display','none');
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                for (var i in that.sn) {
                    $('#expired_' + that.sn[i]).html(dt.data[that.sn[i]].expired);
                }
                toastr.info('续费成功', '', {positionClass: "toast-top-center"});
                $('#renewsModel').modal('hide');
                /*if (dt.data.status != qyStatusAsc.STATUS_RUN && confirm("确认开启服务器吗？")) {
                 _start(that.sn);
                 }*/
            }
        });
    });
    //paycross
    $('.bank').click(function () {
        if($(this).val() == 2){
            $('input[name=payType]').val('alipay');
        }else{
            $('input[name=payType]').val('wxpay');
        }
    });

    $('.btn-renew').click(function () {
        that.sn  = $(this).attr('data-sn');
        that.tag = $('#tag_' + that.sn).text();
        var ip_reg = /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/;
        that.ip  = ip_reg.exec($('#ip_' + that.sn).html());
        $('#renew-name').html(that.tag);
        $('#renew-ip').html(that.ip);
        $('#renew-coupon').html('<option value="">暂无代金券</option>');
        $('#renew-coupon-tr').hide();
        $('#renewModel').modal();
        $('#renew-submit').show();
        $('.payway').hide();
        $('#renew-submit-ol').hide();
    });

    $('.btn-renews').click(function () {
        that.sn = [];
        $('.selectable-item:checked').each(function() {
            that.sn.push($(this).val());
        });
        if (that.sn.length < 1) {
            toastr.info('请选择要续费的云服务器', '', {positionClass: "toast-top-center"});
            return;
        }
        $('#renews-num').html(that.sn.length);
        $('#renewsModel').modal();
        $('#renews-submit').show();
        $('.payways').hide();
        $('#renews-submit-ol').hide();
    });
}();

var storageRenew = function () {
    var that = {};
    $('.btn-st-renew').click(function () {
        that.sn     = $(this).attr('data-sn');
        that.tag    = $(this).attr('data-tag');
        that.status = $('#status_' + that.sn).html();
        $('#st_renewModel').modal();
    });
    $('#st_renewModel').on('show.bs.modal', function () {
        $('#st_renew-name').text(that.tag);
        $.getJSON(urls.st_renew, {sn: that.sn}, function (res) {
            that.unit_price = res.unit_price;
            that.size       = res.data.size;
            that.mouth      = $('#st-charge-cycle .active').val();
            if (res.error == 1) {
                alert(res.msg);
                $('#st_renewModel').modal('hide');
            } else {
                cal_price();
                $('#st_renew-acount-money').html(res.account);
            }
            console.log(that);
        });
        $('.st-renewmouth').click(function () {
            $('#st-charge-cycle .active').removeClass('active');
            $(this).addClass('active');
            that.mouth = $(this).val();
            cal_price();
        });
    });
    $('#st_renew-submit').click(function () {
        $.getJSON(urls.st_renew, {sn: that.sn, renewmouth: that.mouth}, function (res) {
            if (res.error == 1) {
                $('#msg-box').html(res.msg);
                $('#messageModel').modal();
            } else {
                $('#msg-box').html('续费成功！');
                $('#messageModel').modal();
            }
            $('#st_renewModel').modal('hide');
        })
    });
    function cal_price() {
        var total_price = that.mouth * that.unit_price * that.size;
        $('#st_renew-money').text(total_price.toFixed(2));
    }
}();


var ipRenew     = function () {
    var that           = {};
    var calculateMoney = function () {
        var month = that.mouth;
        var money = that.data.price * month;
        $('#ip_renew-money').text(money+'元');
        return money;
    };
    $('#ip_renewModel').on('shown.bs.modal', function () {
        $.getJSON(urls.ip_renew, {sn: that.sn, opt: 'show'}, function (dt) {
            that.mouth      = $('#charge-cycle .active').val();
            if (dt.error * 1 == 1) {
                $('#ip_renew-footer').hide();
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                $('#ip_renew-account').text(dt.data.account+'元');
                $('#ip_renew-org-bandwidth').html(dt.data.bandwidth + 'M');
                calculateMoney();
            }
        });
        $('.renewmouth').click(function () {
            $('#charge-cycle .active').removeClass('active');
            $(this).addClass('active');
            that.mouth = $(this).val();
            calculateMoney();
        });
    });
    $('#ip_renew-submit').click(function () {
        if (calculateMoney() > that.data.account * 1) {
            toastr.info('帐户余额不足，请先充值！', '', {positionClass: "toast-top-center"});
            return false;
        }
        $('#ip_renew-footer').hide();
        $.getJSON(urls.ip_renew, {sn: that.sn, month: that.mouth, opt: 'renew'}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                toastr.info('续费IP成功', '', {positionClass: "toast-top-center"});
                $('#ended-' + that.sn).html(dt.data.ended);
            }
        });
        $('#ip_renewModel').modal('hide');
    });
    $('.btn-ip-renew').click(function () {
        that.sn = $(this).attr('data-sn');
        that.tag = $(this).attr('data-tag');
        $('#ip_renew-name').html(that.tag);
        $('#ip_renewModel').modal();
        $('#ip_renew-footer').show();
    });
}();

var start = function () {
    var that = {};
    $('#startModel').on('shown.bs.modal', function () {
        $.getJSON(urls.start, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#start-list').html('<tr><td>没有数据</td></tr>');
                $('#start-footer').hide();
            } else {
                that.data = dt.data;
                var msg   = (dt.data.status * 1 == qyStatusAsc.STATUS_SHUTDOWN) ? '<span class="text-success">可开机</span>'
                    : '<span class="text-warnnig">不可开机</span>';
                $('#start-list').html('<tr><td>1</td><td>' + dt.data.tag + '</td><td>' + msg + '</td></tr>');
            }
        });
    });
    $('#start-submit').click(function () {
        $('#start-footer').hide();
        $.getJSON(urls.start, {sn: that.sn, opt: 1}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#status_' + that.sn).html(getStatusHtml('STATUS_START_ING'));
            }
        });
        $('#startModel').modal('hide');
    });
    $('.btn-start').click(function () {
        that.sn = $(this).attr('data-sn');
        $('#startModel').modal();
        $('#start-footer').show();
    });
}();

var reboot = function () {
    var that = {};
    $('#rebootModel').on('shown.bs.modal', function () {
        $.getJSON(urls.reboot, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                var msg   = (dt.data.status * 1 == qyStatusAsc.STATUS_RUN) ? '<span class="text-success">可重启</span>'
                    : '<span class="text-warnnig">不可重启</span>';
                $('#reboot-list').html('<tr><td>1</td><td>' + dt.data.tag + '</td><td>' + msg + '</td></tr>');
            }
        });
    });
    $('#reboot-submit').click(function () {
        $('#reboot-footer').hide();
        var opt = $('#reboot-opt').prop('checked') ? 'destroy' : 'reboot';
        $('#status_' + that.sn).html('<span class="loading">重启中</span>');
        $.getJSON(urls.reboot, {sn: that.sn, opt: opt}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#status_' + name).html(getStatusHtml('STATUS_START_ING'));
            }
        });
        $('#rebootModel').modal('hide');
    });
    $('.btn-reboot').click(function () {
        that.sn = $(this).attr('data-sn');
        $('#rebootModel').modal();
        $('#reboot-footer').show();
    });
}();

var shutdown = function () {
    var that = {};
    $('#shutdownModel').on('shown.bs.modal', function () {
        $.getJSON(urls.shutdown, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                var msg   = (dt.data.status * 1 == qyStatusAsc.STATUS_RUN) ? '<span class="text-success">可关机</span>'
                    : '<span class="text-warnnig">不可关机</span>';
                $('#shutdown-list').html('<tr><td>1</td><td>' + dt.data.tag + '</td><td>' + msg + '</td></tr>');
            }
        });
    });
    $('#shutdown-submit').click(function () {
        $('#shutdown-footer').hide();
        var opt = $('#shutdown-opt').prop('checked') ? 'destroy' : 'reboot';
        $('#status_' + that.sn).html('<span class="loading">关机中</span>');
        $('#shutdownModel').modal('hide');
        $.getJSON(urls.shutdown, {sn: that.sn, opt: opt}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#status_' + name).html(getStatusHtml('STATUS_SHUTDOWN_ING'));
            }
        });
        $('#shutdownModel').modal('hide');
    });
    $('.btn-shutdown').click(function () {
        that.sn = $(this).attr('data-sn');
        $('#shutdownModel').modal();
        $('#shutdown-footer').show();
    });
}();

var destroy = function () {
    var that = {};
    $('#destroyModel').on('shown.bs.modal', function () {
        $.getJSON(urls.destroy, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                that.data = dt.data;
                var msg   = '<span class="text-danger">强制关机-谨慎操作</span>';
                $('#destroy-list').html('<tr><td>1</td><td>' + dt.data.tag + '</td><td>' + msg + '</td></tr>');
            }
        });
    });
    $('#destroy-submit').click(function () {
        $('#destroy-footer').hide();
        var opt = $('#destroy-opt').prop('checked') ? 'destroy' : 'reboot';
        $('#status_' + that.sn).html('<span class="loading">关机中</span>');
        $('#destroyModel').modal('hide');
        $.getJSON(urls.destroy, {sn: that.sn, opt: opt}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#status_' + name).html(getStatusHtml('STATUS_SHUTDOWN_ING'));
            }
        });
        $('#destroyModel').modal('hide');
    });
    $('.btn-destroy').click(function () {
        that.sn = $(this).attr('data-sn');
        $('#destroyModel').modal();
        $('#destroy-footer').show();
    });
}();

var password = function () {
    var that = {};
    $('#passwordModel').on('shown.bs.modal', function () {
        $('#password-name').html(that.tag);
        $.getJSON(urls.password, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#password-form').remove();
                $('#password-submit').remove();
            }
        });
    });
    $('#password-submit').click(function () {
        var no = $('#password-new'), ro = $('#password-rep'), pwdLen = $.trim(no.val()).length;
        if (pwdLen < 8 || pwdLen > 16) {
            toastr.info('密码长度应在8-16个字符', '', {positionClass: "toast-top-center"});
            no.focus();
            return;
        }
        if (no.val() != ro.val()) {
            toastr.info('两次输入密码不一致', '', {positionClass: "toast-top-center"});
            ro.focus();
            return;
        }
        nv = no.val();
        if (!/[A-Z]/.test(nv) || !/[a-z]/.test(nv) || !/[0-9]/.test(nv)) {
            toastr.info('密码必须包裹 大小字母 与 数字', '', {positionClass: "toast-top-center"});
            ro.focus();
            return;
        }
        $('#password-footer').hide();
        $.getJSON(urls.password, {sn: that.sn, password: no.val(), password_rep: ro.val()}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                toastr.info('修改密码成功', '', {positionClass: "toast-top-center"});
                $('#status_' + that.sn).html(getStatusHtml('STATUS_START_ING'));
            }
        });
        $('#passwordModel').modal('hide');
    });
    $('.btn-password').click(function () {

        var $this = $(this),
            options = $.extend(true, {}, {theme: 'bootstrap'}, $this.data());
        if (typeof options.theme !== 'undefined') {
            alertify.theme(options.theme);
        }
        alertify.cancelBtn('取消');
        alertify.okBtn('已关机');

        alertify.confirm(options.confirmTitle, function() {
            that.sn  = $this.attr('data-sn');
            that.tag = $('#tag_' + that.sn).text();
            $('#passwordModel').modal();
            $('#password-new').val('');
            $('#password-rep').val('');
            $('#password-footer').show();
        }, function() {
            alertify.error(options.errorMessage);
        });
    });
}();
var vncpwd   = function () {
    var that = {};
    $('#vncpwdModel').on('shown.bs.modal', function () {
        $('#vncpwd-name').html(that.tag);
        $.getJSON(urls.vncpwd, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#vncpwd-form').remove();
                $('#vncpwd-submit').remove();
            }
        });
    });
    $('#vncpwd-submit').click(function () {
        var no = $('#vncpwd-new'), ro = $('#vncpwd-rep');
        if ($.trim(no.val()).length < 6) {
            toastr.info('密码长度不能小于6', '', {positionClass: "toast-top-center"});
            no.focus();
            return;
        }
        if (no.val() != ro.val()) {
            toastr.info('两次输入密码不一致', '', {positionClass: "toast-top-center"});
            ro.focus();
            return;
        }
        $('#vncpwd-footer').hide();
        $.getJSON(urls.vncpwd, {sn: that.sn, password: no.val(), password_rep: ro.val()}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                toastr.info('修改VNC密码成功,请重启服务器', '', {positionClass: "toast-top-center"});
                $('#status_' + that.sn).html(getStatusHtml('STATUS_START_ING'));
            }
        });
        $('#vncpwdModel').modal('hide');
    });
    $('.btn-vncpwd').click(function () {
        that.sn  = $(this).attr('data-sn');
        that.tag = $('#tag_' + that.sn).text();
        $('#vncpwdModel').modal();
        $('#vncpwd-new').val('');
        $('#vncpwd-rep').val('');
        $('#vncpwd-footer').show();
    });
}();
var snapshot = function () {
    var that = {};
    $('#snapshotModel').on('shown.bs.modal', function () {
        $('#snapshot-name').html(that.tag);
        $.getJSON(urls.snapshot, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#snapshot-footer').remove();
            } else {
                that.dev   = dt.data;
                var o, ins = [];
                for (o in dt.data) {
                    ins.push('<label><input type="checkbox" name="snapshot-dev[]" checked value="' + dt.data[o] + '"> ' + dt.data[o] + '盘</label>');
                }
                $('#snapshot-dev').html(ins.join('')).hide();
                $('#snapshot-tag').val(dt.tag);
                $('#snapshot-table').show();
                $('#snapshot-footer').show();
            }
        });
    });
    $('#snapshot-submit').click(function () {
        var v = $.trim($('#snapshot-tag').val());
        if (v.length > 32) {
            toastr.info('备份名称字符数不能超过32个', '', {positionClass: "toast-top-center"});
            return false;
        }
        $('#snapshot-dev').html('<span class="loading">创建备份中...</span>').show();
        $('#snapshot-footer').hide();
        $.getJSON(urls.snapshot, {sn: that.sn, dev: that.dev, tag: v, opt: 1}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                toastr.info('创建备份成功', '', {positionClass: "toast-top-center"});
                $('#snapshot-dev').html('创建备份成功').show();
                $('#snapshotModel').modal('hide');
            }
        });
    });
    $('.btn-snapshot').click(function () {
        that.sn  = $(this).attr('data-sn');
        that.tag = $('#tag_' + that.sn).text();
        $('#snapshot-dev').html('<span class="loading">加载中</span>').show();
        $('#snapshot-tag').val('');
        $('#snapshot-table').hide();
        $('#snapshot-footer').hide();
        $('#snapshotModel').modal();
    });
}();
var recovery = function () {
    var that = {};
    $('#recoveryModel').on('shown.bs.modal', function () {
        $('#recovery-name').html(that.tag);
        $('#recovery-dev').show();
        $.getJSON(urls.recovery, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#recovery-footer').remove();
            } else {
                that.dev      = dt.data;
                var i, o, ins = [];
                for (i in dt.data) {
                    o = dt.data[i];
                    ins.push('<tr><td><input type="radio" id="recovery-snapshot-' + o.sn + '" name="recovery-snapshot" value="' + o.sn + '"></td>'
                        + '<td><label for="recovery-snapshot-' + o.sn + '">' + o.tag + '</label></td>'
                        + '<td>' + o.created + '</td>'
                    );
                }
                if (ins.length > 0) {
                    $('#recovery-list').html(ins.join(''));
                } else {
                    $('#recovery-list').html('<tr><td>没有可以恢复的备份！</td></tr>');
                }
                $('#recovery-table').show();
                $('#recovery-footer').show();
                $('#recovery-dev').hide();
            }
        });
    });
    $('#recovery-submit').click(function () {
        var snapshot = $('#recovery-list input:checked').val();
        if (!snapshot) {
            toastr.info('请选择回恢复备份点', '', {positionClass: "toast-top-center"});
            return;
        }
        $('#recovery-dev').html('<span class="loading">恢复数据中...</span>').show();
        $('#recovery-footer').hide();
        $.getJSON(urls.recovery, {sn: that.sn, snapshot: snapshot}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                toastr.info('恢复数据成功', '', {positionClass: "toast-top-center"});
                $('#recovery-dev').html('恢复数据成功').show();
                $('#recoveryModel').modal('hide');
            }
        });
    });
    $('.btn-recovery').click(function () {
        that.sn  = $(this).attr('data-sn');
        that.tag = $('#tag_' + that.sn).text();
        $('#recoveryModel').modal();
    });
}();
var upgrade  = function () {
    var that = {};
    $('#upgradeModel').on('shown.bs.modal', function () {
        $('#upgrade-name').html(that.tag);
        $('#upgrade-dev').show();
        $.getJSON(urls.upgrade, {sn: that.sn, opt: 'init'}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#upgrade-footer').remove();
            } else {
                that.data = dt.data;
                $('#upgrade-org-cpu').text(dt.data.vcpu);
                $('#upgrade-org-memory').text(dt.data.memory);
                $('#upgrade-org-bandwidth').text(dt.data.bandwidth);
                $('#upgrade-accout-money').text(dt.data.accout_money);
                $('#upgrade-charge').html(0);
                $('#upgradeModel .ended').text(dt.data.ended);
                $('#upgradeModel .total-money').text(dt.data.money);
                var i = dt.data.vcpu * 1, m = dt.data.max_vcpu * 1, ct = [1, 2, 4, 8, 12, 16, 32], len = ct.length, j = 0, t = [];
                for (j; j < len; j++) {
                    if (ct[j] < i) continue;
                    t.push('<option value="' + ct[j] + '">' + ct[j] + '核</option>');
                }
                $('#upgrade-cpu').html(t.join(''));
                i      = dt.data.memory * 1;
                var mt = [1, 2, 4, 8, 12, 16, 24, 32, 48, 64, 96, 128];
                t      = [];
                for (j = 0; j < mt.length; j++) {
                    if (mt[j] < i) continue;
                    t.push('<option value="' + mt[j] + '">' + mt[j] + 'G</option>');
                }
                $('#upgrade-memory').html(t.join(''));
                i = dt.data.bandwidth * 1;
                m = 200;
                t = [];
                for (i; i <= m; i++) {
                    t.push('<option value="' + i + '">' + i + 'M</option>');
                }
                $('#upgrade-bandwidth').html(t.join(''));

                $('#upgrade-table').show();
                $('#upgrade-footer').show();
                $('#upgrade-dev').hide();
            }
        });
    });
    $('#upgrade-table select').change(function () {
        var cpu = $('#upgrade-cpu').val() * 1, memory = $('#upgrade-memory').val() * 1, bandwidth = $('#upgrade-bandwidth').val() * 1;
        $.getJSON(urls.upgrade, {sn: that.sn, cpu: cpu, memory: memory, bandwidth: bandwidth, opt: 'calculate'}, function (dt) {
            $('#upgrade-charge').html(dt.data.needMoney);
            $('#upgradeModel .total-money').text(dt.data.money);
        });
    });
    $('#upgrade-submit').click(function () {
        var cpu = $('#upgrade-cpu').val() * 1, memory = $('#upgrade-memory').val() * 1, bandwidth = $('#upgrade-bandwidth').val() * 1;
        if (cpu == that.data.vcpu * 1 && memory == that.data.memory * 1 && bandwidth == that.data.bandwidth * 1) {
            toastr.info('你的服务器配置没有调整', '', {positionClass: "toast-top-center"});
            return;
        }
        if ($('#upgrade-charge').text() * 1 != that.data.accout_money * 1 && $('#upgrade-charge').text() * 1 > that.data.accout_money * 1) {
            toastr.info('你的余额不足，请先充值', '', {positionClass: "toast-top-center"});
            return;
        }
        if (cpu != that.data.vcpu * 1 || memory != that.data.memory * 1) {
            if (!confirm('升级将重启服务器，确定升级吗?')) {
                return;
            }
        }
        $('#upgrade-dev').html('<span class="loading">升级中...</span>').show();
        $('#upgrade-footer').hide();
        $.getJSON(urls.upgrade, {sn: that.sn, cpu: cpu, memory: memory, bandwidth: bandwidth, opt: 'upgrade'}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#info_' + that.sn).html('CPU ' + cpu + '核,内存' + memory + 'G');
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#upgrade-dev').html('升级成功').show();
                $('#upgradeModel').modal('hide');
            }
        });
    });
    $('.btn-upgrade').click(function () {
        that.sn  = $(this).attr('data-sn');
        that.tag = $('#tag_' + that.sn).text();
        $('#upgrade-table').hide();
        $('#upgrade-footer').hide();

        $('#upgrade-org-cpu').html('');
        $('#upgrade-org-memory').html('');
        $('#upgrade-org-bandwidth').html('');
        $('#upgrade-cpu').html('');
        $('#upgrade-memory').html('');
        $('#upgrade-bandwidth').html('');
        $('#upgradeModel .ended').text('');
        $('#upgradeModel .total-money').text('');
        $('#upgradeModel').modal();
    });
}();
var defense  = function () {
    var that = {};
    $('#defenseModel').on('shown.bs.modal', function () {
        $('#defense-name').html(that.tag);
        $('#defense-dev').show();
        $.getJSON(urls.defense, {sn: that.sn, opt: 'init'}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#defense-footer').hide();
            } else {
                that.data = dt.data;
                $('#defense-org-cpu').text(dt.data.vcpu);
                $('#defense-org-memory').text(dt.data.memory);
                $('#defense-org-bandwidth').text(dt.data.bandwidth);
                $('#defense-org-defense').text(dt.data.defense);
                $('#defense-accout-money').text(dt.data.accout_money);
                $('#defenseModel .ended').text(dt.data.ended);
                $('#defenseModel .total-money').text(dt.data.money);
                var i, o = dt.data.defense * 1, arr = dt.data.reg_def;
                t        = [];
                for (i in arr) {
                    if (arr[i] < o) continue;
                    if (arr[i] < that.data.minDefense) continue;
                    if (arr[i] > that.data.maxDefense) continue;
                    t.push('<option value="' + arr[i] + '">' + arr[i] + ' G</option>');
                }
                if (t.length < 1) {
                    $('#defense-defense').html('<option value="">不支付升级</option>');
                    $('#defense-table').show();
                    return;
                }
                $('#defense-defense').html(t.join(''));
                if (o != 20) {
                    var defense = $('#defense-defense').val() * 1;
                    $.getJSON(urls.defense, {sn: that.sn, defense: defense, opt: 'calculate'}, function (dt) {
                        $('#defense-charge').html(dt.data);
                    });
                }

                $('#defense-table').show();
                $('#defense-footer').show();
            }
            $('#defense-dev').hide();
        });
    });
    $('#defense-defense').change(function () {
        var defense = $('#defense-defense').val() * 1;
        $.getJSON(urls.defense, {sn: that.sn, defense: defense, opt: 'calculate'}, function (dt) {
            $('#defense-charge').html(dt.data.needMoney);
            $('#defenseModel .total-money').text(dt.data.money);
        });
    });
    $('#defense-submit').click(function () {
        var defense = $('#defense-defense').val() * 1;
        if (defense == that.data.defense * 1) {
            toastr.info('你的服务器防御值没有调整', '', {positionClass: "toast-top-center"});
            return;
        }
        if ($('#defense-charge').text() * 1 != that.data.accout_money * 1 && $('#defense-charge').text() * 1 > that.data.accout_money * 1) {
            toastr.info('你的余额不足，请先充值', '', {positionClass: "toast-top-center"});
            return;
        }
        $('#defense-dev').html('<span class="loading">升级中...</span>').show();
        $('#defense-footer').hide();
        $.getJSON(urls.defense, {sn: that.sn, defense: defense, opt: 'upgrade'}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#defense_' + that.sn).html(defense + 'G');
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#defense-dev').html('升级成功').show();
                $('#defenseModel').modal('hide');
            }
        });
    });
    $('.btn-defense').click(function () {
        that.sn  = $(this).attr('data-sn');
        that.tag = $('#tag_' + that.sn).text();
        $('#defense-table').hide();
        $('#defense-footer').hide();

        $('#defense-org-cpu').html('');
        $('#defense-org-memory').html('');
        $('#defense-org-bandwidth').html('');
        $('#defense-defense').html('');
        $('#defenseModel .ended').text('');
        $('#defenseModel .total-money').text('');
        $('#defenseModel').modal();
    });
}();

var install = function () {
    var that = {};
    var step = 0;

    $('#installModel').on('shown.bs.modal', function () {
        $('#install-name').html(that.sn);
        $.getJSON(urls.install, {sn: that.sn}, function (dt) {
            if (dt.error * 1 == 1) {
                $('#install-footer').hide();
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#install-footer').show();
                that.data = dt.data;
                var msg   = (dt.data.status * 1 < qyStatusAsc.STATUS_EXPIRED) ? '<span class="text-success">可重装系统</span>'
                    : '<span class="text-warnnig">不可重装系统</span>';
                $('#install-list').html('<tr><td>1</td><td>' + dt.data.tag + '</td><td>' + msg + '</td></tr>');
                if (dt.data.status * 1 != 1) $('#install-table').show();
                $('#install-current-image').text(that.system_name = dt.data.sysimg_name + ' ' + dt.data.sysimg_bits + '位');
                if(dt.data.install_sms != 2) {
                    $('.getCodemsg').show();
                    $('.getCode').hide();
                }else{
                    $('.getCodemsg').hide();
                    $('.getCode').show();
                }
            }
        });
    });
    $(that).on('step', function () {
        step %= 4;
        $('#installModel .hide-' + step).hide();
        $('#installModel .show-' + step).show();
        if (step == 0) {
            $('#install-submit').text('下一步');
        } else if (step == 1) {
            $.getJSON(urls.image, {sn: that.sn}, function (dt) {
                if (dt.error * 1 == 1) {
                    toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                } else {
                    var lis = [], len = dt.data.system.length, i, o, chk;
                    for (i in dt.data.system) {
                        o   = dt.data.system[i];
                        chk = i > 0 ? '' : 'checked';
                        lis.push('<li class="radio-custom radio-primary">'
                            + '<input type="radio" id="system_image_' + o.id + '" name="oimage" value="system-' + o.id + '" ' + chk + '> '
                            + '<label for="system_image_' + o.id + '">'
                            + o.name + ' ' + o.bits + ' 位' + '</label></li>');
                    }
                    $('#install_public_image').html(lis.join(''));
                    i   = 0;
                    lis = [];
                    for (i in dt.data.user) {
                        o = dt.data.user[i];
                        lis.push('<li class="radio-custom radio-primary">'
                            + '<input type="radio" id="user_image_' + o.id + '" name="oimage" value="user-' + o.id + '"> '
                            + '<label for="user_image_' + o.id + '">'
                            + o.name + ' ' + o.bits + ' 位' + '</label></li>');
                    }
                    $('#install_user_image').html(lis.join(''));
                }
            });
            $('#install-submit').text('下一步');
        } else if (step == 2) {
            that.image = $('#install-first input:checked').val() * 1;
            if (that.image == 0) {
                var chk          = $('#install-second input:checked');
                that.image       = $(chk).val();
                that.system_name = $.trim($(chk).parent().text());
            }

            $('#install-third-image').text(that.system_name);
            $('#install-submit').text('开始安装');
        } else if (step == 3) {
            $('#install-footer').hide();
            $.getJSON(urls.install, {sn: that.sn, code: that.code, image: that.image, password: $('#install-third-password').val()}, function (dt) {
                if (dt.error * 1 == 1) {
                    toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                } else {
                    $('#status_' + that.sn).html(getStatusHtml('STATUS_INSTALL'));
                }
            });
            $('#installModel').modal('hide');
        }
    });
    $('#install-submit').click(function () {
        if (step == 2) {
            if (that.data.status * 1 == qyStatusAsc.STATUS_INSTALL) {
                $('#install-table').show();
                toastr.info('此云服务器正安装系统中，请稍后再安装系统', '', {positionClass: "toast-top-center"});
                return false;
            }

            var o = $('#install-third-password'), v = $.trim(o.val());
            if (v != '') {
                if (v.length < 8) {
                    toastr.info('密码需要8-16个字符', '', {positionClass: "toast-top-center"});
                    o.focus();
                    return false;
                }
                if (!/[A-Z]/.test(v) || !/[a-z]/.test(v) || !/[0-9]/.test(v)) {
                    toastr.info('密码必须包裹 大小字母 与 数字', '', {positionClass: "toast-top-center"});
                    o.focus();
                    return false;
                }
                if (!/^[a-zA-Z0-9]{8,16}$/.test(v)) {
                    toastr.info('密码必须是8-16个字符，同时包含大小写字母和数字，不支持特殊符号', '', {positionClass: "toast-top-center"});
                    o.focus();
                    return false;
                }
            }

            if(that.data.install_sms == 2) {
                var code = $('#code').val();
                that.code  = $.trim(code);
                if (!(/^[0-9]{6}$/.test(that.code))) {
                    $('#code').focus();
                    toastr.info('请输入正确验证码', '', {positionClass: "toast-top-center"});
                    return false;
                }
            }
        }
        if (step == 0 && $('#install-first input:checked').val() * 1 == 1) {
            step += 2;
        } else {
            step += 1;
        }
        $(that).trigger('step');
    });
    $('#install-pre').click(function () {
        if (step == 2 && $('#install-first input:checked').val() * 1 == 1) {
            step = 0;
        } else {
            step -= 1;
        }
        $(that).trigger('step');
    });

    $('.btn-install').click(function () {
        that.sn = $(this).attr('data-sn');
        step    = 0;
        $(that).trigger('step');
        $('#install-first-default').prop('checked', true);

        $('#installModel').modal();
        $('#install-footer').show();
        $('#install-third-password').val('');
    });

    //验证码
    var time ,timer;
    function remainningTime(){
        time--;
        if(time<=0){
            resetBtn();
        }else{
            $("#getCode").html("重新获取("+time+"S)");
        }
    }

    function resetBtn() {
        clearInterval(timer);
        $("#getCode").removeClass('getting').html("获取短信验证码");
    }

    $('#getCode').click(function () {
        if($("#getCode").hasClass("getting")) {
            return;
        }
        $("#getCode").addClass("getting");

        $.ajax({
            type: 'POST',
            url : urls.getSmsCode,
            dataType: 'json',
            success:function(res) {
                if(res == 1){
                    //修改按键样式
                    time=100;
                    timer=setInterval(remainningTime,1000);
                    return;
                }
                resetBtn();
                if(res == 0){
                    toastr.info('短信验证码发送失败，请稍候重新获取', '', {positionClass: "toast-top-center"});
                }else if(res == -1){
                    toastr.info('请不要频繁获取短信验证码', '', {positionClass: "toast-top-center"});
                }else if(res == -2){
                    toastr.info('手机号码有误，请联系售后人员', '', {positionClass: "toast-top-center"});
                }else if(res == -3){
                    toastr.info('手机号码格式不正确', '', {positionClass: "toast-top-center"});
                }
            }
        });
    });
}();

var tag = function () {
    var that = {};
    $('#tagModel').on('shown.bs.modal', function () {
        $('#tag-name').html(that.tag);
        $.getJSON(urls.tag, {sn: that.sn}, function (dt) {
            $('#tag-dev').html('');
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#tag-footer').remove();
            } else {
                $('#tag-tag').val(dt.data.tag);
                $('#tag-desc').val(dt.data.desc);
                $('#tag-table').show();
                $('#tag-footer').show();
            }
        });
    });
    $('#tag-submit').click(function () {
        var v = $.trim($('#tag-tag').val()), desc = $.trim($('#tag-desc').val());
        if (v.length > 32) {
            toastr.info('服务器新名称字符数不能超过32个', '', {positionClass: "toast-top-center"});
            return false;
        }
        if (desc.length > 200) {
            toastr.info('服务器描述字符数不能超过200个', '', {positionClass: "toast-top-center"});
            return false;
        }
        $('#tag-dev').html('<span class="loading">修改名称字中...</span>').show();
        $.getJSON(urls.tag, {sn: that.sn, tag: v, desc: desc}, function (dt) {
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            } else {
                $('#tag_' + that.sn).html(v);
                $('#desc_' + that.sn).html(desc);
                toastr.info('修改名称成功', '', {positionClass: "toast-top-center"});
                $('#tagModel').modal('hide');
            }
        });
    });
    $('.btn-tag').click(function () {
        that.sn  = $(this).attr('data-sn');
        that.tag = $('#tag_' + that.sn).text();
        $('#tag-dev').html('<span class="loading">加载中</span>').show();
        $('#tag-tag').val('');
        $('#tag-desc').val('');
        $('#tag-table').hide();
        $('#tag-footer').hide();
        $('#tagModel').modal();
    });
}();

var inip = function () {
    var that = {};
    $('#inipModel').on('shown.bs.modal', function () {
        $.getJSON(urls.inip, {sn: that.sn}, function (dt) {
            $('#inip-dev').html('');
            if (dt.error * 1 == 1) {
                toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
                $('#inip-footer').hide();
            } else {
                if(!dt.data['inip'] || $.isEmptyObject(dt.data['inip'])){
                    toastr.info('暂无内网，请先创建', '', {positionClass: "toast-top-center"});
                    return false;
                }
                var i,t = [], o=dt.data['inip'];
                for(i in o){
                    t.push('<option value="'+i+'">'+o[i]['ip']+' / 24 ['+o[i]['name']+']</option>');
                }
                $('#ipip-ipip_id').html(t.join(''));
                $('#inip-table').show();
                $('#inip-footer').show();
            }
        });
    });
    $('#inip-submit').click(function () {
        var v = $.trim($('#ipip-ipip_id').val());
        console.log(v);
        if (v.length <1) {
            toastr.info('请选择内网', '', {positionClass: "toast-top-center"});
            return false;
        }
        $('#inip-footer').hide();
        $('#inip-dev').html('<span class="loading">加入内网中...</span>').show();
        $.getJSON(urls.inip, {sn: that.sn, inip_id: v}, function (dt) {
            toastr.info(dt.msg, '', {positionClass: "toast-top-center"});
            if (dt.error * 1 == 0) {
                $('#inip_' + that.sn).html('内网：'+dt.data);
                $('#inipModel').modal('hide');
            }
        });
    });
    $('.btn-inip').click(function () {
        that.sn  = $(this).attr('data-sn');
        $('#inip-dev').html('<span class="loading">加载中</span>').show();
        $('#inip-name').html(that.sn);
        $('#inip-ip').html($('#ip_' + that.sn).html().replace(/<br>/g, ';'));

        $('#inip-table').hide();
        $('#inip-footer').hide();
        $('#inipModel').modal();
    });
}();
var changeTwoDecimal = function(x){
    var f_x = parseFloat(x);
    if(isNaN(f_x)){
        alert('function:changeTwoDecimal->parameter error');
        return false;
    }
    f_x = Math.round(f_x *100)/100;

    return f_x;
};